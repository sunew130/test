# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class BatchValidationWizard(models.TransientModel):

    _name = 'batch.validation.wizard'

    order_ids = fields.Many2many('sale.order')
    type = fields.Selection([('validate_tier', '验证'), ('reject_tier', '拒绝')], '验证类型', required=True)
    
    def button_confirm(self):
        for order in self.order_ids:
            getattr(order, self.type)()
        