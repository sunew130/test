# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class BatchApprovalWizard(models.TransientModel):

    _name = 'batch.approval.wizard'
    
    order_ids = fields.Many2many('sale.order')
    
    def button_confirm(self):
        for order in self.order_ids:
            order.request_validation()