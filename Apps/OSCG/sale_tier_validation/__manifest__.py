# Copyright 2019 Open Source Integrators
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Sale Tier Validation",
    "summary": "Extends the functionality of Sale Orders to "
    "support a tier validation process.",
    "version": "14.0.1.0.0",
    "category": "Sale",
    "website": "https://github.com/OCA/sale-workflow",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["sale", "base_tier_validation"],
    "data": [
        "security/ir.model.access.csv",
        "views/sale_order_view.xml",
        "wizard/batch_approval_wizard_views.xml",
        "wizard/batch_validation_wizard_views.xml",
    ],
}
