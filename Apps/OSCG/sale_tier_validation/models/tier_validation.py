# Copyright 2017-19 ForgeFlow S.L. (https://www.forgeflow.com)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
class TierValidation(models.AbstractModel):
    _inherit = "tier.validation"
    
    @api.model
    def _get_under_validation_exceptions(self):
        res = super()._get_under_validation_exceptions()
        fields = self.env['sale.order'].fields_get()
        """Extend for more field exceptions."""
        return res + list(fields.keys())