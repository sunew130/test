# Copyright 2019 Open Source Integrators
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _name = "sale.order"
    _inherit = ["sale.order", "tier.validation"]
    _state_from = ["draft", "sent", "to approve"]
    _state_to = ["sale", "approved"]

    def action_batch_validation(self):
        return {
            'name': '批量验证',
            'type': 'ir.actions.act_window',
            'res_model': 'batch.validation.wizard',
            'view_mode': 'form',
            'target': 'new',
            'context':{'default_order_ids': self.ids}
        }


    def action_batch_approval(self):
        return {
            'name': '批量提交验证',
            'type': 'ir.actions.act_window',
            'res_model': 'batch.approval.wizard',
            'view_mode': 'form',
            'target': 'new',
            'context':{'default_order_ids': self.ids}
        }

    def _validate_tier(self, tiers=False):
        res = super(SaleOrder, self)._validate_tier(tiers)
        self.action_confirm()
        return res