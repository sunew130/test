# -*- coding: utf-8 -*-
# Part of Odoo.

from . import controllers
from . import models
from . import wizard
from . import report
