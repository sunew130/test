#-*- coding:utf-8 -*-
# Part of Odoo.

from . import models
from . import wizard
from . import controllers
from . import report
