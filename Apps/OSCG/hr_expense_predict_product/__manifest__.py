# -*- coding: utf-8 -*-
# Part of Odoo.

{
    'name': 'Hr Expense Predict product',
    'version': '1.0',
    'category': 'Human Resources/Expenses',
    'summary': 'predict the product from the expense description based on old ones',
    'depends': ['hr_expense'],
    'data': [],
    'auto_install': True,
      'license': '',
}
