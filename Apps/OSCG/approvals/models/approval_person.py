# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from odoo.modules.module import get_module_resource


class ApprovalPerson(models.Model):
    _name = 'approval.person'
    _description = 'Approval Person'
    _order = 'sequence'

    _check_company_auto = True

    sequence = fields.Integer(string="Sequence", default_sequence=1)

    company_id = fields.Many2one(
        'res.company', 'Company', copy=False,
        required=True, index=True, default=lambda s: s.env.company)

    user_id = fields.Many2one('res.users', string="Approves", check_company=True,
                              domain="['&', ('company_ids', 'in', company_id), ('share', '=', False)]")

    category_id = fields.Many2one('approval.category', string='Category Reference', index=True, required=True,
                               ondelete='cascade')
