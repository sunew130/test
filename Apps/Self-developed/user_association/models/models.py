# -*- coding: utf-8 -*-

# from odoo import models, fields, api
from odoo import fields, models


class user_inherit(models.Model):
    _inherit = "res.users"
    associate_user_id = fields.One2many('user_association.user_association', 'x_user_id')
    # associate_user_id_id = fields.Many2many('user_association.user_association')


class user_association(models.Model):
    _name = 'user_association.user_association'
    _description = 'user_association.user_association'
    x_user_id = fields.Many2one('res.users', string='Users to be associated')
    x_associated_user_id = fields.Many2many('res.users', string="Associated user")
