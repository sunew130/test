# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from odoo.exceptions import UserError, ValidationError
from operator import itemgetter


class MultiDelivery(models.TransientModel):
    _name = "multi.delivery"

    repair_ids = fields.Many2many('repair.order', string="Repair Id")
    express_id = fields.Many2one('delivery.carrier', string="Express Id")

    waybillNo = fields.Char(string='Way Bill No.', required=True)
    toPhone = fields.Char(string='To Phone', required=True)
    toAddress = fields.Char(string='To Address', required=True)
    transPrice = fields.Float(string='Transport Price', required=True)

    def _check_repaired(self):
        if any(repair.repaired is False for repair in self.repair_ids):
            raise UserError(_('You cannot delivery a repair order which is not repaired!'))
        elif any(repair.state not in ('done', 'cancel') for repair in self.repair_ids):
            raise UserError(_('You cannot delivery a repair order which state is not done or cancel!'))
        elif any(repair.isReturn is True for repair in self.repair_ids):
            raise UserError(_('You cannot delivery a repair order which is already repaired and return!'))
        else:
            return True

    @api.onchange('repair_ids')
    def _onchange_repair_ids(self):
        act_ids, act_mod = itemgetter('active_ids', 'active_model')(self._context)
        if act_mod == 'helpdesk.ticket':
            repairs = self.env['repair.order'].search([('ticket_id', 'in', act_ids)])
            self.repair_ids = repairs
            return {
                'domain': {'repair_ids': [('ticket_id', 'in', act_ids)]}
            }
        if act_mod == 'repair.order':
            self.repair_ids = act_ids
            return {
                'domain': {'repair_ids': [('id', 'in', act_ids)]}
            }

    def multi_delivery(self):
        if self._check_repaired():
            vals = {
                'carrier': self.express_id.name or '',
                'waybillNo': self.waybillNo or '',
                'transPrice': self.transPrice or '',
                'toPhone': self.toPhone or '',
                'toAddress': self.toAddress or '',
                'isReturn': True
            }
            return [r.write(vals) for r in self.repair_ids]
