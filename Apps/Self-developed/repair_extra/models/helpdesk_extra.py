# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class HelpdeskTickerExtra(models.Model):
    _inherit = 'helpdesk.ticket'

    taxes_id = fields.Many2many(related='product_id.product_tmpl_id.taxes_id')

    def unlink(self):
        """
        `use_product_returns` and `use_credit_notes` untreated .
        @return:
        """
        self.tickets_stage_alter()
        for ticket in self:
            if ticket.use_product_repairs:
                ticket.mapped('repair_ids').unlink()
        return super(HelpdeskTickerExtra, self).unlink()

    def ticket_multi_delivery(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _('Multi Delivery'),
            'res_model': 'multi.delivery',
            'views': [(False, 'form')],
            'target': 'new',
        }

    def _stage_change(self, ticket, stage, state):
        nums = len(state)
        if nums == 0:
            ticket.stage_id = stage['新建']
        if nums == 1:
            if 'draft' in state:
                ticket.stage_id = stage['新建']
            elif 'done' in state:
                ticket.stage_id = stage['已解决']
            elif 'cancel' in state:
                ticket.stage_id = stage['已取消']
            else:
                ticket.stage_id = stage['进行中']
        if nums == 2:
            if 'done' in state and 'cancel' in state:
                ticket.stage_id = stage['已解决']
            else:
                ticket.stage_id = stage['进行中']
        if nums > 2:
            ticket.stage_id = stage['进行中']

    def tickets_stage_alter(self, *args):
        """conditions: team config can use mode and stage."""
        for t in self:
            team_stages = self.env['helpdesk.stage'].search([('team_ids', 'in', t.team_id.id)])
            stage = {s.name: s for s in team_stages}
            r_set = {r.state for r in t.repair_ids}
            stage and self._stage_change(t, stage, r_set) or False
            stage.clear()
            r_set.clear()
        return args
