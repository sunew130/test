# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class RepairExtra(models.Model):
    _inherit = 'repair.order'

    carrier = fields.Char(string='Carrier')
    waybillNo = fields.Char(string='Way Bill No.')
    transPrice = fields.Float(string='Transport Price')
    toPhone = fields.Char(string='To Phone')
    toAddress = fields.Char(string='To Address')
    isReturn = fields.Boolean(string='Is Return', copy=False, readonly=True)

    taxes_id = fields.Many2many(related='product_id.product_tmpl_id.taxes_id')
    picking_id = fields.Many2one('stock.picking', 'Transfer', copy=False, readonly=True, tracking=True,
                                 check_company=True)

    # picking_ids = fields.One2many('stock.picking', 'repair_id', 'Transfer')
    # stock_move_ids = fields.One2many(related='picking_id.move_line_ids')
    # stock_move_ids = fields.One2many('stock.move', 'repair_id')

    def action_open_stock_picking(self):
        self.ensure_one()
        action = {
            'type': 'ir.actions.act_window',
            'name': _('Picking Order'),
            'res_model': 'stock.picking',
            'view_mode': 'form',
            'res_id': self.picking_id.id,
            'domain': [('id', '=', self.picking_id.id)],
            'context': {'create': False}
        }
        return action

    def action_open_stock_moves(self):
        self.ensure_one()
        # domain = [("id", "in", self.mapped("stock_move_ids").ids)]
        action = {
            "type": "ir.actions.act_window",
            "name": _("Stock Moves"),
            "res_model": "stock.move",
            "view_type": "tree",
            "view_mode": "list,form",
            "domain": [("repair_id", "=", self.id)],
            "context": {'create': False}
        }
        return action

    @api.onchange('product_id')
    def _init_product_qty(self):
        for repair in self:
            if repair.tracking == 'serial':
                repair.product_qty = float(1)

    def repair_multi_delivery(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _('Multi Delivery'),
            'res_model': 'multi.delivery',
            'views': [(False, 'form')],
            'target': 'new',
        }

    @property
    def _get_stock_warehouse(self):
        warehouse = self.env['stock.warehouse'].search(
            [('company_id', '=', self.env.company.id)], limit=1)
        return warehouse

    @property
    def _get_repair_location(self) -> int:
        location = self.env['stock.location'].search(
            [('id', 'child_of', self.company_id.id), ('name', '=', '维修仓')])
        return location.id

    @api.onchange('company_id')
    def _onchange_company_id(self):
        if self.company_id:
            self.location_id = self._get_repair_location
        else:
            self.location_id = False

    def _merge_lines(self) -> dict:
        data = {}
        if len(self.operations) > 0:
            for op in self.operations:
                if op.product_id not in data:
                    data[op.product_id] = dict(qty=0)
                    data[op.product_id].update(dict(serial=op.lot_id))
                if op.type == 'add':
                    data[op.product_id]['qty'] += op.product_uom_qty
                    if op.lot_id:
                        data[op.product_id]['serial'] |= op.lot_id
                if op.type == 'remove':
                    data[op.product_id]['qty'] -= op.product_uom_qty

            return data

    def _create_repair_stock_picking(self):
        self.ensure_one()
        vals = {
            'company_id': self.company_id.id,
            'partner_id': self.partner_id.id,
            'user_id': self.user_id.id,
            'picking_type_id': self._get_stock_warehouse.int_type_id.id,
            'location_id': self._get_stock_warehouse.lot_stock_id.id,
            'location_dest_id': self.location_id.id,
            'state': 'draft',
            'origin': self.name,
            # 'move_lines': [],
            # 'move_line_ids': []
        }
        return self.env['stock.picking'].create(vals)

    def action_repair_confirm(self):
        res, moves = super().action_repair_confirm()
        for repair in self:
            picking = repair._create_repair_stock_picking()
            picking.write({'move_lines': [(6, 0, moves[repair.id][0].ids)]})
            repair.picking_id = picking.id
            picking.action_confirm()
            repair.mapped("stock_move_ids")._action_confirm()
        return res

    def action_repair_start(self):
        if self.picking_id.state != 'done':
            raise UserError(_('Please check the repair picking materials availability.'))
        # self.picking_id.action_assign()
        return super().action_repair_start()

    def action_repair_cancel(self):
        self.picking_id.action_cancel()
        return super().action_repair_cancel()

    def action_repair_end(self):
        for repair in self:
            # `state` maybe confirmed
            if repair.move_id.state not in ['assigned', 'done']:
                repair.move_id.write({'state': 'done'})
        return super().action_repair_end()

    def unlink(self):
        super().unlink()
        for order in self:
            if order.picking_id:
                self.picking_id.action_cancel()
        return True


class RepairLineExtra(models.Model):
    _inherit = 'repair.line'

    @api.onchange('type')
    def onchange_operation_type(self):
        super().onchange_operation_type()
        if self.type == 'add':
            self.location_dest_id = self.repair_id._get_repair_location
        if self.type == 'remove':
            self.location_id = self.repair_id._get_repair_location

    @api.onchange('lot_id')
    def _onchange_lot_id(self):
        for line in self:
            if line.lot_id and line.product_id.tracking == 'serial':
                line.product_uom_qty = float(1)

    @api.onchange('product_id')
    def _init_lot_id(self):
        for line in self:
            line.lot_id = False
