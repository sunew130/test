# -*- coding: utf-8 -*-
{
    'name': "repair_extra",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "sunell",
    'website': "http://www.sunell.com.cn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['repair', 'helpdesk', 'helpdesk_repair'],

    # always loaded
    'data': [
        'security/repair_extra_security.xml',
        'security/ir.model.access.csv',
        'views/helpdesk_extra_views.xml',
        'views/repair_extra_views.xml',
        'views/multi_delivery_views.xml',
        'views/templates.xml',
    ],

    'qweb': ['static/src/xml/*.xml'],

}
