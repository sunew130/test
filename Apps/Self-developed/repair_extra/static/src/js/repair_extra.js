odoo.define('repair_extra.alterBtnController', function (require) {
    "use strict";

    var core = require('web.core');
    var ListController = require('web.ListController');
    var Dialog = require('web.Dialog');

    let _t = core._t;


    ListController.include({
        _onclickStageAlter: function () {
            var ids = this.getSelectedIds()
            if (!ids.length || ids.length > 80) {
                Dialog.alert(this, _t("Please select records to update stage. (max: 80)"));
            } else {
                this._rpc({
                    model: this.modelName,
                    method: 'tickets_stage_alter',
                    args: [ids, this.viewType],
                }).then((res) => {
                    !_.isEmpty(res) ? location.reload() : false;
                });
            }
        },
        _onSelectionChanged: function () {
            this._super.apply(this, arguments);
            if (this.modelName === 'helpdesk.ticket') {
                this.$('.o_list_button_stage').toggle(!!this.selectedRecords.length);
            }
        },
        renderButtons: function () {
            this._super.apply(this, arguments);
            if (this.modelName === 'helpdesk.ticket') {
                this.$buttons.find('.o_list_button_stage').on('click', this._onclickStageAlter.bind(this));
            }
        },
    });

});