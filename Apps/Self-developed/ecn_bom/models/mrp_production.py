# -*- coding: utf-8 -*-
from odoo import models, fields, api


class MrpProduction(models.Model):
    """ Manufacturing Orders """
    _inherit = 'mrp.production'

    def button_batch_substitution(self):
        #
        #   遍历bom表的物料，如果发现物料有ECN替代变更，批量替代物料
        #   如果重复点击按钮，会把旧料替换成替代料，相信是一个bug
        for production in self:
            factor = production.product_uom_id._compute_quantity(production.product_qty,
                                                                 production.bom_id.product_uom_id) / production.bom_id.product_qty
            boms, lines = production.bom_id.explode(production.product_id, factor,
                                                    picking_type=production.bom_id.picking_type_id)
            for bom_line, line_data in lines:
                if bom_line.child_bom_id and bom_line.child_bom_id.type == 'phantom' or \
                        bom_line.product_id.type not in ['product', 'consu']:
                    continue

                for ecn_item_id in bom_line.ecn_item_id:
                    if ecn_item_id.alternative_type == 'replace':
                        ecn_item_id.handle_replace_material(self)

    def button_auto_substitution(self):

        #    遍历bom表的物料，如果发现物料有ECN变更，先使用原物料，原物料消耗完，才使用ECN变更后的物料，
        #    混合用料:
        #        第一种情况：ECN变更是取代料，消耗旧料不足，再消耗取代料也不足，物料需求尾数应该是在取代料行数中
        #        第二种情况：ECN变更是替代料，消耗旧料不足，再消耗取代料也不足，物料需求尾数应该是在旧料行数中
        #
        #    如果重复点击按钮，会把旧料替换成替代料，相信是一个bug

        for production in self:
            factor = production.product_uom_id._compute_quantity(production.product_qty,
                                                                 production.bom_id.product_uom_id) / production.bom_id.product_qty
            boms, lines = production.bom_id.explode(production.product_id, factor,
                                                    picking_type=production.bom_id.picking_type_id)
            for bom_line, line_data in lines:
                if bom_line.child_bom_id and bom_line.child_bom_id.type == 'phantom' or \
                        bom_line.product_id.type not in ['product', 'consu']:
                    continue

                for ecn_item_id in bom_line.ecn_item_id:

                    # 第一种情况：ECN变更是取代料
                    if ecn_item_id.alternative_type == 'natural_substitute':
                        ecn_item_id.handle_natural_substitute_material(self)
                    elif ecn_item_id.alternative_type == 'replace':
                        ecn_item_id.handle_replace_material(self)
