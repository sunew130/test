from odoo import models, fields, api

class StockMove(models.Model):
    _inherit = 'stock.move'

    ecn_replace_count = fields.Integer(string="Replace material",  compute="_compute_ecn_replace_count")

    def _compute_ecn_replace_count(self):
        for record in self:
            if record.bom_line_id:
                domain = [('work_id.state', '=', 'approved')]
                domain += [('var_bom_line.id', '=', record.bom_line_id.id)]
                domain += [('alternative_type', '=', 'replace')]
                count = len(self.env['ecn_bom.ecn_item_record'].search(domain))
                record.ecn_replace_count = count
            else:
                record.ecn_replace_count = 0

    def action_replace_material(self):
        if self.ecn_replace_count <= 0:
            return

        if self.bom_line_id:
            domain = [('work_id.state', '=', 'approved')]
            domain += [('var_bom_line.id', 'in', self.bom_line_id.ids)]
            domain += [('alternative_type', '=', 'replace')]
            ecn_item = self.env['ecn_bom.ecn_item_record'].search(domain)

            return {
                'type': 'ir.actions.act_window',
                'name': 'Replace material',
                'view_mode': 'tree',
                'res_model': 'ecn_bom.ecn_item_record',
                'target': 'new',
                'flags': {'action_buttons': False,
                          'search_view': False,
                          'hidden': True,
                          'disable_groupby': True,
                          },
                'res_id': ecn_item.ids,
                'domain': [('id', 'in', ecn_item.ids)],
            }
