from odoo import models, fields, api
from datetime import datetime


class EcnWork(models.Model):
    _name = 'ecn_bom.ecn_work'
    _description = 'ecn_bom.ecn_work'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    READONLY_STATES = {
        'approving': [('readonly', True)],
        'approved': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    name = fields.Char(string='Enc Work', required=True, index=True, copy=False, default='New', tracking=True)
    record_time = fields.Datetime(string='Record Date', readonly=True, states=READONLY_STATES, index=True, copy=False,
                                  default=fields.Datetime.now, )
    effect_time = fields.Datetime(string='Effect Date', readonly=True, tracking=True)
    applicant = fields.Many2one('res.users', string='Applicant', states=READONLY_STATES, tracking=True,
                                default=lambda self: self.env.user)
    attachment_number = fields.Integer('Number of Attachments', compute='_compute_attachment_number')
    # notes = fields.Text('ecn notes')
    state = fields.Selection([
        ('wait', 'Waiting'),
        ('ready', 'Ready'),
        ('approving', 'Approving'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
    ], default="wait", store=True, tracking=True)

    ecn_item_id = fields.One2many('ecn_bom.ecn_item', 'work_id', states=READONLY_STATES, string='Work item', copy=True)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            company_id = vals.get("company_id", self.env.company.id)
            seq_date = None
            if 'record_time' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['record_time']))

        vals['name'] = self.env['ir.sequence'].with_company(company_id).next_by_code('ecn.work',
                                                                                     sequence_date=seq_date) or '/'
        vals['state'] = 'ready'
        return super(EcnWork, self).create(vals)

    def button_cancel(self):
        self.sudo().write({'state': 'cancel'})

    def button_approving(self):
        self.sudo().write({'state': 'approving'})

    def button_approved(self):
        # 处理ecn更变后的bom操作
        self.ecn_item_id.action_handle_ecn_bom()

        self.sudo().write({'state': 'approved'})
        self.sudo().write({'effect_time': fields.Datetime.now()})

        # 添加ECN记录到bom
        self.ecn_item_id.action_bom_add_ecn_record()

    def _compute_attachment_number(self):
        domain = [('res_model', '=', 'ecn_bom.ecn_work'), ('res_id', 'in', self.ids)]
        attachment_data = self.env['ir.attachment'].read_group(domain, ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for work in self:
            work.attachment_number = attachment.get(work.id, 0)

    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window']._for_xml_id('base.action_attachment')
        res['domain'] = [('res_model', '=', 'ecn_bom.ecn_work'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'ecn_bom.ecn_work', 'default_res_id': self.id}
        return res


class EcnItem(models.Model):
    _name = 'ecn_bom.ecn_item'
    _description = 'ecn_bom.ecn_item'
    work_id = fields.Many2one('ecn_bom.ecn_work', string='Enc Work', index=True, required=True,
                              ondelete='cascade')

    var_main_bom = fields.Many2one('mrp.bom', string='var main bom', invisible=True)
    # 属于变异物料的产品
    var_product = fields.Many2one('product.product', string='var product')
    var_bom_line = fields.Many2one('mrp.bom.line', string='var bom line', invisible=True)
    var_product_quantity = fields.Float(string='var product quantity')
    replace_product = fields.Many2one('product.product', string='replace product', tracking=True)
    replace_product_quantity = fields.Float(string='replace product quantity', tracking=True)

    ALTERNATIVE_CHAR = [
        ('natural_substitute', 'Natural Substitute'),  # 自然消耗取代
        ('immediate_substitute', 'Immediate Substitute'),  # 立即变更取代
        ('new_add', 'New add'),  # 新增物料
        ('replace', 'Replace')]  # 替换物料

    alternative_type = fields.Selection(ALTERNATIVE_CHAR, 'Alternative Type', default='natural_substitute',
                                        required=True)

    ecn_record_ids = fields.Many2one('mrp.bom')

    tag_number = fields.Char('Tag number')

    remarks = fields.Text('Remarks')

    @api.onchange('var_main_bom')
    def onchange_var_main_bom(self):
        res = {'domain': {'var_product': [('id', '=', self.var_main_bom.bom_line_ids.product_id.ids)]}}
        return res

    @api.onchange('var_product')
    def onchange_var_product(self):
        var_bom_line = self.var_main_bom.bom_line_ids.filtered(lambda x: x.product_id.id == self.var_product.id)
        if var_bom_line:
            self.var_bom_line = var_bom_line[0]
            self.var_product_quantity = self.var_bom_line.product_qty

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "%s" % record.work_id.name))
        return result

    def action_handle_ecn_bom(self):
        for record in self:
            if record.alternative_type == 'natural_substitute' or record.alternative_type == 'immediate_substitute':
                # 基于原来的bom_line替换物料和数量
                record.var_bom_line.update({'product_id': record.replace_product.id,
                                           'product_tmpl_id': record.replace_product.product_tmpl_id.id,
                                           'product_qty': record.replace_product_quantity})
            elif record.alternative_type == 'new_add':
                # 在原来的bom_line新增物料和数量
                if record.var_main_bom:
                    vals = {'bom_id': record.var_main_bom.id, 'product_id': record.replace_product.id,
                            'product_tmpl_id': record.replace_product.product_tmpl_id.id,
                            'product_qty': record.replace_product_quantity, 'product_uom_id': 1, 'operation_id': False}
                    self.env['mrp.bom.line'].create(vals)

    def action_bom_add_ecn_record(self):
        for item in self:
            if item.alternative_type == 'replace':
                item.var_bom_line.ecn_item_id.filtered_domain([('alternative_type', '=', 'replace')]).unlink()
            elif item.alternative_type == 'natural_substitute' or item.alternative_type == 'immediate_substitute':
                item.var_bom_line.ecn_item_id.unlink()

            vals = {'work_id': item.work_id.id, 'name': item.work_id.name, 'effect_time': item.work_id.effect_time,
                    'applicant': item.work_id.applicant.id, 'var_product': item.var_product.id,
                    'var_main_bom': item.var_main_bom.id,
                    'var_bom_line': item.var_bom_line.id, 'var_product_quantity': item.var_product_quantity,
                    'replace_product': item.replace_product.id,
                    'replace_product_quantity': item.replace_product_quantity,
                    'alternative_type': item.alternative_type, 'tag_number': item.tag_number,
                    'remarks': item.remarks}

            self.env['ecn_bom.ecn_item_record'].create(vals)
