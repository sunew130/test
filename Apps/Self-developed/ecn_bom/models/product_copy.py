# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models, SUPERUSER_ID

class ProductCopy(models.Model):
    _name = 'ecn_bom.product_copy'
    _description = 'ecn_bom.product_copy'

    change_product_num = fields.Char(string='change product num')
    is_copy_bom = fields.Boolean(string='is copy bom')
    is_copy_ecn = fields.Boolean(string='is copy ecn')

    def button_copy_product(self):
        # 获取当前的产品
        active_model = self.env.context.get('active_model', False)
        active_id = self.env.context.get('active_id', False)
        product_template = self.env[active_model].search([('id', '=', active_id)], limit=1)
        product_template_copy = product_template.copy(default={})
        product_template_copy.update({'default_code': self.change_product_num})
        # 是否复制BOM
        if self.is_copy_bom is True:
            for bom_id in product_template.bom_ids:
                copy_bom_id = bom_id.copy()
                copy_bom_id.write({'product_tmpl_id': product_template_copy.id})

                # 是否复制BOM的物料变更记录
                if self.is_copy_ecn is True:
                    if bom_id.ecn_record_ids:
                        copy_ecn_record_ids = bom_id.ecn_record_ids.copy()
                        copy_ecn_record_ids.write({'var_main_bom': copy_bom_id.id, 'name': 'copy'})
                        for copy_ecn_record in copy_ecn_record_ids:
                            copy_ecn_record.var_bom_line = copy_bom_id.bom_line_ids.filtered_domain([('product_tmpl_id', '=', copy_ecn_record.var_bom_line.product_tmpl_id.id)])
                        copy_bom_id.ecn_record_ids = copy_ecn_record_ids

        return {
            'name': "product template",
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'product.template',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'res_id': product_template_copy.id,
        }
