# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import operator as py_operator
from ast import literal_eval
from collections import defaultdict
from odoo import _, api, fields, models, SUPERUSER_ID


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def button_copy_product(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Product copy',
            'view_mode': 'form',
            'res_model': 'ecn_bom.product_copy',
            'target': 'new',
            'flags': {'action_buttons': False,
                      'search_view': False,
                      'hidden': True,
                      'disable_groupby': True,
                      },
        }


class ProductProduct(models.Model):
    _inherit = 'product.product'

    def button_copy_product(self):
        self.ensure_one()
        action = self.product_tmpl_id.button_copy_product()
        return action
