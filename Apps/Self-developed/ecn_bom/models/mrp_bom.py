from odoo import models, fields, api
from datetime import datetime


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    ecn_item_id = fields.One2many('ecn_bom.ecn_item_record', 'var_bom_line', string='ECN Record')
    replace_product = fields.Many2one(related='ecn_item_id.replace_product', string='replace product')
    replace_product_quantity = fields.Float(related='ecn_item_id.replace_product_quantity', string='replace product quantity')
    alternative_type = fields.Selection(related='ecn_item_id.alternative_type', string='Alternative Type')

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        res = super(MrpBomLine, self).search(args, offset, limit, order, count)
        return res


class MrpBom(models.Model):
    _inherit = 'mrp.bom'

    ecn_record_ids = fields.Many2many('ecn_bom.ecn_item_record', 'var_bom_line', string='ECN Record', compute='_compute_ecn_record')

    def _compute_ecn_record(self):
        res = self.env['ecn_bom.ecn_item_record']
        for item in self.bom_line_ids:
            if len(item.ecn_item_id) > 0:
                res |= item.ecn_item_id

        self.ecn_record_ids = res








