# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime


class EcnItemRecord(models.Model):
    _name = 'ecn_bom.ecn_item_record'
    _description = 'ecn_bom.ecn_item_record'

    work_id = fields.Many2one('ecn_bom.ecn_work', string='Enc Work', index=True)

    name = fields.Char(string='ECN name')
    effect_time = fields.Datetime(string='Effect Date', tracking=True)
    applicant = fields.Many2one('res.users', string='Applicant', tracking=True,
                                default=lambda self: self.env.user)

    # 属于变异物料的产品
    var_main_bom = fields.Many2one('mrp.bom', string='var main bom')
    var_bom_line = fields.Many2one('mrp.bom.line', string='var bom line')
    var_product = fields.Many2one('product.product', string='var product')
    var_product_quantity = fields.Float(string='var product quantity')
    replace_product = fields.Many2one('product.product', string='replace product', tracking=True)
    replace_product_quantity = fields.Float(string='replace product quantity', tracking=True)

    ALTERNATIVE_CHAR = [
        ('natural_substitute', 'Natural Substitute'),  # 自然消耗取代
        ('immediate_substitute', 'Immediate Substitute'),  # 立即变更取代
        ('new_add', 'New add'),  # 新增物料
        ('replace', 'Replace')]  # 替换物料

    alternative_type = fields.Selection(ALTERNATIVE_CHAR, 'Alternative Type', default='natural_substitute',
                                        required=True)

    ecn_record_ids = fields.Many2one('mrp.bom')

    tag_number = fields.Char('Tag number')

    remarks = fields.Text('Remarks')

    is_active = fields.Boolean('Active', default=True)

    def button_replace_ecn_item(self):
        # 获取当前的移动
        active_model = self.env.context.get('active_model', False)
        active_id = self.env.context.get('active_id', False)
        move_id = self.env[active_model].search([('id', '=', active_id)], limit=1)

        # 物料替换当前的移动
        if move_id:
            for production in move_id.raw_material_production_id:
                self.env['stock.move'].create(production._get_move_raw_values(
                    self.replace_product,
                    move_id.product_uom_qty,
                    self.var_bom_line.product_uom_id,
                ))

            move_id.product_uom_qty = 0

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        res = super(EcnItemRecord, self).search(args, offset, limit, order, count)

        # if self._context.get('show_newest_record', False):
        #     res_dict = {}
        #     # 如果相同一个产品/物料，取最新有效的ECN记录
        #     for item in res:
        #         if item.var_element.id in res_dict.keys():
        #             if res_dict[item.var_element.id].work_id.effect_time > item.work_id.effect_time:
        #                 continue
        #         res_dict[item.var_element.id] = item
        #
        #     res = self.env['ecn_bom.ecn_item_record']
        #     for item in res_dict.values():
        #         res |= item
        #     return res

        return res

    def handle_natural_substitute_material(self, production=None):
        #
        #   处理自然消耗物料
        #
        if production is None:
            return

        for ecn_item_id in self:
            # 变异元件
            var_bom_line = ecn_item_id.var_bom_line
            var_product = ecn_item_id.var_product
            # 替换元件
            replace_product = ecn_item_id.replace_product

            # 获取物料的移动
            ori_move = production.move_raw_ids.filtered(
                lambda move: move.product_tmpl_id.product_variant_id.id == replace_product.id)

            # 创建旧料的移动
            old_move = self.env['stock.move'].create(production._get_move_raw_values(
                var_product,
                0,
                var_bom_line.product_uom_id,
            ))

            # 旧物料剩余数量 = 旧物料在手数量 - 旧物料工单已占料数量
            surplus_qty = old_move.to_on_hand_qty - old_move.to_forecast_availability

            # 更变物料需求数量 = 旧物料需求数量 - 旧物料剩余数量
            replace_demand_qty = ori_move.product_uom_qty - surplus_qty
            if replace_demand_qty < 0:
                old_move.update({
                    'product_uom_qty': ori_move.product_uom_qty,
                })

                ori_move.update({
                    'product_uom_qty': 0,
                })
            else:
                old_move.update({
                    'product_uom_qty': surplus_qty,
                })

                if replace_demand_qty >= 0:
                    # 物料需求尾数应该是在取代料行数中
                    ori_move.update({
                        'product_uom_qty': replace_demand_qty,
                    })

    def handle_replace_material(self, production=None):
        #
        #   处理替换物料
        #
        if production is None:
            return

        for ecn_item_id in self:
            # 变异元件
            var_bom_line = ecn_item_id.var_bom_line
            var_product = ecn_item_id.var_product
            # 替换元件
            replace_product = ecn_item_id.replace_product

            # 获取旧料的移动
            ori_move = production.move_raw_ids.filtered(
                lambda move: move.product_tmpl_id.product_variant_id.id == var_bom_line.product_id.id)

            # 旧物料剩余数量 = 旧物料在手数量 - 旧物料工单已占料数量
            surplus_qty = ori_move.to_on_hand_qty - ori_move.to_forecast_availability
            # 更变物料需求数量 = 旧物料需求数量 - 旧物料剩余数量
            replace_demand_qty = ori_move.product_qty - surplus_qty
            if replace_demand_qty <= 0:
                ori_move.update({
                    'product_uom_qty': ori_move.product_qty,
                })
            else:
                replace_on_hand_qty = replace_product.qty_available
                dst_move = self.env['stock.move'].search([('product_id.id', '=', replace_product.id)], limit=1)
                if dst_move:
                    replace_on_hand_qty -= dst_move.to_forecast_availability

                # 消耗ECN替代料，假设替代料不足，把剩余尾数转到旧物料的需求量
                if replace_demand_qty >= replace_on_hand_qty:
                    self.env['stock.move'].create(production._get_move_raw_values(
                        replace_product,
                        replace_on_hand_qty,
                        var_bom_line.product_uom_id,
                    ))

                    surplus_qty += replace_demand_qty - replace_on_hand_qty

                ori_move.update({
                    'product_uom_qty': surplus_qty,
                })
