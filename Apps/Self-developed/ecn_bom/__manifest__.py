# -*- coding: utf-8 -*-
{
    'name': "ecn_bom",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product', 'mrp', 'mrp_extra'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ecn_bom_security.xml',
        'security/ir.model.access.csv',
        'data/ecn_bom_data.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/ecn_bom.xml',
        'views/mrp_bom.xml',
        'views/mrp_production_extra.xml',
        'views/ecn_item_record.xml',
        'views/product_copy.xml',
        'views/product.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
