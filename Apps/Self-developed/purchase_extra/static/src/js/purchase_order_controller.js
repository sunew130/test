﻿odoo.define('purchase_extra.PurchaseOrderController', function (require) {
    "use strict";

//这些是调⽤需要的模块
    var ListView = require('web.ListView');
    var viewRegistry = require('web.view_registry');
    var ListController = require('web.ListController');
    var view_registry = require('web.view_registry');
    const {PurchaseListDashboardController, PurchaseListDashboardModel, PurchaseListDashboardRenderer} = require('purchase.dashboard')

//这块代码是继承PurchaseListDashboardController在原来的基础上进⾏扩展
    var PurchaseOrderController = PurchaseListDashboardController.extend({
        renderButtons: function () {
            this._super.apply(this, arguments);
            if (this.$buttons) {
                //这⾥找到刚才定义的class名为update_supplier的按钮
                var btn = this.$buttons.find('.o_button_update_supplier');
                //给按钮绑定click事件和⽅法update_supplier
                btn.on('click', this.proxy('update_supplier'));

            }
        },

        update_supplier: function () {
            var self = this;

            //这⾥是获取tree视图中选中的数据的记录集
            var records = _.map(self.selectedRecords, function (id) {
                return self.model.localData[id];
            });
            console.log("数据id：" + _.pluck(records, 'res_id'));
            //获取到数据集中每条数据的对应数据库id集合
            var ids = _.pluck(records, 'res_id');

            //通过rpc调⽤purchase.order 的button_update_supplier⽅法
            this._rpc({
                model: 'purchase.order',
                method: 'button_update_supplier',
                args: [ids],
            }).then(function () {
                 location.reload();
            });
        },

    });

//这块代码是继承ListView在原来的基础上进⾏扩展
//这块⼀般只需要在config中添加上⾃⼰的Model,Renderer,Controller
//这⾥我就对原来的Controller进⾏了扩展编写，所以就配置了⼀PurchaseOrderController
   var PurchaseOrderView = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Model: PurchaseListDashboardModel,
        Renderer: PurchaseListDashboardRenderer,
        Controller: PurchaseOrderController,
    }),
});

//这⾥⽤PurchaseOrderView，第⼀个字符串是注册名到时候需要根据注册名调⽤视图
view_registry.add('purchase_list_dashboard_extra', PurchaseOrderView);
return PurchaseOrderView;

});

