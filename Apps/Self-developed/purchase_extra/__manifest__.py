# -*- coding: utf-8 -*-
{
    'name': "purchase_extra",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly, 'purchase_stock'
    'depends': ['base', 'purchase', 'stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/purchase_extra_security.xml',
        'views/views.xml',
        'views/purchase_print_template_selector_views.xml',
        'views/templates.xml',
        'views/purchase_order_views.xml',
        'views/purchase_report_views.xml',
        'report/purchase_reports.xml',
        'views/purchase_extra_views.xml',
        'views/res_partner_view.xml',
        'views/supplierinfo_view.xml',
        'report/purchase_quotation_templates.xml',
        'report/purchase_quotation_templates2.xml',
        'report/purchase_quotation_templates2_struct.xml',
        'report/purchase_quotation_templates3.xml',
        'report/purchase_quotation_templates4.xml',
        'report/purchase_quotation_templates5.xml',
        'report/purchase_quotation_templates6_dev.xml',
        'report/purchase_quotation_templates_outsourcing.xml',
        'report/purchase_quotation_templates_outsourcing_zx.xml',
        'report/purchase_quotation_templates7_hand.xml',
        'report/purchase_quotation_templates8_mould.xml',
        'report/purchase_quotation_templates_end_pdt_dahua.xml',
        'report/purchase_quotation_templates_end_pdt2.xml',
        'report/purchase_quotation_templates_accessories1.xml',
        'report/purchase_quotation_templates_accessories2.xml',
        'report/purchase_quotation_templates_steel_mesh.xml',
        'report/purchase_quotation_templates_mould_make.xml',
    ],
    'qweb': [
        "static/src/xml/purchase_dashboard.xml",
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}
