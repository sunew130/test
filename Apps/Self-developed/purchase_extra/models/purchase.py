from odoo import api, models, fields, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.osv import expression

class PurchaseOrder_extra(models.Model):
    _inherit = "purchase.order"
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Approve'),
        ('finance approve', '待财务审批'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', tracking=True)

    x_rmb_upper = fields.Char("rmb cn show", compute='_compute_rmb_upper', compute_sudo=True, readonly=True)
    x_payment_provision = fields.Many2one('account.payment.term', string='payment provision')

    def button_approve_ex(self, force=False):
        # self.write({'state': 'purchase', 'date_approve': fields.Datetime.now()})
        self.write({'state': 'finance approve'})
        # self.write({'state': 'purchase', 'date_approve': fields.Datetime.now()})
        # self.filtered(lambda p: p.company_id.po_lock == 'lock').write({'state': 'done'})
        return {}

    def button_manager_back(self):
        self.write({'state': 'sent'})
        return {}

    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue

            if 'VS' in order['partner_id']['name'] and order['partner_id']['supplier_rank'] > 0:
                raise ValidationError(
                    _("Please edit the virtual supplier as the real supplier to confirm the purchase order."))

            if order.x_outsourcing is True:
                self.sudo().set_order_create_uid(order)
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step' \
                    or (order.company_id.po_double_validation == 'two_step' \
                        and order.amount_total < self.env.company.currency_id._convert(
                        order.company_id.po_double_validation_amount, order.currency_id, order.company_id,
                        order.date_order or fields.Date.today())) \
                    or order.user_has_groups('purchase.group_purchase_manager'):
                if order.x_outsourcing is True:
                    order.write({'state': 'to approve'})
                else:
                    order.button_approve()
            else:
                order.write({'state': 'to approve'})
            if order.partner_id not in order.message_partner_ids:
                order.message_subscribe([order.partner_id.id])
        return True

    def access_approve_info_msg(self):
        raise ValidationError(_("Sorry, You don't have permission to approve the purchase order."))

    def nothing_approve_order_info_msg(self):
        raise ValidationError(_("Congratulations, there is no order to approve for your selected."))

    @api.model
    def retrieve_dashboard(self):
        result = super(PurchaseOrder_extra, self).retrieve_dashboard()
        has_group = self.user_has_groups("sn_account.readonly_standard_price")
        result['groups_standard_price'] = has_group
        return result

    x_can_finish_case = fields.Integer('是否可以结案')
    x_can_recover_case = fields.Integer('是否可以反结案')

    def finish_case(self):
        for record in self:
            find_case = False
            if 0 != len(record.order_line):
                for line in record.order_line:
                    if line.qty_received == line.product_uom_qty:
                        break
                    for invoice in line.move_ids:
                        if "assigned" == invoice.state:
                            invoice.state = "cancel"
                            find_case = True
            if find_case is True:
                record['x_can_finish_case'] = 0

    def recover_case(self):
        for record in self:
            find_case = False
            if 0 != len(record.order_line):
                for line in record.order_line:
                    if line.qty_received == line.product_uom_qty:
                        break
                    for invoice in line.move_ids:
                        if "cancel" == invoice.state:
                            invoice.state = "assigned"
                            find_case = True
            if find_case is False:
                record['x_can_recover_case'] = 0

    def update_case_flag(self):
        for record in self:
            record['x_can_finish_case'] = 0
            record['x_can_recover_case'] = 0
            can_finish_case = False
            can_recover_case = False
            if 0 != len(record.order_line):
                for line in record.order_line:
                    if 0 == line.qty_received or line.qty_received == line.product_uom_qty:
                        break

                    for invoice in line.move_ids:
                        if "assigned" == invoice.state:
                            can_finish_case = True
                            break
                        if "cancel" == invoice.state:
                            can_recover_case = True
                            break
                    if can_finish_case is True or can_recover_case is True:
                        break
            if can_finish_case is True:
                record['x_can_finish_case'] = 1
                record['x_can_recover_case'] = 0
            elif can_recover_case is True:
                record['x_can_finish_case'] = 0
                record['x_can_recover_case'] = 1

    def order_modify(self):
        self.write({'state': 'sent'})
        return {}

    def button_approve(self, force=False):
        if self.x_outsourcing is True:
            self.write({'state': 'done'})
        else:
            self.write({'state': 'purchase', 'date_approve': fields.Datetime.now()})
            self.filtered(lambda p: p.company_id.po_lock == 'lock').write({'state': 'done'})
        return {}

    def button_finance_approve(self, force=False):
        for order in self:
            if order.x_outsourcing is True:
                order.write({'state': 'done'})
            else:
                order.button_approve()
        return {}

    def button_finance_back(self):
        self.write({'state': 'sent'})
        return {}

    def button_update_supplier(self):

        domain = [('partner_id.name', 'like', '-VS'),
                  ('user_id.id', '=', self.env.user.id), ('state', 'in', ['draft', 'sent'])]

        records = []
        if self:
            records = self.filtered_domain(domain)
        else:
            records = self.env['purchase.order'].search(domain)

        for record in records:
            supplier_id = None
            for order in record.order_line:
                # 计算获取最优供应商
                order._onchange_quantity()
                supplier_id = order.better_seller
                if supplier_id:
                    break

            if supplier_id:
                record.partner_id = supplier_id

        return {}

    x_outsourcing = fields.Boolean(string='Outsourcing')

    def set_order_create_uid(self, order):
        # for order in self:
        if order.x_outsourcing is True and order.create_uid != self.env.user.id:
            # self.sudo().env['purchase.order'].browse(order.id).write({'create_uid': self.env.user.id})
            # self.sudo().env['purchase.order'].browse(order.id).write({'user_id': self.env.user.id})
            sql_query = "UPDATE purchase_order SET create_uid='{0}' WHERE id='{1}'".format(self.env.user.id, order.id)
            # self.sudo().env.cr.execute(sql_query)
            self.env.cr.execute(sql_query)
            self.env.cr.commit()

    def print_quotation(self):
        self.write({'state': "sent"})
        action = {
            'name': _('打印选择'),
            'view_mode': 'tree',
            'res_model': 'purchase_extra.print_selector',
            'view_id': self.env.ref('purchase_extra.purchase_print_template_select_list').id,
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            # 'context': {
            #     'order_id': self.id
            # },
            'target': 'new'
        }
        return action

    def _compute_rmb_upper(self):
        str_digital = str(self.amount_total)
        chinese = {'1': '壹', '2': '贰', '3': '叁', '4': '肆', '5': '伍', '6': '陆', '7': '柒', '8': '捌', '9': '玖', '0': '零'}
        chinese2 = ['拾', '佰', '仟', '万', '厘', '分', '角']
        jiao = ''
        bs = str_digital.split('.')
        yuan = bs[0]
        if len(bs) > 1:
            jiao = bs[1]
        r_yuan = [i for i in reversed(yuan)]
        count = 0
        for i in range(len(yuan)):
            if i == 0:
                r_yuan[i] += '圆'
                continue
            r_yuan[i] += chinese2[count]
            count += 1
            if count == 4:
                count = 0
                chinese2[3] = '亿'

        s_jiao = [i for i in jiao][:3]  # 去掉小于厘之后的

        j_count = -1
        for i in range(len(s_jiao)):
            s_jiao[i] += chinese2[j_count]
            j_count -= 1
        last = [i for i in reversed(r_yuan)] + s_jiao

        last_str = ''.join(last)
        print(str_digital)
        print(last_str)
        for i in range(len(last_str)):
            digital = last_str[i]
            if digital in chinese:
                last_str = last_str.replace(digital, chinese[digital])

        self.x_rmb_upper = last_str

    @api.onchange('partner_id')
    def _onchange_partner_supplier(self):
        if self.partner_id is not False:
            self.x_payment_provision = self.partner_id.x_payment_provision

    def button_batch_bind(self):
        for item in self.order_line:
            if item.x_has_bind_material is True:
                records = self.sudo().env["purchase_extra.material_bind"].search(
                    [("x_product_id", '=', item.product_id.id)]
                )
                for record in records.line_ids:
                    if record.x_product_id.id not in self.order_line.product_id.ids:
                        self.sudo().env["purchase.order.line"].create([{
                            "product_id": record.x_product_id.id,
                            "name": record.x_product_id.name,
                            "product_qty": record.x_bind_count * item.product_qty,
                            "price_unit": record.x_product_id.price,
                            "order_id": self.id,
                        }])

    x_has_bind_material = fields.Boolean("Has bind material", compute='_compute_has_bind_material', compute_sudo=True)

    def _compute_has_bind_material(self):
        self.x_has_bind_material = False
        for item in self.order_line:
            if item.x_has_bind_material is True:
                records = self.sudo().env["purchase_extra.material_bind"].search(
                    [("x_product_id", '=', item.product_id.id)]
                )
                for record in records.line_ids:
                    if record.x_product_id.id not in self.order_line.product_id.ids:
                        self.x_has_bind_material = True
                        break

    @api.onchange('product_id', 'x_outsourcing')
    def onchange_product(self):
        if self._context.get('outsourcing_order') == 1:
            if self.x_outsourcing is False or self.state == 'draft':
                self.x_outsourcing = True

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'state' in groupby:
            domain = expression.AND([domain, [('state', '!=', 'sent')]])
        return super().read_group(
            domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
