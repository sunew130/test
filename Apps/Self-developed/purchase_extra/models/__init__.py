# -*- coding: utf-8 -*-

from . import models
from . import purchase
from . import res_partner
from . import purchase_order_line
from . import print_template_selector
