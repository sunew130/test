﻿from odoo import api, models, fields, _
from odoo.exceptions import AccessError, UserError, ValidationError


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    user_id = fields.Many2one(related='order_id.user_id', readonly=True)

    categ_id = fields.Many2one(related="product_id.product_tmpl_id.categ_id", readonly=True)

    qty_on_hand = fields.Float("On hand", compute='_compute_qty', compute_sudo=True, readonly=True)

    qty_to_check = fields.Float("To check", compute='_compute_qty', compute_sudo=True, readonly=True)

    qty_to_in = fields.Float("To In", compute='_compute_qty', compute_sudo=True, readonly=True)

    x_has_bind_material = fields.Boolean("Has bind material", compute='_compute_has_bind_material', compute_sudo=True)

    def _compute_qty(self):
        """计算单条采购订单的物料，采购量、入库量、待检量、未入量"""
        for record in self:
            to_check_moves = self.env['stock.move'].search([('picking_id.picking_type_id.sequence_code', '=', 'QC'),
                                                            ('move_orig_ids.id', 'in', record.move_ids.ids)])
            qty_on_hand = 0.0
            for move in to_check_moves:
                if len(move.move_line_ids) == 0:
                    break
                for move_line in move.move_line_ids:
                    if move_line.product_id.id == record.product_id.id:
                        qty_on_hand += move_line.qty_done

            record.qty_to_in = record.product_uom_qty - record.qty_received
            if record.order_id.x_can_recover_case == 1:
                record.qty_to_in = 0

            record.qty_on_hand = qty_on_hand
            record.qty_to_check = record.qty_received - qty_on_hand

    @api.depends('product_qty', 'price_unit', 'taxes_id')
    def _compute_amount(self):
        for line in self:
            # min_p_qty = line.product_id.x_min_packing_quantity
            # if min_p_qty > 0:
            #     pdt_qty = int(line.product_qty)
            #     mod_qty = min_p_qty if pdt_qty % min_p_qty > 0 else 0
            #     line.product_qty = (pdt_qty // min_p_qty) * min_p_qty + mod_qty
            vals = line._prepare_compute_all_values()
            taxes = line.taxes_id.compute_all(
                vals['price_unit'],
                vals['currency_id'],
                vals['product_qty'],
                vals['product'],
                vals['partner'])
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })
            self.update_quantity(line)

    def action_bind_material(self):
        if not self.product_id:
            return
        records = self.sudo().env["purchase_extra.material_bind"].search(
            [("x_product_id", "=", self.product_id.id)]
        )
        tips = ''
        for record in records.line_ids:
            if record.x_product_id.id not in self.order_id.order_line.product_id.ids:
                self.sudo().env["purchase.order.line"].create([{
                    "product_id": record.x_product_id.id,
                    "name": record.x_product_id.name,
                    "product_qty": record.x_bind_count * self.product_qty,
                    "price_unit": record.x_product_id.price,
                    "order_id": self.order_id.id,
                }])
            else:
                tips += "        " + record.x_product_id.name + "\n"

        if tips is not '':
            tip_string = "物料：" + self.product_id.name + "\n"
            tip_string += "已捆绑:\n" + tips + "\n无需再次添加"
            raise ValidationError(_(tip_string))

    def _compute_has_bind_material(self):
        for item in self:
            if not item.product_id:
                item.x_has_bind_material = False
            else:
                records = self.sudo().env["purchase_extra.material_bind"].search(
                    [("x_product_id", '=', item.product_id.id)]
                )
                if len(records.line_ids) > 0:
                    item.x_has_bind_material = True
                else:
                    item.x_has_bind_material = False

    # @api.onchange('product_qty')
    # def _onchange_quantity(self):
    #     super(PurchaseOrderLine, self)._onchange_quantity()
    #     if not self.product_id:
    #         return
    #     records = self.sudo().env["purchase_extra.material_bind"].search(
    #         [("x_product_id", "=", self.product_id.id)]
    #     )
    #     if self.x_has_bind_material is True:
    #         if records is False:
    #             return
    #         if records.sync_qty is False:
    #             return
    #         for record in records.line_ids:
    #             pdt_id = record.x_product_id.id
    #             if pdt_id in self.order_id.order_line.product_id.ids:
    #                 index = 0
    #                 for o_line in self.order_id.order_line:
    #                     if o_line.product_id.id == pdt_id:
    #                         # o_line.product_qty = self.product_qty * record.x_bind_count
    #                         # self.order_id.order_line[index].product_qty = self.product_qty * record.x_bind_count
    #                         self.sudo().order_id.order_line[index].update({
    #                             'product_qty': self.product_qty * record.x_bind_count,
    #                         })
    #                         break
    #                     index += 1

    def update_quantity(self, line):
        if not line.product_id:
            return
        records = self.sudo().env["purchase_extra.material_bind"].search(
            [("x_product_id", "=", line.product_id.id)]
        )
        if line.x_has_bind_material is True:
            if records is False:
                return
            if records.sync_qty is False:
                return
            for record in records.line_ids:
                pdt_id = record.x_product_id.id
                if pdt_id in line.order_id.order_line.product_id.ids:
                    index = 0
                    for o_line in line.order_id.order_line:
                        if o_line.product_id.id == pdt_id:
                            # o_line.product_qty = line.product_qty * record.x_bind_count
                            # line.order_id.order_line[index].product_qty = line.product_qty * record.x_bind_count
                            line.order_id.order_line[index].update({
                                'product_qty': line.product_qty * record.x_bind_count,
                            })
                            break
                        index += 1
