# from odoo import api, models, fields, _
from odoo import fields, models


class purchase_print_selector(models.Model):
    _name = "purchase_extra.print_selector"
    _description = 'purchase_extra.print_selector'

    template_name = fields.Char(string='Print Template Name')
    paper_name = fields.Char(string='Paper Name')
    paper_rotation = fields.Char(string='Paper Rotation')

    def action_print(self):
        # 方法1 需要在传值的时候传递自定义的order_id
        # order_id = self._context.get('order_id')
        # return self.env.ref('purchase_extra.report_purchase_quotation1').report_action(order_id)
        # 方法2 需要在传值的时候传递自定义的order_id
        # order_id = self._context.get('order_id')
        # purchase_id = self.env['purchase.order'].browse(int(order_id))
        # return self.env.ref('purchase_extra.report_purchase_quotation1').report_action(purchase_id)
        # 方法3 不需要在传值的时候传递自定义的order_id
        # order_ids = self._context.get('active_ids')
        # return self.env.ref('purchase_extra.report_purchase_quotation1').report_action(order_ids)
        order_ids = self._context.get('active_ids')
        action = None
        if self.template_name == '采购订单（一）':
            action = self.env.ref('purchase_extra.report_purchase_quotation1').report_action(order_ids)
        elif self.template_name == '采购订单（二）':
            action = self.env.ref('purchase_extra.report_purchase_quotation2').report_action(order_ids)
        elif self.template_name == '结构采购订单（二）':
            action = self.env.ref('purchase_extra.report_purchase_quotation2_struct').report_action(order_ids)
        elif self.template_name == '采购订单（三）':
            action = self.env.ref('purchase_extra.report_purchase_quotation3').report_action(order_ids)
        elif self.template_name == '采购订单（四）':
            action = self.env.ref('purchase_extra.report_purchase_quotation4').report_action(order_ids)
        elif self.template_name == '采购订单（五）':
            action = self.env.ref('purchase_extra.report_purchase_quotation5').report_action(order_ids)
        elif self.template_name == '设备采购合同（六）':
            action = self.env.ref('purchase_extra.report_purchase_quotation6').report_action(order_ids)
        elif self.template_name == '委外订单':
            action = self.env.ref('purchase_extra.report_purchase_quotation_outsourcing').report_action(order_ids)
        elif self.template_name == '委外订单（ZX专用）':
            action = self.env.ref('purchase_extra.report_purchase_quotation_outsourcing_zx').report_action(order_ids)
        elif self.template_name == '采购订单（仅用于手板）':
            action = self.env.ref('purchase_extra.report_purchase_quotation7_hand').report_action(order_ids)
        elif self.template_name == '采购订单（仅用于模具）':
            action = self.env.ref('purchase_extra.report_purchase_quotation8_mould').report_action(order_ids)
        elif self.template_name == '成品采购（大华专用）':
            action = self.env.ref('purchase_extra.report_purchase_quotation_end_pdt_dahua').report_action(order_ids)
        elif self.template_name == '成品采购合同（二）':
            action = self.env.ref('purchase_extra.report_purchase_quotation_end_pdt2').report_action(order_ids)
        elif self.template_name == '辅料合同':
            action = self.env.ref('purchase_extra.report_purchase_quotation_accessories1').report_action(order_ids)
        elif self.template_name == '辅料合同(单页)':
            action = self.env.ref('purchase_extra.report_purchase_quotation_accessories2').report_action(order_ids)
        elif self.template_name == '钢网加工合同':
            action = self.env.ref('purchase_extra.report_purchase_quotation_steel_mesh').report_action(order_ids)
        elif self.template_name == '模具采购合同':
            action = self.env.ref('purchase_extra.report_purchase_quotation_mould_make').report_action(order_ids)
        return action

