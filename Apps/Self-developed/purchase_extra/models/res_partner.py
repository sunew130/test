from odoo import api, models, fields, _
from odoo.exceptions import AccessError, UserError, ValidationError


class supplier_info(models.Model):
    _inherit = "product.supplierinfo"
    x_min_packing_quantity = fields.Integer(string="Minimum amount of packaging")

class ResPartner(models.Model):
    _inherit = "res.partner"

    taxed_id = fields.Many2one('account.tax', string='Taxed', domain=[('type_tax_use', '=', 'purchase')])
    x_supplier_code = fields.Char(string='Supplier Code')
    x_fax = fields.Char(string='Fax')
    x_is_purchase_contacts = fields.Boolean(string='Is Purchase Contacts')
    x_contract_no = fields.Char(string='Contract No')
    x_annual_cooperation_agreement = fields.Char(string='Annual cooperation agreement')
    x_payment_provision = fields.Many2one('account.payment.term', string='付款条款')
