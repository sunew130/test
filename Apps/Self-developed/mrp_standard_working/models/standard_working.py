# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class StandardWorking(models.Model):
    _name = "mrp.standard.working"
    _description = "Standard Working Hours"
    _order = 'default_code, name, id'
    _check_company_auto = True

    company_id = fields.Many2one('res.company', 'Company', index=True, default=lambda s: s.env.company)
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', auto_join=True, index=True,
                                      ondelete="cascade", delegate=True, required=True)
    routing_ids = fields.One2many('mrp.routing.workcenter', 'working_id', 'Operations', copy=True)
    time_cycle = fields.Float(related='routing_ids.time_cycle')

    _sql_constraints = [
        ('product_uniq', 'unique (product_tmpl_id)', "Product name already exists!"),
    ]


class StandardBom(models.Model):
    _inherit = 'mrp.bom'

    time_cycle = fields.Float(related='operation_ids.time_cycle')


class StandardRouting(models.Model):
    _inherit = 'mrp.routing.workcenter'

    working_id = fields.Many2one('mrp.standard.working', 'Work Center Product', index=True,
                                 check_company=True, ondelete='cascade')
