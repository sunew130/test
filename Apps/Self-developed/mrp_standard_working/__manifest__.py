# -*- coding: utf-8 -*-
{
    'name': "MRP Standard working Hours",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        IE material standard work management.
    """,

    'author': "sunell",
    'website': "http://www.sunell.com.cn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['mrp'],

    # always loaded
    'data': [
        'security/standard_working_security.xml',
        'security/ir.model.access.csv',
        'views/products_standard_working_views.xml',
        'views/bom_standard_working_views.xml',
        # 'views/templates.xml',
    ],

    # 'qweb': ['static/src/xml/*.xml'],

    'installable': True,

}
