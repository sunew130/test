from odoo import models, fields, api


class MrpReportWork(models.Model):
    _name = 'mrp_report_work.mrp_report_work'
    _description = 'mrp_report_work.mrp_report_work'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    READONLY_STATES = {
        'approving': [('readonly', True)],
        'approved': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    name = fields.Char(string='Mrp Report Work', required=True, index=True, copy=False, default='New', tracking=True)
    report_work_time = fields.Datetime(string='Report Work  Date', state=READONLY_STATES, required=True, index=True, copy=False,
                                       default=fields.Datetime.now, tracking=True)
    product_time = fields.Datetime(string='Product Date', state=READONLY_STATES, required=True, tracking=True)
    product_line = fields.Many2one('mrp.workcenter', string='Product line', state=READONLY_STATES, tracking=True)
    applicant = fields.Many2one('res.users', string='Applicant', state=READONLY_STATES, tracking=True, default=lambda self: self.env.user)

    report_work_line = fields.One2many('mrp_report_work.mrp_report_work_line', 'report_work_id',
                                       string='Report Work item', state=READONLY_STATES, copy=True)

    product_shift = fields.Selection([
        ('day shift', 'Day shift'),
        ('night shift', 'Night shift')
    ], state=READONLY_STATES, store=True, tracking=True)

    state = fields.Selection([
        ('wait', 'Waiting'),
        ('ready', 'Ready'),
        ('approving', 'Approving'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
    ], default="wait", store=True, tracking=True)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            company_id = vals.get("company_id", self.env.company.id)
            seq_date = None
            seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['report_work_time']))

        vals['name'] = self.env['ir.sequence'].with_company(company_id).next_by_code('mrp.report.work',
                                                                                     sequence_date=seq_date) or '/'
        vals['state'] = 'ready'
        return super(MrpReportWork, self).create(vals)

    def button_cancel(self):
        self.sudo().write({'state': 'cancel'})

    def button_approving(self):
        self.sudo().write({'state': 'approving'})

    def button_approved(self):
        self.sudo().write({'state': 'approved'})


class MrpReportWorkLine(models.Model):
    _name = 'mrp_report_work.mrp_report_work_line'
    _description = 'mrp_report_work.mrp_report_work_line'

    report_work_id = fields.Many2one('mrp_report_work.mrp_report_work', string='Mrp Report Work', index=True,
                                     required=True,
                                     ondelete='cascade')

    mrp_id = fields.Many2one('mrp.production', string='Mrp production')

    product_qty = fields.Float(related='mrp_id.product_qty', string='Product qty', readonly=True)

    product_id = fields.Many2one(related='mrp_id.product_id', string='Product', readonly=True)

    good_product_qty = fields.Float(string='Good Product qty', required=True)

    worker_qty = fields.Float(string='Worker qty', required=True)

    single_worker_time = fields.Float(string='Single Worker Time', required=True)

    worker_time = fields.Float(string='Worker Time', compute='_compute_work_time')

    machine_worker_time = fields.Float(string='Machine worker Time', compute='_compute_work_time')

    work_order_id = fields.Many2one('mrp.workorder', string='work order', copy=True)

    result_notes = fields.Text('Result notes')

    responsible_department_1 = fields.Char(string='Responsible Department 1')

    responsible_department_2 = fields.Char(string='Responsible Department 2')

    responsible_department_3 = fields.Char(string='Responsible Department 3')

    responsible_department_4 = fields.Char(string='Responsible Department 4')

    @api.depends('worker_qty', 'single_worker_time')
    def _compute_work_time(self):
        for record in self:
            if record.single_worker_time > 0 and record.worker_qty > 0:
                sum_time = record.single_worker_time * record.worker_qty
                record.worker_time = sum_time
                record.machine_worker_time = sum_time
            else:
                record.worker_time = 0
                record.machine_worker_time = 0

    @api.onchange('report_work_id')
    def onchange_report_work_id(self):
        res = {'domain': {'work_order_id': [('production_id', '=', self.report_work_id.id)]}}
        return res
