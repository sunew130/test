# -*- encoding: utf-8 -*-
import os
import logging
from odoo import models, fields, api, _, osv
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

COMPARE_TYPE = [
    ('all_material', '全部料'),  # 全部料
    ('diff_material', '差异料'),  # 差异料
    ('common_material', '共用料')]  # 共用料


class BomCompare(osv.osv.osv_memory):
    _name = 'bom.compare'
    _description = 'Bom Compare'

    bom_compare_ids = fields.Many2many('mrp.bom', string='Bom比对列表', required=True,
                                       ondelete='cascade')

    effect_date = fields.Datetime('有效时间')

    compare_type = fields.Selection(COMPARE_TYPE, '比对选项', default='all_material', required=True)

    show_raw_material = fields.Boolean('显示原材料', default=True)

    show_half_product = fields.Boolean('显示半成品', default=True)

    show_product_name = fields.Boolean('显示品名', default=True)

    show_single_tap = fields.Boolean('只显示单阶')

    @api.model
    def default_get(self, fields):
        record_ids = self.env.context.get('active_ids')
        res = {}
        if record_ids is not None:
            if len(record_ids) > 0:
                res['bom_compare_ids'] = self.env['mrp.bom'].browse(record_ids)
        res['compare_type'] = 'all_material'
        res['show_raw_material'] = 'True'
        res['show_half_product'] = 'True'
        res['show_product_name'] = 'True'
        return res

    @api.model
    def name_get(self):
        return [(bom.id, 'Bom比对清单') for bom in self]

    def action_compare_bom(self):
        self.ensure_one()
        if len(self.bom_compare_ids) > 5:
            raise ValidationError('目前对比BOM的数量不能大于5个!')
        return {
            "name": 'Bom对比表',
            'type': 'ir.actions.client',
            'tag': 'report_bom_compare',
            'context': {'model': 'report.bom.compare'},
        }

    def get_last_layer_bom(self, main_bom_id, bom_line_id, compare_bom_dict):
        # 1.当过滤条件是显示半成品，且不显示原材料，只过滤半成品的类型
        if self.show_half_product and not self.show_raw_material:
            if '半成品' in bom_line_id.product_tmpl_id.categ_id.name:
                compare_bom_dict.setdefault(bom_line_id.product_tmpl_id, {})[main_bom_id.id] = bom_line_id
                return
            if len(bom_line_id.child_line_ids) == 0:
                return

        # 2.当过滤条件是不显示半成品，且不显示原材料，过滤非半成品、非原材料的类型
        if not self.show_half_product and not self.show_raw_material and '半成品' not in bom_line_id.product_tmpl_id.categ_id.name and \
                '原材料' not in bom_line_id.product_tmpl_id.categ_id.name:
            compare_bom_dict.setdefault(bom_line_id.product_tmpl_id, {})[main_bom_id.id] = bom_line_id
            return

        if len(bom_line_id.child_line_ids) == 0:
            compare_bom_dict.setdefault(bom_line_id.product_tmpl_id, {})[main_bom_id.id] = bom_line_id
        else:
            for child_line_id in bom_line_id.child_line_ids:
                self.get_last_layer_bom(main_bom_id, child_line_id, compare_bom_dict)

    def get_compare_bom_data(self):
        self.ensure_one()
        compare_type = self.compare_type
        show_raw_material = self.show_raw_material
        show_half_product = self.show_half_product
        show_product_name = self.show_product_name
        show_single_tap = self.show_single_tap
        bom_count = len(self.bom_compare_ids)
        effect_date = self.effect_date

        # 1.获取比对物料
        compare_bom_dict = dict()
        for bom_compare_id in self.bom_compare_ids:
            for bom_line_id in bom_compare_id.bom_line_ids:
                # 判断是否显示单阶
                if show_single_tap is True:
                    if show_raw_material and '原材料' in bom_line_id.product_tmpl_id.categ_id.name or \
                            show_half_product and '半成品' in bom_line_id.product_tmpl_id.categ_id.name:
                        compare_bom_dict.setdefault(bom_line_id.product_tmpl_id, {})[bom_compare_id.id] = bom_line_id
                else:
                    self.get_last_layer_bom(bom_compare_id, bom_line_id, compare_bom_dict)

        # 2.移除过滤的物料
        filter_material_list = []
        for key, value in compare_bom_dict.items():
            if effect_date and key.effect_date and key.expire_date:
                if key.effect_date > effect_date or key.expire_date < effect_date:
                    filter_material_list.append(key)

            # 获取共用料
            if compare_type == 'common_material':
                if len(value) != bom_count:
                    filter_material_list.append(key)
            # 获取差异料
            elif compare_type == 'diff_material':
                if len(value) == bom_count:
                    filter_material_list.append(key)

        for item in filter_material_list:
            compare_bom_dict.pop(item)

        # 3.组织Bom列表信息
        bom_list = []
        for bom_compare_id in self.bom_compare_ids:
            bom_list.append({'id': bom_compare_id.id,
                             'name': bom_compare_id.product_tmpl_id.name,
                             'code': bom_compare_id.product_tmpl_id.default_code,
                             'product': bom_compare_id.product_tmpl_id})

        # 4.组织对比的物料信息
        compare_item_list = []
        for key, value in compare_bom_dict.items():
            compare_item = []
            for item in bom_list:
                product_qty = 0.0
                if item['id'] in value.keys():
                    bom_line = value[item['id']]
                    product_qty = bom_line.product_qty

                compare_item.append({'product_qty': product_qty})

            display_name = '[%s]' % key.default_code
            if show_product_name:
                display_name = key.display_name

            compare_item_list.append({'product_display_name': display_name,
                                      'product_id': key.id,
                                      'compare_item_list': compare_item
                                      })

        # 5.组织用户信息
        effect_date = ''
        if self.effect_date:
            effect_date = self.effect_date.strftime("%Y/%m/%d %H:%M:%S")

        create_date = ''
        if self.create_date:
            create_date = self.create_date.strftime("%Y/%m/%d %H:%M:%S")
        info_dict = {'effect_date': effect_date, 'create_date': create_date, 'user': self.env.user.name}


        lines = dict()
        lines['components'] = {
            'bom_list': bom_list,
            'compare_bom_list': compare_item_list,
            'info': info_dict}

        val = dict()
        val['lines'] = lines
        return val

    def get_compare_bom_html(self):
        val = self.get_compare_bom_data()
        val['lines']['report_type'] = 'html'
        val['lines'] = self.env.ref('bom_compare.report_bom_compare1')._render({'data': val['lines']})
        return val
