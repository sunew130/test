# -*- coding: utf-8 -*-

import json

from odoo import api, models, _
from odoo.tools import float_round

from odoo import api, models, _
from odoo.tools import float_round


class ReportBomCompare(models.AbstractModel):
    _name = 'report.bom_compare.report_bom_compare'
    _description = 'BOM Compare Report'

    @api.model
    def get_html(self, bom_compare_id):
        bom_compare = self.env['bom.compare'].browse(bom_compare_id)
        res = bom_compare.get_compare_bom_html()
        return res

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['bom.compare'].browse(docids).get_compare_bom_data()
        bom_compare_dict = docs['lines']
        bom_compare_dict['report_type'] = 'pdf'
        val = [bom_compare_dict]
        return {
            'doc_ids': docids,
            'doc_model': 'bom.compare',
            'docs': val,
        }
