﻿odoo.define('bom_compare.report_bom_compare', function (require) {
'use strict';

var core = require('web.core');
var framework = require('web.framework');
var stock_report_generic = require('stock.stock_report_generic');

var QWeb = core.qweb;
var _t = core._t;

var BomCompareReport = stock_report_generic.extend({
    events: {
        'click .o_bom_compare_action': '_onClickAction',
    },
    get_html: function() {
        var self = this;
        var args = [
            this.given_context.active_id,
        ];
        return this._rpc({
                model: 'report.bom_compare.report_bom_compare',
                method: 'get_html',
                args: args,
                context: this.given_context,
            })
            .then(function (result) {
                self.data = result;
            });
    },
    set_html: function() {
        var self = this;
        return this._super().then(function () {
            self.$('.o_content').html(self.data.lines);
            self.renderSearch();
            self.update_cp();
        });
    },
    render_html: function(event, $el, result){
        if (result.indexOf('mrp.document') > 0) {
            if (this.$('.o_mrp_has_attachments').length === 0) {
                var column = $('<th/>', {
                    class: 'o_mrp_has_attachments',
                    title: 'Files attached to the product Attachments',
                    text: 'Attachments',
                });
                this.$('table thead th:last-child').after(column);
            }
        }
        $el.after(result);
        $(event.currentTarget).toggleClass('o_mrp_bom_foldable o_mrp_bom_unfoldable fa-caret-right fa-caret-down');
    },
    update_cp: function () {
        var status = {
            cp_content: {
                $buttons: this.$buttonPrint,
            },
        };
        return this.updateControlPanel(status);
    },
    renderSearch: function () {
        this.$buttonPrint = $(QWeb.render('bom_compare.button'));
        this.$buttonPrint.find('.o_bom_compare_print').on('click', this._onClickPrint.bind(this));
    },
    _onClickPrint: function (ev) {
        framework.blockUI();
        var report_name = 'bom_compare.report_bom_compare?docids=' + this.given_context.active_id;
        var action = {
            'type': 'ir.actions.report',
            'report_type': 'qweb-pdf',
            'report_name': report_name,
            'report_file': 'bom_compare.report_bom_compare',
        };
        return this.do_action(action).then(function (){
            framework.unblockUI();
        });
    },
    _onClickAction: function (ev) {
        ev.preventDefault();
        return this.do_action({
            type: 'ir.actions.act_window',
            res_model: $(ev.currentTarget).data('model'),
            res_id: $(ev.currentTarget).data('res-id'),
            context: {
                'active_id': $(ev.currentTarget).data('res-id')
            },
            views: [[false, 'form']],
            target: 'current'
        });
    },
    });
core.action_registry.add('report_bom_compare', BomCompareReport);
return BomCompareReport;

});