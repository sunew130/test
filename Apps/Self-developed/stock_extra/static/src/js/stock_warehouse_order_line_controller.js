﻿odoo.define('stock_extra.StockWarehouseOrderLineController', function (require) {
    "use strict";

var ListView = require('web.ListView');
var viewRegistry = require('web.view_registry');
var StockOrderpointListController = require('stock.StockOrderpointListController');
var StockOrderpointListModel = require('stock.StockOrderpointListModel');

//这些是调⽤需要的模块
var OrderPointsListController =StockOrderpointListController.extend({
    renderButtons: function () {
        this._super.apply(this, arguments);
        if (this.$buttons) {
            //这⾥找到刚才定义的class名为update_supplier的按钮
            var btn = this.$buttons.find('.o_button_update_add_order');
            //给按钮绑定click事件和⽅法update_supplier
            btn.on('click', this.proxy('update_add_order'));

        }
    },
    update_add_order: function () {
        //通过rpc调⽤purchase.order 的button_update_supplier⽅法
        this._rpc({
            model: 'stock.warehouse.orderpoint',
            method: 'button_update_orderpoint',
            args: [],
        }).then(function () {
             location.reload();
        });
    },
});

var OrderPointsListController =StockOrderpointListController.extend({
    renderButtons: function () {
        this._super.apply(this, arguments);
        if (this.$buttons) {
            //这⾥找到刚才定义的class名为update_supplier的按钮
            var btn = this.$buttons.find('.o_button_update_add_order');
            //给按钮绑定click事件和⽅法update_supplier
            btn.on('click', this.proxy('update_add_order'));

        }
    },
    update_add_order: function () {

        //这⾥是获取tree视图中选中的数据的记录集
//        var records = _.map(self.selectedRecords, function (id) {
//            return self.model.localData[id];
//        });
//        console.log("数据id：" + _.pluck(records, 'res_id'));
        //获取到数据集中每条数据的对应数据库id集合
        //var ids = _.pluck(records, 'res_id');
        var ids = [];
        //通过rpc调⽤stock.warehouse.orderpoint 的 button_update_orderpoint⽅法
        this._rpc({
            model: 'stock.warehouse.orderpoint',
            method: 'button_update_orderpoint',
            args: [ids],
        }).then(function () {
             location.reload();
        });
    },
});

var StockOrderpointListView = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Controller: OrderPointsListController,
        Model: StockOrderpointListModel,
    }),
});


//这⾥⽤PurchaseOrderView，第⼀个字符串是注册名到时候需要根据注册名调⽤视图
viewRegistry.add('order_points_listview_extra', StockOrderpointListView);
return StockOrderpointListView;

});

