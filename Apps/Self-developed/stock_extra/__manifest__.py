# -*- coding: utf-8 -*-
{
    'name': "stock_extra",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "sunell",
    'website': "http://www.sunell.com.cn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product', 'barcodes', 'digest', 'stock', 'mrp'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/stock_data.xml',
        'views/users_view.xml',
        'security/security.xml',
        'security/stock_extra_security.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/stock_orderpoint_views.xml',
        'views/stock_crap_views.xml',
        'views/stock_move_views.xml',
        'views/stock_picking_views.xml'
    ],

    'qweb': [
        "static/src/xml/stock_warehouse_order_line_board.xml",
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',

    ],
}
