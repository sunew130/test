from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.float_utils import float_compare, float_is_zero, float_repr, float_round
from odoo.tools.misc import format_date, OrderedSet

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    origin = fields.Char(store=True)