from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.safe_eval import datetime, time


class stock_picking(models.Model):
    _inherit = "stock.picking"

    lot_id = fields.Many2one('stock.production.lot', related='move_line_ids.lot_id', readonly=True)

    @api.depends('move_type', 'immediate_transfer', 'move_lines.state', 'move_lines.picking_id')
    def _compute_state(self):
        super(stock_picking, self)._compute_state()
        for picking in self:
            if picking.state != 'done':
                continue
            self.build_outsourcing_purchase_order(picking)

    def build_outsourcing_purchase_order(self, picking):
        order = self.sudo().env['mrp.production'].search([('name', '=', picking.origin)])
        if order is not False:
            if order.x_outsourcing is True:
                #46 代表景阳库位，52代表生成后库位
                if picking.location_dest_id.id == 46 and picking.location_id.id == 52:
                    if len(picking.move_lines) > 0:
                        seller_id = None
                        # if len(picking.product_id.seller_ids) > 0:
                        #     seller_id = picking.product_id.seller_ids[0].id
                        # else:
                        #     seller_id = 50 #50代表虚拟供应商
                        seller_id = 50  # 50代表虚拟供应商
                        po = self.sudo().env['purchase.order'].create({
                            'partner_id': seller_id,
                            'date_approve': picking.create_date,
                            'date_planned': time.strftime('%Y-%m-%d %H:%M:%S'),
                            'origin': picking.origin,
                            'x_outsourcing': order.x_outsourcing,
                            # 'state': 'done',
                            'order_line': [
                                (0, 0, {
                                    'name': picking.product_id.name,
                                    'product_id': picking.product_id.id,
                                    'qty_received': picking.move_lines.product_qty,
                                    # 'qty_done': picking.move_lines.product_qty,
                                    'product_qty': picking.move_lines.product_qty,
                                    'product_uom': picking.product_id.uom_po_id.id,
                                    'price_unit': 0,
                                    # 'date_planned': datetime.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                }),
                            ]
                            })
                        # po.button_confirm()

