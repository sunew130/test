# -*- coding: utf-8 -*-

import json
from collections import defaultdict
from datetime import datetime
from itertools import groupby
from operator import itemgetter
from re import findall as regex_findall
from re import split as regex_split

from dateutil import relativedelta

from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.float_utils import float_compare, float_is_zero, float_repr, float_round
from odoo.tools.misc import format_date, OrderedSet

class StockMove(models.Model):
    _inherit = 'stock.move'

    to_purchase_product_qty = fields.Float(related='product_id.to_purchase_product_qty', string='To Purchase')

    to_product_product_qty = fields.Float(related='product_id.to_product_product_qty', string='To Product')

    to_check_product_qty = fields.Float(related='product_id.to_check_product_qty', string='To Check')

    to_on_hand_qty = fields.Float(related='product_id.qty_available', string='On hand')

    to_forecast_availability = fields.Float(compute='_compute_product_extra_qty', string='On Occupy')

    @api.onchange("product_id")
    def _compute_product_extra_qty(self):
            for move in self:
                out_domain = [('product_id', '=', move.product_id.id)]
                out_domain += [
                    '&',
                    ('location_id', '=', move.location_id.id),
                    ('location_dest_id', '=', move.location_dest_id.id),
                ]
                out_domain += [('state', 'not in', ['draft', 'cancel', 'done', 'waiting'])]
                # out_domain += [('reference', '!=', move.reference)]
                outs = self.env['stock.move'].search(out_domain, order='priority desc, date, id')
                forecast_qty_count = 0.0
                for item in outs:
                    if item.forecast_availability >= item.product_qty:
                        forecast_qty_count += item.product_qty
                    else:
                        forecast_qty_count += item.reserved_availability

                move.to_forecast_availability = forecast_qty_count