from odoo import api, fields, models


class StockScrap(models.Model):
    _inherit = 'stock.scrap'

    def do_scrap(self):
        self._check_company()
        for scrap in self:
            product_categ_name = scrap['product_id']['categ_id']['complete_name']
            if '原材料' in product_categ_name:
                scrap.name = self.env['ir.sequence'].next_by_code('stock.scrap.raw') or _('New')
            elif '成品' in product_categ_name:
                scrap.name = self.env['ir.sequence'].next_by_code('stock.scrap.product') or _('New')
            else:
                scrap.name = self.env['ir.sequence'].next_by_code('stock.scrap') or _('New')

            move = self.env['stock.move'].create(scrap._prepare_move_values())
            # master: replace context by cancel_backorder
            move.with_context(is_scrap=True)._action_done()
            scrap.write({'move_id': move.id, 'state': 'done'})
            scrap.date_done = fields.Datetime.now()
        return True

    @api.onchange('product_id')
    def onchange_product(self):
        if self._context.get('search_default_raw_scrap') == 1:
            return {
                'domain': {'product_id': [('type', 'in', ['product', 'consu']), '|', ('company_id', '=', False),
                                          ('company_id', '=', self.env.company.id),
                                          ('categ_id.complete_name', 'ilike', '原材料')]}
            }
        elif self._context.get('search_default_product_scrap') == 1:
            return {
                'domain': {'product_id': [('type', 'in', ['product', 'consu']), '|', ('company_id', '=', False),
                                          ('company_id', '=', self.env.company.id),
                                          ('categ_id.complete_name', 'ilike', '成品')]}
            }