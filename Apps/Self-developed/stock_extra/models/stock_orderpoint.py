# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from datetime import datetime, time
from dateutil import relativedelta
from itertools import groupby
from json import dumps
from psycopg2 import OperationalError

from odoo import SUPERUSER_ID, _, api, fields, models, registry
from odoo.addons.stock.models.stock_rule import ProcurementException
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools import float_compare, frozendict, split_every

class StockWarehouseOrderpoint(models.Model):
    _inherit = "stock.warehouse.orderpoint"

    order_number = fields.Char('Order number', store=True, readonly=False)
    order_number_product_qty = fields.Float('Order Demand', readonly=True, compute='_compute_qty_order_number')
    to_purchase_product_qty = fields.Float('To Purchase', store=True, readonly=True, compute='_compute_qty_order_number')
    to_product_product_qty = fields.Float('To Product', store=True, readonly=True, compute='_compute_qty_order_number')
    to_check_product_qty = fields.Float('To Check', store=True, readonly=True, compute='_compute_qty_order_number')

    order_number_create_time = fields.Datetime('oder create time', store=True, readonly=True, compute='_compute_order_info')
    order_number_schedule_time = fields.Datetime('oder schedule time', store=True, readonly=True, compute='_compute_order_info')
    order_number_delivery_time = fields.Datetime('oder  delivery time', store=True, readonly=True, compute='_compute_order_info')

    @api.depends('order_number')
    def _compute_qty_order_number(self):
        for orderpoint in self:
            out_domain = [('product_id', '=', orderpoint.product_id.id)]
            out_domain += [
                '&',
                ('location_id', '=', orderpoint.location_id.id),
                ('location_dest_id', '!=', orderpoint.location_id.id),
            ]
            out_domain += [('state', 'not in', ['draft', 'cancel', 'done'])]
            out_domain += [('origin', '=', orderpoint.order_number)]
            outs = self.env['stock.move'].search(out_domain, order='priority desc, date, id')

            orderpoint.order_number_product_qty = 0.0
            for item in outs:
                orderpoint.order_number_product_qty = item.product_qty

            orderpoint.to_purchase_product_qty = orderpoint.product_id['to_purchase_product_qty']
            orderpoint.to_product_product_qty = orderpoint.product_id['to_product_product_qty']
            orderpoint.to_check_product_qty = orderpoint.product_id['to_check_product_qty']
            if orderpoint.to_check_product_qty != orderpoint.product_id['to_check_product_qty']:
                orderpoint._compute_qty_to_order()

    @api.depends('order_number')
    def _compute_order_info(self):
        for orderpoint in self:
            domain = [('name', '=', orderpoint.order_number)]
            production = self.env['mrp.production'].search(domain, order='priority desc, id')
            production = production
            orderpoint.order_number_create_time = production.create_date
            orderpoint.order_number_schedule_time = production.date_planned_start
            orderpoint.order_number_delivery_time = production.date_planned_finished

    def action_replenish(self):

        self._procure_orderpoint_confirm(company_id=self.env.company)
        notification = False
        if len(self) == 1:
            notification = self._get_replenishment_order_notification()
        # Forced to call compute quantity because we don't have a link.
        self._compute_qty()
        #self.filtered(lambda o: o.create_uid.id == SUPERUSER_ID and o.qty_to_order <= 0.0 and o.trigger == 'manual').unlink()
        return notification

    @api.depends('qty_multiple', 'qty_forecast', 'product_min_qty', 'product_max_qty')
    def _compute_qty_to_order(self):
        for orderpoint in self:
            # 同一个产品有多个工单，当self只有一个时,需要返回
            # if len(self) == 1:
            #    if len(self.search([('product_id', '=', self.product_id.id)])) > 1:
            #        return

            # 根据工单号顺序遍历
            # 过滤完成或者取消的工单
            production = self.env['mrp.production'].search([('state', 'in', ['cancel', 'done'])]).ids
            ids = self.search([('order_number', 'in', production)]).ids

            domain = [('product_id', 'in', [w['id'] for w in self.product_id])]
            if len(ids) > 0:
                domain += [('id' not in ids)]
            orderpoint_dict = self.search(domain)
            orderpoint_dict = sorted(orderpoint_dict, key=lambda i: str(i.order_number) if i.order_number is not False else str(i.order_number_schedule_time))
            purchase_dict = {}

            orderpoint_move_dict = {}
            for orderpoint in orderpoint_dict:
                if not orderpoint.product_id or not orderpoint.location_id:
                    orderpoint.qty_to_order = False
                    continue

                qty_to_order = 0.0
                rounding = orderpoint.product_uom.rounding
                if float_compare(orderpoint.qty_forecast, orderpoint.product_min_qty, precision_rounding=rounding) < 0:
                    qty_to_order = max(orderpoint.product_min_qty, orderpoint.product_max_qty) - orderpoint.qty_forecast

                    remainder = orderpoint.qty_multiple > 0 and qty_to_order % orderpoint.qty_multiple or 0.0
                    if float_compare(remainder, 0.0, precision_rounding=rounding) > 0:
                        qty_to_order += orderpoint.qty_multiple - remainder
                orderpoint.qty_to_order = qty_to_order

                # 获取在手在制、在购、待检的预测的数量
                if orderpoint.product_id.id not in purchase_dict.keys():
                    purchase_dict[
                        orderpoint.product_id.id] = orderpoint.qty_on_hand + orderpoint.product_id.to_purchase_product_qty + orderpoint.product_id.to_check_product_qty + orderpoint.product_id.to_product_product_qty

                # 根据工单优先级，满足优先级高的订单，先消耗在制、在购、待检的预测的数量
                out_domain = [('product_id', '=', orderpoint.product_id.id)]
                out_domain += [
                    '&',
                    ('location_id', '=', orderpoint.location_id.id),
                    ('location_dest_id', '!=', orderpoint.location_id.id),
                ]
                out_domain += [('state', 'not in', ['draft', 'cancel', 'done'])]
                out_domain += [('origin', '=', orderpoint.order_number)]
                outs = self.env['stock.move'].search(out_domain, order='priority desc, date, id')
                for out in outs:
                    reserved_availability = 0.0
                    if out.forecast_availability >= out.product_qty:
                        reserved_availability += out.product_qty
                    else:
                        reserved_availability = out.reserved_availability

                    purchase_dict[orderpoint.product_id.id] -= reserved_availability
                    orderpoint_move_dict.update({outs: orderpoint})

            for outs, orderpoint in orderpoint_move_dict.items():
                if orderpoint.qty_to_order > 0.0:
                    reserved_availability = 0.0
                    for out in outs:
                        if out.forecast_availability >= out.product_qty:
                            reserved_availability += out.product_qty
                        else:
                            reserved_availability = out.reserved_availability

                        if purchase_dict[orderpoint.product_id.id] <= out.product_uom_qty:
                            orderpoint.qty_to_order = out.product_uom_qty - reserved_availability
                            if purchase_dict[orderpoint.product_id.id] > orderpoint.qty_to_order:
                                purchase_dict[orderpoint.product_id.id] -= orderpoint.qty_to_order
                                orderpoint.qty_to_order = 0.0
                            else:
                                orderpoint.qty_to_order -= purchase_dict[orderpoint.product_id.id]
                                purchase_dict[orderpoint.product_id.id] = 0.0
                        else:
                            purchase_dict[orderpoint.product_id.id] -= out.product_uom_qty
                            purchase_dict[orderpoint.product_id.id] += reserved_availability
                            orderpoint.qty_to_order = 0.0

    def _get_orderpoint_action(self):
        """Create manual orderpoints for missing product in each warehouses. It also removes
        orderpoints that have been replenish. In order to do it:
        - It uses the report.stock.quantity to find missing quantity per product/warehouse
        - It checks if orderpoint already exist to refill this location.
        - It checks if it exists other sources (e.g RFQ) tha refill the warehouse.
        - It creates the orderpoints for missing quantity that were not refill by an upper option.

        return replenish report ir.actions.act_window
        """
        action = self.env["ir.actions.actions"]._for_xml_id("stock.action_orderpoint_replenish")
        action['context'] = self.env.context
        orderpoints = self.env['stock.warehouse.orderpoint'].search([])
        # Remove previous automatically created orderpoint that has been refilled.
        to_remove = orderpoints.filtered(
            lambda o: o.create_uid.id == SUPERUSER_ID and o.qty_to_order <= 0.0 and o.trigger == 'manual')
        to_remove.unlink()
        orderpoints = orderpoints - to_remove
        #更新工单时间
        for item in orderpoints:
            item._compute_order_info()

        to_refill = defaultdict(float)
        qty_by_product_warehouse = self.env['report.stock.quantity'].read_group(
            [('date', '=', fields.date.today()), ('state', '=', 'forecast')],
            ['product_id', 'product_qty', 'warehouse_id'],
            ['product_id', 'warehouse_id'], lazy=False)
        for group in qty_by_product_warehouse:
            warehouse_id = group.get('warehouse_id') and group['warehouse_id'][0]
            if group['product_qty'] >= 0.0 or not warehouse_id:
                continue
            to_refill[(group['product_id'][0], warehouse_id)] = group['product_qty']
        if not to_refill:
            return action

        # Remove incoming quantity from other otigin than moves (e.g RFQ)
        product_ids, warehouse_ids = zip(*to_refill)
        # lot_stock_ids = [lot_stock_id_by_warehouse[w] for w in warehouse_ids]
        dummy, qty_by_product_wh = self.env['product.product'].browse(product_ids)._get_quantity_in_progress(
            warehouse_ids=warehouse_ids)
        rounding = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for (product, warehouse), product_qty in to_refill.items():
            qty_in_progress = qty_by_product_wh.get((product, warehouse)) or 0.0
            qty_in_progress += sum(orderpoints.filtered(
                lambda o: o.product_id.id == product and o.warehouse_id.id == warehouse
            ).mapped('qty_to_order'))
            # Add qty to order for other orderpoint under this warehouse.
            if not qty_in_progress:
                continue
            to_refill[(product, warehouse)] = product_qty + qty_in_progress
        to_refill = {k: v for k, v in to_refill.items() if float_compare(
            v, 0.0, precision_digits=rounding) < 0.0}

        lot_stock_id_by_warehouse = self.env['stock.warehouse'].search_read([
            ('id', 'in', [g[1] for g in to_refill.keys()]), ('active', 'in', [False, True])
        ], ['lot_stock_id'])
        lot_stock_id_by_warehouse = {w['id']: w['lot_stock_id'][0] for w in lot_stock_id_by_warehouse}

        product_qty_available = {}
        for warehouse, group in groupby(sorted(to_refill, key=lambda p_w: p_w[1]), key=lambda p_w: p_w[1]):
            products = self.env['product.product'].browse([p for p, w in group])
            products_qty_available_list = products.with_context(location=lot_stock_id_by_warehouse[warehouse]).mapped(
                'qty_available')
            product_qty_available.update({(p.id, warehouse): q for p, q in zip(products, products_qty_available_list)})

        orderpoint_values_list = []
        for (product, warehouse), product_qty in to_refill.items():
            lot_stock_id = lot_stock_id_by_warehouse[warehouse]
            orderpoint = self.filtered(lambda o: o.product_id == product and o.location_id == lot_stock_id)
            if orderpoint:
                orderpoint[0].qty_forecast += product_qty
            else:
                orderpoint_values = self.env['stock.warehouse.orderpoint']._get_orderpoint_values(product, lot_stock_id)
                orderpoint_values.update({
                    'name': _('Replenishment Report'),
                    'warehouse_id': warehouse,
                    'company_id': self.env['stock.warehouse'].browse(warehouse).company_id.id,
                })

            # orderpoint_values_list.append(orderpoint_values)

            out_domain = [('product_id', '=', product)]
            out_domain += [
                '&',
                ('location_id', '=', lot_stock_id),
                ('location_dest_id', '!=', lot_stock_id),
            ]
            out_domain += [('state', 'not in', ['draft', 'cancel', 'done'])]
            outs = self.env['stock.move'].search(out_domain, order='priority desc, date, id')

            purchase_dict = {}
            # order_number
            for item in outs:
                if item.product_id.id not in purchase_dict.keys():
                    purchase_dict[
                        item.product_id.id] = orderpoint.qty_on_hand + item.product_id.to_purchase_product_qty + item.product_id.to_check_product_qty + item.product_id.to_product_product_qty

                orderpoint_values.update({
                    'order_number': item.origin
                })
                qty_to_order = item.product_uom_qty
                if purchase_dict[item.product_id.id] > qty_to_order:
                    purchase_dict[item.product_id.id] -= qty_to_order
                    continue
                else:
                    purchase_dict[item.product_id.id] = 0.0

                existing_orderpoint = self.env['stock.warehouse.orderpoint'].search([
                    ('order_number', '=', item.origin),
                    ('product_id', '=', item.product_id.id)])

                if len(existing_orderpoint) == 0:
                    orderpoint_values1 = orderpoint_values.copy()
                    orderpoint_values_list.append(orderpoint_values1)

        orderpoints = self.env['stock.warehouse.orderpoint'].with_user(SUPERUSER_ID).create(orderpoint_values_list)
        for orderpoint in orderpoints:
            orderpoint.route_id = orderpoint.product_id.route_ids[:1]
        orderpoints.filtered(lambda o: not o.route_id)._set_default_route_id()
        return action

    def button_update_orderpoint(self):
        self.sudo().env['stock.warehouse.orderpoint'].search([('order_number', '!=', False)]).unlink()
        self.with_context(
            search_default_trigger='manual',
            search_default_filter_to_reorder=True,
            search_default_filter_not_snoozed=True,
            default_trigger='manual'
        ).action_open_orderpoints()
