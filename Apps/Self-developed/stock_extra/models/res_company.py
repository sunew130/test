from odoo import _, api, fields, models


class Company(models.Model):
    _inherit = "res.company"

    @api.model
    def create_scrap_sequence(self):
        company_ids = self.env['res.company'].search([])
        company_has_unbuild_seq = self.env['ir.sequence'].search([('code', '=', 'stock.scrap.raw')]).mapped('company_id')
        company_todo_sequence = company_ids - company_has_unbuild_seq

        scrap_vals = []
        for company in company_todo_sequence:
            scrap_vals.append({
                'name': '%s Sequence scrap' % company.name,
                'code': 'stock.scrap.raw',
                'company_id': company.id,
                'prefix': 'SP/RAW/',
                'padding': 5,
                'number_next': 1,
                'number_increment': 1
            })
            scrap_vals.append({
                'name': '%s Sequence scrap' % company.name,
                'code': 'stock.scrap.product',
                'company_id': company.id,
                'prefix': 'SP/PRO/',
                'padding': 5,
                'number_next': 1,
                'number_increment': 1
            })

        if scrap_vals:
            self.env['ir.sequence'].create(scrap_vals)

    def _create_scrap_sequence(self):
        super(Company, self)._create_scrap_sequence()
        self.create_scrap_sequence()