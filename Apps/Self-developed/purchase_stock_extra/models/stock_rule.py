from collections import defaultdict
from datetime import datetime
from dateutil.relativedelta import relativedelta
from itertools import groupby

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.addons.stock.models.stock_rule import ProcurementException

class StockRule(models.Model):
    _inherit = 'stock.rule'

    def _make_po_get_domain(self, company_id, values, partner, user_domain):
        """ Avoid to merge two RFQ for the same MPS replenish. """
        domain = super(StockRule, self)._make_po_get_domain(company_id, values, partner)

        for index, element in enumerate(domain):
            if element[0] == 'user_id' and user_domain:
                domain = domain[:index] + user_domain + domain[index + 1:]
                break

        return domain

    @api.model
    def _run_buy(self, procurements):

        procurements_by_po_domain = defaultdict(list)
        errors = []
        # Origins we don't want to appear in the PO source field.
        origins_to_hide = [
            _('Manual Replenishment'),
            _('Replenishment Report'),
        ]
        for procurement, rule in procurements:

            # Get the schedule date in order to find a valid seller
            procurement_date_planned = fields.Datetime.from_string(procurement.values['date_planned'])
            schedule_date = (procurement_date_planned - relativedelta(days=procurement.company_id.po_lead))

            supplier = False
            if procurement.values.get('supplierinfo_id'):
                supplier = procurement.values['supplierinfo_id']
            else:
                supplier = procurement.product_id.with_company(procurement.company_id.id)._select_seller(
                    partner_id=procurement.values.get("supplierinfo_name"),
                    quantity=procurement.product_qty,
                    date=schedule_date.date(),
                    uom_id=procurement.product_uom)

            # Fall back on a supplier for which no price may be defined. Not ideal, but better than
            # blocking the user.
            supplier = supplier or procurement.product_id._prepare_sellers(False).filtered(
                lambda s: not s.company_id or s.company_id == procurement.company_id
            )[:1]

            if not supplier:
                #获取虚拟供应商,产品没有填写供应商信息，就默认填写虚拟供应商
                supplier_temp = self.env['product.supplierinfo'].search([('name.name', 'ilike', 'VS'),('name.supplier_rank', '>', '0')], limit=1)
                if supplier_temp:
                    supplier = supplier_temp
                else:
                    msg = _('There is no matching vendor price to generate the purchase order for product %s (no vendor defined, minimum quantity not reached, dates not valid, ...). Go on the product form and complete the list of vendors.') % (procurement.product_id.display_name)
                    errors.append((procurement, msg))

            partner = supplier.name
            # we put `supplier_info` in values for extensibility purposes
            procurement.values['supplier'] = supplier
            procurement.values['propagate_cancel'] = rule.propagate_cancel


            user_domain = (('user_id', '=',  procurement[0]['product_tmpl_id']['x_purchase_user_id'].id),)
            domain = rule._make_po_get_domain(procurement.company_id, procurement.values, partner, user_domain)

            procurements_by_po_domain[domain].append((procurement, rule))

        if errors:
            raise ProcurementException(errors)

        for domain, procurements_rules in procurements_by_po_domain.items():
            # Get the procurements for the current domain.
            # Get the rules for the current domain. Their only use is to create
            # the PO if it does not exist.
            procurements, rules = zip(*procurements_rules)

            # Get the set of procurement origin for the current domain.
            origins = set([p.origin for p in procurements if p.origin not in origins_to_hide])

            # Check if a PO exists for the current domain.
            po = self.env['purchase.order'].sudo().search([dom for dom in domain], limit=1)
            company_id = procurements[0].company_id

            #判断请购的物料是否虚拟供应商，是则分开请购
            if domain and domain[0][2]:
                supplier_temp = self.env['product.supplierinfo'].search(
                    [('name.name', 'ilike', 'VS'), ('name.supplier_rank', '>', '0')], limit=1)
                if supplier_temp and int(domain[0][2]) == supplier_temp.name.id:
                    po = None

            if not po:
                # We need a rule to generate the PO. However the rule generated
                # the same domain for PO and the _prepare_purchase_order method
                # should only uses the common rules's fields.
                vals = rules[0]._prepare_purchase_order(company_id, origins, [p.values for p in procurements])

                if domain:
                    user_id = None
                    for element in domain:
                        if element[0] == 'user_id':
                            user_id = element[2]
                            break
                    if user_id:
                        vals['user_id'] = user_id

                # The company_id is the same for all procurements since
                # _make_po_get_domain add the company in the domain.
                # We use SUPERUSER_ID since we don't want the current user to be follower of the PO.
                # Indeed, the current user may be a user without access to Purchase, or even be a portal user.
                po = self.env['purchase.order'].with_company(company_id).with_user(SUPERUSER_ID).create(vals)
            else:
                # If a purchase order is found, adapt its `origin` field.
                if po.origin:
                    missing_origins = origins - set(po.origin.split(', '))
                    if missing_origins:
                        po.write({'origin': po.origin + ', ' + ', '.join(missing_origins)})
                else:
                    po.write({'origin': ', '.join(origins)})

            procurements_to_merge = self._get_procurements_to_merge(procurements)
            procurements = self._merge_procurements(procurements_to_merge)

            po_lines_by_product = {}
            grouped_po_lines = groupby(po.order_line.filtered(lambda l: not l.display_type and l.product_uom == l.product_id.uom_po_id).sorted(lambda l: l.product_id.id), key=lambda l: l.product_id.id)
            for product, po_lines in grouped_po_lines:
                po_lines_by_product[product] = self.env['purchase.order.line'].concat(*list(po_lines))
            po_line_values = []
            for index, procurement in enumerate(procurements):
                po_lines = po_lines_by_product.get(procurement.product_id.id, self.env['purchase.order.line'])
                po_line = po_lines._find_candidate(*procurement)

                if po_line:
                    # If the procurement can be merge in an existing line. Directly
                    # write the new values on it.
                    vals = self._update_purchase_order_line(procurement.product_id,
                        procurement.product_qty, procurement.product_uom, company_id,
                        procurement.values, po_line)
                    po_line.write(vals)
                else:
                    # If it does not exist a PO line for current procurement.
                    # Generate the create values for it and add it to a list in
                    # order to create it in batch.
                    partner = procurement.values['supplier'].name
                    po_line_values.append(self.env['purchase.order.line']._prepare_purchase_order_line_from_procurement(
                        procurement.product_id, procurement.product_qty,
                        procurement.product_uom, procurement.company_id,
                        procurement.values, po))

                    if index < len(procurements)-1:
                        # 判断请购的物料是否虚拟供应商，是则分开请购
                        if domain and domain[0][2]:
                            supplier_temp = self.env['product.supplierinfo'].search(
                                [('name.name', 'ilike', 'VS'), ('name.supplier_rank', '>', '0')], limit=1)
                            if supplier_temp and int(domain[0][2]) == supplier_temp.name.id:
                                po = None

                        if not po:
                            # We need a rule to generate the PO. However the rule generated
                            # the same domain for PO and the _prepare_purchase_order method
                            # should only uses the common rules's fields.
                            vals = rules[0]._prepare_purchase_order(company_id, origins, [p.values for p in procurements])

                            if domain:
                                user_id = None
                                for element in domain:
                                    if element[0] == 'user_id':
                                        user_id = element[2]
                                        break
                                if user_id:
                                    vals['user_id'] = user_id

                            # The company_id is the same for all procurements since
                            # _make_po_get_domain add the company in the domain.
                            # We use SUPERUSER_ID since we don't want the current user to be follower of the PO.
                            # Indeed, the current user may be a user without access to Purchase, or even be a portal user.
                            po = self.env['purchase.order'].with_company(company_id).with_user(SUPERUSER_ID).create(vals)

            self.env['purchase.order.line'].sudo().create(po_line_values)
