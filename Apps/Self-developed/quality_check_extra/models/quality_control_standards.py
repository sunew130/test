from odoo import api, models, fields
from odoo.exceptions import UserError


class ReceiveQualityLimit(models.Model):
    _name = 'receive.quality.limit'
    _description = '接收限质量'
    _rec_name = 'AQL'

    AQL = fields.Float('接受质量限', digits=(16, 3))


class SampleCode(models.Model):
    _name = 'sample.code'
    _description = '样本量字码'
    _rec_name = 'sample_code'

    min_qty = fields.Float('批量开始区间')
    max_qty = fields.Float('批量结束区间')
    sample_code = fields.Char('样本代码')
    sample_qty = fields.Float('样本量')
    _sql_constraints = [
        ('unique_sample_code', 'UNIQUE (sample_code)', '样本代码不能重复!'),
    ]

    @api.constrains('min_qty', 'max_qty', 'sample_code')
    def constrains_min_max_qty(self):
        for record in self:
            if record.max_qty < record.min_qty:
                raise UserError('开始区间不能大于结束区间,请重新设置!')
            if record.sample_code:
                if not record.sample_code.isalpha():
                    raise UserError('样本代码只能为字母!')
                else:
                    if record.sample_code == record.sample_code.upper():
                        return
                    else:
                        record.sample_code = record.sample_code.upper()


class QualityControlStandards(models.Model):
    _name = 'quality.control.standards'
    _description = '检验水准样本'
    _rec_name = 'AQL_id'

    sample_id = fields.Many2one('sample.code', string='批量')
    AQL_id = fields.Many2one('receive.quality.limit', string='质量接收限')
    accept_qty = fields.Float('接受数量')
    refuse_qty = fields.Float('拒绝数量')
    sample_standard_id = fields.Many2one('sample.standards', string='抽样标准')


class SampleStandards(models.Model):
    _name = 'sample.standards'
    _description = '抽样标准设置'

    name = fields.Char('标准名称')
    standard_ids = fields.One2many('quality.control.standards', 'sample_standard_id', string='检验水准')


