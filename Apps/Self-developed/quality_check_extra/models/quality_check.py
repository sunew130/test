from datetime import datetime
from odoo.tools.float_utils import float_compare
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import operator
from io import BytesIO
import xlsxwriter
import base64

Ope = {"<": operator.lt, "<=": operator.le, "==": operator.eq,
       ">": operator.gt, ">=": operator.ge, "!=": operator.ne}
NONCRITICAL_RESULT_LIST = [('1', '合格'), ('2', '退回'), ('3', '特采')]
STATE = [('wait_approval', '等待审核'), ('first_approval', '一审'), ('approval', '审核通过')]


class QualityCheck(models.Model):
    _inherit = 'quality.check'

    default_code = fields.Char(related='product_id.default_code', string='内部参考')
    product_size = fields.Text(related='product_id.description', string='品名规格')
    inspection_level = fields.Many2one('quality.control.standards', string='检验水准', compute='_compute_inspection_level',
                                       store=True)
    sample_standard_id = fields.Many2one('sample.standards', string='抽样检验标准')
    categ_id = fields.Many2one(related='product_id.product_tmpl_id.categ_id', string='产品类别')
    IQC_code = fields.Many2one('sample.code', string='IQC检验代码')
    qualified_qty = fields.Float(string='合格数量', tracking=True)
    noncritical_result = fields.Selection(string='判定结果', selection=NONCRITICAL_RESULT_LIST, tracking=True,
                                          compute='_compute_noncritical_result')
    special_purchase = fields.Boolean('特采', default=False)
    check_user = fields.Many2one('res.users', string='检验员', tracking=True)
    check_qty = fields.Float('送验量', tracking=True)
    line_ids = fields.One2many('quality.check.line', 'check_id', string='质检明细')
    state = fields.Selection(selection=STATE, string='审批状态', default='wait_approval', tracking=True)
    purchase_id = fields.Many2one('purchase.order', string='采购单')
    in_check_picking_id = fields.Many2one('stock.picking', string='质检收货单', tracking=True)
    AQL = fields.Many2one('receive.quality.limit', string='AQL')
    query_result = fields.Binary('检验报告')

    @api.depends('AQL', 'IQC_code')
    def _compute_inspection_level(self):
        for rec in self:
            qcs_id = self.env['quality.control.standards'].search(
                [('AQL_id', '=', rec.AQL.id), ('sample_id', '=', rec.IQC_code.id)], limit=1)
            rec.inspection_level = qcs_id.id
            standard_id = self.sample_standard_id.search([('standard_ids', 'in', qcs_id.id)], limit=1)
            rec.sample_standard_id = standard_id.id

    @api.depends('line_ids')
    def _compute_noncritical_result(self):
        for record in self:
            if record.special_purchase:
                record.noncritical_result = '3'
                # 特采默认全部合格
                record.qualified_qty = record.check_qty
            else:
                if record.line_ids:
                    # 不合格全部退货
                    if any(list(filter(lambda x: x.noncritical_id.id == 2, record.line_ids))):
                        record.noncritical_result = '2'
                        # record.qualified_qty = 0
                    elif any(list(filter(lambda x: x.noncritical_id == False, record.line_ids))):
                        record.noncritical_result = False
                        # record.qualified_qty = 0
                    else:
                        # 合格全部入库
                        record.noncritical_result = '1'
                        # record.qualified_qty = record.check_qty
                else:
                    record.noncritical_result = False
                    # record.qualified_qty = 0

    @api.constrains('check_qty', 'qualified_qty')
    def check_qualified_qty(self):
        for check_id in self:
            if check_id.qualified_qty and float_compare(check_id.check_qty, check_id.qualified_qty,
                                                        precision_rounding=check_id.product_id.uom_id.rounding) == -1:
                raise UserError('质检合格数量不能大于质检数量!')

    @api.model
    def create(self, vals_list):
        res = super(QualityCheck, self).create(vals_list)
        self.sudo()._update_vals(res)
        return res

    @api.model
    def _update_vals(self, res):
        """
        创建质检记录时, 继承性修改values, 添加原来没有的参数
        @param res: _(New)
        @return: none
        """
        move_ids = res.picking_id.move_lines.filtered(lambda x: x.product_id == res.product_id)
        # 送检数量为需求数量 - 完成数量
        res.check_qty = sum(move_ids.mapped('product_uom_qty')) - sum(move_ids.mapped('quantity_done'))
        # 是否设置检验项目
        domain = [('product_ids', 'in', res.product_id.id),
                  ('picking_type_ids', 'in', res.picking_id.picking_type_id.id)]
        point_ids = self.env['quality.point'].search(domain)
        if len(point_ids) > 1:
            raise UserError('产品控制点不能重复设置作业类型!')
        items = point_ids.item_ids
        for item in items:
            values = {
                'check_id': res.id,
                'item_id': item.id,
                'product_id': res.product_id.id,
            }
            res.line_ids = [(0, 0, values)]
        if res.picking_id.origin:
            if res.picking_id.origin.startswith('P'):
                # 采购单
                purchase_id = self.env['purchase.order'].search([('name', 'ilike', res.picking_id.origin)], limit=1)
                res.purchase_id = purchase_id.id if purchase_id else False
            if 'SN/MO' in res.picking_id.origin:
                # 制造单
                production_id = self.env['mrp.production'].search([('name', 'ilike', res.picking_id.origin)], limit=1)
                res.production_id = production_id.id if production_id else False
            # 确认销售order发现bug
            if res.picking_id.origin.startswith('S'):
                # 如果是mo, 此时已经产生了move_orig_ids, 如果是po, 还没有产生
                if res.picking_id.move_ids_without_package.move_orig_ids:
                    res.production_id = res.picking_id.move_ids_without_package.move_orig_ids.production_id.id
                    res.purchase_id = False
                else:
                    purchase_id = self.env['purchase.order'].search([('group_id', '=', res.picking_id.group_id.id)])
                    # purchase_id = self.env['purchase.order'].search([('origin', 'ilike', res.picking_id.origin)])
                    if len(purchase_id) > 1:
                        raise UserError('创建质检记录查找采购单出错, 采购单数量超出!')
                    else:
                        res.purchase_id = purchase_id.id
                        res.production_id = False

    def action_first_approval(self):
        """只是标记为审核状态"""
        self.state = 'first_approval'
        # 规避原有状态报错
        self.write({'quality_state': 'pass',
                    'user_id': self.env.user.id,
                    'control_date': datetime.now()})
        self.check_user = self.env.user.id

    def action_cancel_first_approval(self):
        """标记状态由一审变成待审批状态"""
        self.state = 'wait_approval'
        self.write({'quality_state': 'none',
                    'user_id': self.env.user.id,
                    'control_date': datetime.now()})

    def action_approval(self):
        """ 完成审核, 入库操作 """
        action = self.sudo().operate_transfers()
        if action:
            return action
        if self.state == 'approval':
            self.write({'control_date': fields.Datetime.now()})
            self.message_post(body='入库完成, 操作员:{}'.format(self.env.user.partner_id.name))

    def operate_transfers(self):
        """ 完成入库, 操作调拨单, 欠单"""
        # 先检查可用量, 防止找不到move_id, 如果找不到可能是收货单没有完成
        self.picking_id.action_assign()
        if self.purchase_id:
            move_id = self.picking_id.move_line_ids_without_package.filtered(
                lambda x: x.product_id == self.product_id)
            if not move_id:
                raise UserError('此单的收货单未完成, 请先完成收货, 单号为{}'.format(
                    self.in_check_picking_id.name if self.in_check_picking_id else self.purchase_id.name))
            # 只有一种情况是点击全部完成数量(当前调拨单的产品只剩这一个,并且合格数量等于检验数量), 其余的全部是创建欠单
            if set(self.product_id) == set(
                    self.picking_id.move_line_ids_without_package.mapped(
                        'product_id')) and self.qualified_qty == self.check_qty:
                self.write({'quality_state': 'pass',
                            'user_id': self.env.user.id,
                            'control_date': datetime.now()})
                if self.special_purchase:
                    # 特采的话全部完成
                    move_id.write({'qty_done': self.check_qty})
                else:
                    move_id.write({'qty_done': self.qualified_qty})
                self.picking_id.button_validate()
                # self.state = 'approval'
            else:
                # 合格数量为0,不会创建欠单, 如果是特采将会创建欠单
                if self.qualified_qty == 0:
                    if not self.special_purchase:
                        backorder_id = False
                        action = self.env["ir.actions.actions"]._for_xml_id(
                            "quality_check_extra.create_return_order_confirm_wizard_view_action")
                        action['target'] = 'new'
                        action['context'] = {'move_id': move_id.id}
                        return action
                    else:
                        backorder_id = self._create_backorder(move_id=move_id)

                # 合格数量不是0,(单个或者多个产品)需要创建欠单(上面已经判断, 如果不为0但是只有单个产品并且合格与质检数量相同也不会创建欠单)
                else:
                    backorder_id = self._create_backorder(move_id=move_id)
                    # self.state = 'approval'
                if backorder_id:
                    # 创建新的质检记录
                    for line in backorder_id.move_ids_without_package:
                        # 判断产品是否需要质检
                        qp = self.env['quality.point'].search(
                            [('picking_type_ids', 'in', backorder_id.picking_type_id.id),
                             ('product_ids', 'in', line.product_id.id)])
                        if qp and len(qp) == 1:
                            qc = self.env['quality.check']
                            values = {
                                'product_id': line.product_id.id,
                                'picking_id': backorder_id.id,
                                'point_id': qp.id,
                                'check_qty': line.product_uom_qty - line.quantity_done,
                                'team_id': 1,
                                'test_type_id': 1
                            }
                            qc.create(values)
                    backorder_id.action_assign()
                # 是否需要创建退货单: (检验数量=合格数量就不需要创建, 不相等则需要创建退货单,并且创建相应的收货单, 特采也是直接完成所有数量不会创建退货单)
                if not self.special_purchase:
                    self._create_return_order(move_id=move_id, backorder_id=backorder_id)
            # self.state = 'approval'
        else:
            ###############################################################################################
            # 1.合格数量为0, 特采全部入库,不是特采(代表全部返工,创建返工单,数量为全部,MO不标记为完成)
            # 2.合格数量等于质检数量, 直接全部入库, 不管特采与否
            # 3.合格数量不为0也不等于质检数量, 完成数量为合格数量, 创建欠单, 拆分入库move
            # 4.拆分stock_move会出现生产数量不是正数的问题,因为系统找到了002欠单,此时生产数量为0, 改变关联规避(可能会出问题)
            ###############################################################################################
            if self.qualified_qty == 0:
                if not self.special_purchase:
                    values = {
                        'product_id': self.product_id.id,
                        'production_id': self.production_id.id,
                        'rework_qty': self.check_qty,
                        'check_id': self.id
                    }
                    rework_id = self.env['mrp.rework'].create(values)
                    self.production_id.rework_ids = [(4, rework_id.id)]
                    self.message_post(body='创建返工单完成, 返工数量为{}'.format(self.check_qty))
                    # self.state = 'wait_approval'
                else:
                    # 特采全部入库
                    self.all_charge()
            else:
                # 质检数量不等于合格数量, 创建返工单,不能标记为完成
                if self.qualified_qty != self.check_qty:
                    if not self.special_purchase:
                        # 生产单创建欠单, 重新创建质检记录,关联创建的backorder
                        self.production_id.qty_producing = self.qualified_qty
                        self.production_id._onchange_producing()
                        self.production_id.button_mark_done()
                        values = {
                            'mrp_production_ids': [(6, 0, [self.production_id.id])],
                            'show_backorder_lines': False
                        }
                        mpb_id = self.env['mrp.production.backorder'].create(values)
                        mpb_id.mrp_production_backorder_line_ids = [(0, 0, {
                            'mrp_production_backorder_id': mpb_id.id,
                            'mrp_production_id': self.production_id.id,
                            'to_backorder': True
                        })]
                        action = mpb_id.action_backorder()
                        res_id = action['res_id']
                        new_production_id = self.production_id.browse(res_id)
                        values = {
                            'product_id': self.product_id.id,
                            'rework_qty': self.check_qty - self.qualified_qty,
                            'check_id': self.id
                        }
                        rework_id = self.env['mrp.rework'].create(values)
                        self.production_id.rework_ids = [(4, rework_id.id)]
                        self.message_post(body='创建返工单完成, 返工数量为{}'.format(values.get('rework_qty')))
                        # 拆分stock_move,成品种类只有一种, 目前这个问题能处理(fixme 可能会出bug)
                        move_vals = self.picking_id.move_ids_without_package._split(
                            qty=(self.check_qty - self.qualified_qty))
                        orig_id = self.env['stock.move'].browse(
                            sorted(self.picking_id.move_ids_without_package.move_orig_ids.ids)[0])
                        self.picking_id.move_ids_without_package.move_orig_ids = [orig_id.id]
                        # 验证调拨单
                        self.picking_id.action_assign()
                        move_id = self.picking_id.move_line_ids_without_package.filtered(
                            lambda x: x.product_id == self.product_id)
                        move_id.qty_done = self.qualified_qty
                        self.picking_id.button_validate()
                        new_move_id = self.env['stock.move'].create(move_vals[0])
                        new_orig_id = self.env['stock.move'].browse(
                            sorted(new_move_id.move_orig_ids.ids)[-1])
                        new_move_id.move_orig_ids = [new_orig_id.id]
                        new_picking_id = self.picking_id.copy()
                        new_picking_id['move_ids_without_package'] = [new_move_id.id]
                        new_picking_id.action_confirm()
                        # 查找质检记录
                        sql = """SELECT ID FROM quality_check WHERE picking_id=%s AND product_id= %s""" % (
                            new_picking_id.id, self.product_id.id)
                        self._cr.execute(sql)
                        qc_ids = self.env['quality.check'].browse([rec[0] for rec in self._cr.fetchall()])
                        check_ids = qc_ids - self
                        for check_id in check_ids:
                            check_id.production_id = new_production_id.id
                        # self.state = 'approval'
                    else:
                        self.all_charge()
                else:
                    self.all_charge()
        self.state = 'approval'

    def all_charge(self):
        if self.production_id.state not in ['confirmed', 'progress', 'to_close']:
            raise UserError('制造单不能标记为完成!')
        self.production_id.qty_producing = self.check_qty
        self.production_id._onchange_producing()
        self.production_id.button_mark_done()
        self.picking_id.action_assign()
        move_id = self.picking_id.move_line_ids_without_package.filtered(
            lambda x: x.product_id == self.product_id)
        if not move_id:
            raise UserError('请先完成制造单{}'.format(self.production_id.name))
        move_id.write({'qty_done': self.check_qty})
        self.picking_id.button_validate()
        self.state = 'approval'

    def _create_return_order(self, move_id, backorder_id=False):
        if move_id.move_id.move_orig_ids and float_compare(self.check_qty, self.qualified_qty,
                                                           precision_rounding=self.product_id.uom_id.rounding) != 0:
            # fixme 可能会出现多张前置移动, 目前测试是第一张收货跟第二张收货(不过发现退随便一张都可以, 退最原始的一张, 防止产品不足的情况出现)
            orig_list = move_id.move_id.move_orig_ids.mapped('id')
            orig_list.sort()
            return_id = move_id.move_id.browse(orig_list[0]).picking_id
            values = {'picking_id': return_id.id}
            srp = self.env['stock.return.picking'].create(values)
            srp._onchange_picking_id()
            return_move_id = srp.product_return_moves.filtered(lambda x: x.product_id == self.product_id)
            srp.product_return_moves = [(6, 0, [return_move_id.id])]
            # 修改退货单向导明细数量
            return_move_id.quantity = self.check_qty - self.qualified_qty
            action = srp.create_returns()
            # 退货单创建
            new_picking_id = self.env['stock.picking'].browse(action['res_id'])
            # 退货单检查可用量
            new_picking_id.action_assign()
            if new_picking_id.state != 'assigned':
                raise UserError('没有足够的库存进行退货!')
            # 验证退货单 fixme 这个退货调拨没有move_line, 所以直接完成这个move_ids_without_package, 暂时不会出现问题
            for line in new_picking_id.move_ids_without_package:
                line.quantity_done = line.product_uom_qty
            new_picking_id.button_validate()
            # 创建收货单
            in_picking_id = self.with_context(
                new_picking_id=new_picking_id.id, product_id=self.product_id.id, backorder_id=backorder_id.id
                if backorder_id else False).create_purchase_stock_picking()
            # new_picking_id.purchase_id.picking_ids = [(4, in_picking_id.id)]
            in_picking_id.with_context(ignore_check=True, backorder_id=backorder_id).action_confirm()
            if backorder_id:
                line_id = backorder_id.move_ids_without_package.filtered(lambda p: p.product_id == self.product_id)
                line_id.move_orig_ids = [(6, 0, in_picking_id.move_lines.ids)]
                # 欠单对应的质检记录
                sql = """SELECT ID FROM quality_check WHERE picking_id=%s AND product_id= %s""" % (
                    backorder_id.id, self.product_id.id)
                self._cr.execute(sql)
                qc_ids = self.env['quality.check'].browse([rec[0] for rec in self._cr.fetchall()])
                for qc_id in qc_ids:
                    qc_id.in_check_picking_id = in_picking_id.id
            else:
                # 没有欠单, 但也退货了
                line_id = self.picking_id.move_ids_without_package.filtered(lambda p: p.product_id == self.product_id)
                line_id.move_orig_ids = [(6, 0, in_picking_id.move_lines.ids)]
                self.in_check_picking_id = in_picking_id.id

            if self.qualified_qty == 0:
                self.state = 'wait_approval'

    def _create_backorder(self, move_id):
        move_id.write({'qty_done': self.qualified_qty if not self.special_purchase else self.check_qty})
        self.picking_id.button_validate()
        tem_values = {'pick_ids': [[6, 0, [self.picking_id.id]]], 'show_transfers': False,
                      'backorder_confirmation_line_ids': [
                          [0, 0, {'picking_id': self.picking_id.id, 'to_backorder': True}]]}
        sql = """ SELECT id FROM quality_check WHERE picking_id=%s""" % self.picking_id.id
        self._cr.execute(sql)
        need_unlink_ids = [rec[0] for rec in self._cr.fetchall()]
        unlink_ids = self.browse(need_unlink_ids) - self
        unlink_ids.unlink()
        self.write({'quality_state': 'pass',
                    'user_id': self.env.user.id,
                    'control_date': datetime.now()})
        bc_id = self.env['stock.backorder.confirmation'].create(tem_values)
        context = {
            'active_id': self.picking_id.id,
            'active_ids': [self.picking_id.id], 'search_default_available': 1,
            'search_default_picking_type_id': [self.picking_id.picking_type_id.id],
            'default_picking_type_id': self.picking_id.picking_type_id.id,
            'button_validate_picking_ids': [self.picking_id.id],
            'default_show_transfers': False, 'default_pick_ids': [[4, self.picking_id.id]]}
        bc_id.sudo().with_context(context).process()
        # 验证过后创建的欠单重新关联新建的质检单
        backorder_id = self.env['stock.picking'].search([('backorder_id', '=', self.picking_id.id)])
        return backorder_id

    @api.model
    def create_purchase_stock_picking(self):
        """模拟创建传入参数"""
        picking_type_id = self.env['stock.picking.type'].search([('name', '=', '收货')], limit=1)
        new_picking_id = self.env['stock.picking'].browse(self._context.get('new_picking_id', False))
        product_id = self.env['product.product'].browse(self.env.context.get('product_id', False))
        values = {'is_locked': True, 'partner_id': new_picking_id.purchase_id.partner_id.id,
                  'picking_type_id': picking_type_id.id,
                  'location_id': new_picking_id.location_dest_id.id,
                  'location_dest_id': picking_type_id.default_location_dest_id.id,
                  'origin': new_picking_id.purchase_id.name,
                  'move_ids_without_package': []}
        lines = [0, 0, {'state': 'draft',
                        'picking_type_id': picking_type_id.id,
                        'purchase_line_id': new_picking_id.purchase_id.order_line.filtered(
                            lambda x: x.product_id == product_id).id,
                        'location_id': values['location_id'],
                        'location_dest_id': picking_type_id.default_location_dest_id.id,
                        'product_id': product_id.id,
                        'date': fields.Datetime.now(),
                        'product_uom_qty': sum(new_picking_id.move_ids_without_package.filtered(
                            lambda p: p.product_id == product_id).mapped('quantity_done')),
                        'product_uom': product_id.uom_id.id, 'description_picking': product_id.default_code,
                        'name': product_id.display_name
                        }]
        values['move_ids_without_package'].append(lines)
        in_picking_id = self.env['stock.picking'].create(values)
        return in_picking_id

    def confirm_special_purchase(self):
        """标记为特采, 需要经过领导审批之后才能进行入库"""
        self.special_purchase = True
        self.noncritical_result = '3'

    def confirm_cancel_special_purchase(self):
        """取消标记为特采"""
        self.special_purchase = False
        self.noncritical_result = False

    def unlink(self):
        # 只能删除草稿状态的质检单(建议不能删除所有单据, 除非不进行质检, 否则可能导致质检失效)
        for rec in self:
            if rec.state != 'wait_approval':
                raise UserError('只能删除草稿状态的质检单!')
        return super(QualityCheck, self).unlink()

    def print_check_result(self):
        """ 打印QA检验报告 """
        self.ensure_one()
        return self.env.ref('quality_check_extra.report_quality_check_result').report_action(self)

    def report_incoming_quality_check_result(self):
        """ 打印来料检验报告 """
        self.ensure_one()
        return self.env.ref('quality_check_extra.report_incoming_quality_check_result').report_action(self)

    def prepare_excel_values(self):
        file_data = BytesIO()
        workbook = xlsxwriter.Workbook(file_data)
        sheet = workbook.add_worksheet()
        sheet.set_default_row(25)

        sheet.set_column(0, 100, 7)

        title_style = workbook.add_format({
            'font_size': 14,  # 字体大小
            'bold': False,  # 加粗
            'border': 0,  # 边框宽度
            'align': 'center',  # 水平居中
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': False,  # 是否自动换行
        })
        title_style2 = workbook.add_format({
            'font_size': 22,  # 字体大小
            'bold': False,  # 加粗
            'border': 0,  # 边框宽度
            'align': 'center',  # 水平居中
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': False,  # 是否自动换行
        })
        body_style = workbook.add_format({
            'font_size': 10,  # 字体大小
            'bold': False,  # 加粗
            'border': 0,  # 边框宽度
            'align': 'center',  # 水平居中
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': False,  # 是否自动换行
        })
        body_style2 = workbook.add_format({
            'font_size': 10,  # 字体大小
            'bold': False,  # 加粗
            'border': 1,  # 边框宽度
            'align': 'center',  # 水平居中
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': False,  # 是否自动换行
        })

        body_style3 = workbook.add_format({
            'font_size': 10,  # 字体大小
            'bold': False,  # 加粗
            'border': 0,  # 边框宽度
            'left': True,
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': True,  # 是否自动换行
        })

        body_style4 = workbook.add_format({
            'font_size': 10,  # 字体大小
            'bold': False,  # 加粗
            'border': 1,  # 边框宽度
            'align': 'center',  # 水平居中
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': True,  # 是否自动换行
        })

        body_style5 = workbook.add_format({
            'font_size': 10,  # 字体大小
            'bold': False,  # 加粗
            'border': 1,  # 边框宽度
            'left': True,
            'valign': 'vcenter',  # 垂直居中
            'text_wrap': False,  # 是否自动换行
        })
        # 来料检验xlsx
        if self.purchase_id:
            sheet.merge_range('D2:J2', self.env.company.name, title_style)
            sheet.write('M2', '密级: P', title_style)

            sheet.merge_range('A3:M3', '来料检验合格报告', title_style2)
            sheet.write('K4', '编号:', body_style)
            sheet.merge_range('L4:M4', '＿', body_style)

            sheet.write('A5', '供应商', body_style2)
            sheet.merge_range('B5:C5', self.purchase_id.partner_id.name, body_style2)
            sheet.merge_range('D5:E5', '来料日期', body_style2)
            sheet.merge_range('F5:H5', self.create_date.strftime('%Y-%m-%d %H:%M'), body_style2)
            sheet.merge_range('I5:J5', '来料料号', body_style2)
            sheet.merge_range('K5:M5', self.product_id.default_code, body_style2)
            sheet.write('A6', '来料名称', body_style2)
            sheet.merge_range('B6:C6', self.product_id.name, body_style2)
            sheet.merge_range('D6:E6', '来料数量', body_style2)
            sheet.merge_range('F6:H6', self.check_qty, body_style2)
            sheet.merge_range('I6:J6', '抽检数量', body_style2)
            # 抽样水准找样本量字码的抽样数量
            sample_qty = self.inspection_level.sample_id.sample_qty
            sheet.merge_range('K6:M6', sample_qty, body_style2)
            sheet.merge_range('A7:C7', '抽样依据', body_style2)
            sheet.merge_range('D7:E8', 'AQL', body_style2)
            sheet.write('F7', 'CR', body_style2)
            sheet.write('G7', 'MAJ', body_style2)
            sheet.write('H7', 'MIN', body_style2)
            sheet.merge_range('I7:J8', '附件', body_style2)
            sheet.merge_range('K7:M8', '供应商检验报告\n□有   份\n□无', body_style2)
            sheet.merge_range('A8:C8', 'GB2828-2012', body_style2)
            sheet.write('F8', '', body_style2)
            sheet.write('G8', '', body_style2)
            sheet.write('H8', '', body_style2)
            sheet.merge_range('A9:B9', '检验项目', body_style2)
            sheet.merge_range('C9:F9', '检验标准', body_style2)
            sheet.merge_range('G9:H9', '使用仪器', body_style2)
            sheet.merge_range('I9:L9', '检验结果', body_style2)
            sheet.write('M9', '判定', body_style2)
            index = 10
            for line in self.line_ids:
                sheet.merge_range('A%s:B%s' % (index, index), line.item_id.name, body_style2)
                sheet.merge_range('C%s:F%s' % (index, index), '', body_style2)
                sheet.merge_range('G%s:H%s' % (index, index), '', body_style2)
                sheet.merge_range('I%s:L%s' % (index, index), '', body_style2)
                sheet.write('M%s' % index, '', body_style2)
                index += 1
            sheet.merge_range('A%s:A%s' % (str(index), str(index + 1)), '尺寸标准', body_style2)
            sheet.merge_range('B%s:B%s' % (str(index), str(index + 1)), '检验工具', body_style2)
            sheet.merge_range('C%s:L%s' % (str(index), str(index)), '检验结果', body_style2)
            sheet.merge_range('M%s:M%s' % (str(index), str(index + 1)), '判定', body_style2)
            sheet.write('C%s' % str(index + 1), str(1), body_style2)
            sheet.write('D%s' % str(index + 1), str(2), body_style2)
            sheet.write('E%s' % str(index + 1), str(3), body_style2)
            sheet.write('F%s' % str(index + 1), str(4), body_style2)
            sheet.write('G%s' % str(index + 1), str(5), body_style2)
            sheet.write('H%s' % str(index + 1), str(6), body_style2)
            sheet.write('I%s' % str(index + 1), str(7), body_style2)
            sheet.write('J%s' % str(index + 1), str(8), body_style2)
            sheet.write('K%s' % str(index + 1), str(9), body_style2)
            sheet.write('L%s' % str(index + 1), str(10), body_style2)
            new_index = index + 2
            i = 0
            while i < 5:
                sheet.write('A%s' % str(new_index), '', body_style2)
                sheet.write('B%s' % str(new_index), '', body_style2)
                sheet.write('C%s' % str(new_index), '', body_style2)
                sheet.write('D%s' % str(new_index), '', body_style2)
                sheet.write('E%s' % str(new_index), '', body_style2)
                sheet.write('F%s' % str(new_index), '', body_style2)
                sheet.write('G%s' % str(new_index), '', body_style2)
                sheet.write('H%s' % str(new_index), '', body_style2)
                sheet.write('I%s' % str(new_index), '', body_style2)
                sheet.write('J%s' % str(new_index), '', body_style2)
                sheet.write('K%s' % str(new_index), '', body_style2)
                sheet.write('L%s' % str(new_index), '', body_style2)
                sheet.write('M%s' % str(new_index), '', body_style2)
                i += 1
                new_index += 1

            sheet.merge_range('A%s:B%s' % (str(index + 7), str(index + 7)), '试装验证', body_style2)
            sheet.merge_range('C%s:F%s' % (str(index + 7), str(index + 7)), '', body_style2)
            sheet.merge_range('G%s:H%s' % (str(index + 7), str(index + 7)), '', body_style2)
            sheet.merge_range('I%s:L%s' % (str(index + 7), str(index + 7)), '', body_style2)
            sheet.write('M%s' % str(index + 7), '', body_style2)

            sheet.merge_range('A%s:B%s' % (str(index + 8), str(index + 8)), '', body_style2)
            sheet.merge_range('C%s:F%s' % (str(index + 8), str(index + 8)), '', body_style2)
            sheet.merge_range('G%s:H%s' % (str(index + 8), str(index + 8)), '', body_style2)
            sheet.merge_range('I%s:L%s' % (str(index + 8), str(index + 8)), '', body_style2)
            sheet.write('M%s' % str(index + 8), '', body_style2)

            sheet.merge_range('A%s:B%s' % (str(index + 9), str(index + 9)), '可靠性验证', body_style2)
            sheet.merge_range('C%s:F%s' % (str(index + 9), str(index + 9)), 'S-2抽检标准/参考标准', body_style2)
            sheet.merge_range('G%s:H%s' % (str(index + 9), str(index + 9)), '使用设备', body_style2)
            sheet.merge_range('I%s:L%s' % (str(index + 9), str(index + 9)), '测试结果说明', body_style2)
            sheet.write('M%s' % str(index + 9), '判定', body_style2)

            sheet.merge_range('A%s:B%s' % (str(index + 10), str(index + 13)),
                              '□百格试验\n□酒精拭擦\n□高低温验证\n□3M胶附着力拉力测试\n□盐雾验证\n□气密性测试\n□插拔试验(其它: )', body_style3)
            sheet.merge_range('C%s:F%s' % (str(index + 10), str(index + 13)), '《SN-WI-QA-109部品可靠性标准》', body_style2)
            sheet.merge_range('G%s:H%s' % (str(index + 10), str(index + 13)), '', body_style2)
            sheet.merge_range('I%s:L%s' % (str(index + 10), str(index + 13)), '', body_style2)
            sheet.merge_range('M%s:M%s' % (str(index + 10), str(index + 13)), '', body_style2)

            sheet.merge_range('A%s:B%s' % (str(index + 14), str(index + 14)), '', body_style2)
            sheet.merge_range('C%s:F%s' % (str(index + 14), str(index + 14)), '', body_style2)
            sheet.merge_range('G%s:H%s' % (str(index + 14), str(index + 14)), '', body_style2)
            sheet.merge_range('I%s:L%s' % (str(index + 14), str(index + 14)), '', body_style2)
            sheet.write('M%s' % str(index + 14), '', body_style2)

            sheet.merge_range('A%s:B%s' % (str(index + 15), str(index + 15)), '综合判定', body_style2)
            sheet.merge_range('C%s:G%s' % (str(index + 15), str(index + 15)), '□合格\t□不合格', body_style2)
            sheet.write('H%s' % str(index + 15), '检验员', body_style2)
            sheet.merge_range('I%s:J%s' % (str(index + 15), str(index + 15)), '', body_style2)
            sheet.write('K%s' % str(index + 15), 'SQE', body_style2)
            sheet.merge_range('L%s:M%s' % (str(index + 15), str(index + 15)), '', body_style2)
            sheet.merge_range('A%s:M%s' % (str(index + 16), str(index + 16)),
                              '编号：SN-FM-QA-024%s版本:B' % (" " * 90), body_style3)
        elif self.production_id:
            sheet.merge_range('A3:M3', 'QA检验报告', title_style2)
            sheet.write('A4:', '订单号', body_style4)
            sheet.write('B4:', self.production_id.name, body_style4)
            sheet.write('C4:', '客户代码', body_style4)
            sheet.write('D4:', self.env.company.name, body_style4)
            sheet.merge_range('E4:F4', '订单数量', body_style4)
            sheet.merge_range('G4:H4', self.check_qty, body_style4)
            sheet.merge_range('I4:K4', '送检时间', body_style4)
            sheet.merge_range('L4:M4', self.create_date.strftime('%Y-%m-%d %H:%M'), body_style4)
            sheet.write('A5:', '产品编码', body_style4)
            sheet.write('B5:', self.product_id.default_code, body_style4)
            sheet.write('C5:', '送检数量', body_style4)
            sheet.write('D5:', self.check_qty, body_style4)
            sheet.merge_range('E5:F5', '组别', body_style4)
            sheet.merge_range('G5:M5', '□A线  □B线  □C线 □D线  □返工/包装', body_style4)
            sheet.merge_range('A6:A7', '规格', body_style4)
            sheet.merge_range('B6:B7', self.product_id.description, body_style4)
            sheet.merge_range('C6:C7', '抽检数量', body_style4)
            sample_qty = self.inspection_level.sample_id.sample_qty
            sheet.merge_range('D6:D7', sample_qty, body_style4)
            sheet.merge_range('E6:H7', 'AQL(GB2828.1-2003)', body_style4)
            sheet.write('I6', 'Ac', body_style4)
            sheet.write('J6', 'Re', body_style4)
            sheet.merge_range('K6:M7', '(CR:0 MA:0.65 MI:1.0)', body_style4)
            sheet.write('I7', self.inspection_level.accept_qty, body_style4)
            sheet.write('J7', self.inspection_level.refuse_qty, body_style4)
            sheet.write('A8', '检验项目', body_style4)
            sheet.merge_range('B8:E8', '项目内容', body_style4)
            sheet.merge_range('F8:G8', '抽检方案', body_style4)
            sheet.merge_range('H8:I8', '抽检数量', body_style4)
            sheet.merge_range('J8:K8', '结果', body_style4)
            sheet.merge_range('L8:M8', '备注', body_style4)
            sheet.merge_range('A9:A10', '型号核对', body_style4)
            sheet.merge_range('B9:E9', '1、依生产计划/流程卡上信息核对机型实物是否一致', body_style4)
            sheet.merge_range('B10:E10', '2、试产新机型核对/有没有试产合格报告', body_style4)
            sheet.merge_range('F9:G10', '1pcs', body_style4)
            sheet.merge_range('H9:I10', '1', body_style4)
            sheet.merge_range('J9:K10', '', body_style4)
            sheet.merge_range('L9:M9', '', body_style4)
            sheet.merge_range('L10:M10', '', body_style4)
            sheet.write('A11:', 'BOM核对', body_style4)
            sheet.merge_range('B11:E11', '器件是否符合BOM表/OEM一览表或CCC认证产品（安全关键件清单、EMC关键件清单）的要求', body_style4)
            sheet.merge_range('F11:G11', '1pcs', body_style4)
            sheet.merge_range('H11:I11', '1', body_style4)
            sheet.merge_range('J11:K11', '', body_style4)
            sheet.merge_range('L11:M11', '', body_style4)
            sheet.merge_range('A12:A17', '包装及配件', body_style4)
            sheet.merge_range('B12:E12', '1.包装盒/集装箱是否有图案字迹模糊、丝印错误、破损、脏污', body_style4)
            sheet.merge_range('B13:E13', '2.包装盒及机身标贴是否有贴错、漏贴、客户型号及描述错误', body_style4)
            sheet.merge_range('B14:E14', '4.QCPass/易碎纸是否漏贴，破损，贴错', body_style4)
            sheet.merge_range('B15:E15', '5.包装盒内是否漏放/多放/放错配件', body_style4)
            sheet.merge_range('B16:E16', '6.说明书/保修卡/合格证内容检验及外观是否有破损、脏污', body_style4)
            sheet.merge_range('B17:E17', '7.光盘实物内容及封面是否与BOM一致', body_style4)
            sheet.merge_range('F12:G17', '一般抽样水平II', body_style4)
            sheet.merge_range('H12:I17', '', body_style4)
            sheet.merge_range('J12:K17', '', body_style4)
            sheet.merge_range('L12:M12', '', body_style4)
            sheet.merge_range('L13:M13', '', body_style4)
            sheet.merge_range('L14:M14', '', body_style4)
            sheet.merge_range('L15:M15', '', body_style4)
            sheet.merge_range('L16:M16', '', body_style4)
            sheet.merge_range('L17:M17', '', body_style4)
            sheet.merge_range('A18:A22', '外观(A、B、C三级面)', body_style4)
            sheet.merge_range('B18:E18', '1.间隙，掉漆、刮伤是否超过检验标准', body_style4)
            sheet.merge_range('B19:E19', '2.外观是否有脏污或丝印错误', body_style4)
            sheet.merge_range('B20:E20', '3.撕开镜头保护膜，观察镜头是否有残胶和脏污', body_style4)
            sheet.merge_range('B21:E21', '4.产品是否漏打螺丝/打花，机器内是否有异物', body_style4)
            sheet.merge_range('B22:E22', '5. 结构是否有色差超标', body_style4)
            sheet.merge_range('F18:G22', '一般抽样水平II', body_style4)
            sheet.merge_range('H18:I22', '', body_style4)
            sheet.merge_range('J18:K22', '', body_style4)
            sheet.merge_range('L18:M18', '', body_style4)
            sheet.merge_range('L19:M19', '', body_style4)
            sheet.merge_range('L20:M20', '', body_style4)
            sheet.merge_range('L21:M21', '', body_style4)
            sheet.merge_range('L22:M22', '', body_style4)
            sheet.merge_range('A23:A36', '功能检测', body_style4)
            sheet.merge_range('B23:E23', '1.电源/POE是否供电，电流大、小；检测产品网络连接是否中断', body_style4)
            sheet.merge_range('B24:E24', '2.通电敲打线材接头处，确认是否有接触不良', body_style4)
            sheet.merge_range('B25:E25', '3.指示灯通电是否亮；电源键是否有作用', body_style4)
            sheet.merge_range('B26:E26', '4.菜单、制式检查，检查MAC地址是否与IE端显示MAC地址一致', body_style4)
            sheet.merge_range('B27:E27', '5.硬盘、HDMI、VGA、USB接口检测是否良好', body_style4)
            sheet.merge_range('B28:E28', '6. OEM定制软件内容检验，并核对软件通知单定制内容。', body_style4)
            sheet.merge_range('B29:E29', '7.录像及录像回放是否正常', body_style4)
            sheet.merge_range('B30:E30', '8.智能分析功能检测和P2P手机功能检测', body_style4)
            sheet.merge_range('B31:E31', '9.图像是否有黑影、边角不清晰、整体不清晰、彩转黑、黑转彩', body_style4)
            sheet.merge_range('B32:E32', '10.正常图像和夜视是否有亮点/黑角/暗角/黑点/干扰等显示不良', body_style4)
            sheet.merge_range('B33:E33', '11.红外灯是否正常亮、虑光片是否切换正常', body_style4)
            sheet.merge_range('B34:E34', '12.实测SD卡、音频、报警功能测试是否正常', body_style4)
            sheet.merge_range('B35:E35', '13.产品是否恢复出厂值', body_style4)
            sheet.merge_range('B36:E36', '14.室外图像功能测试是否正常', body_style4)
            sheet.merge_range('F23:G35', '一般抽样水平II', body_style4)
            sheet.merge_range('H23:I35', '', body_style4)
            sheet.merge_range('J23:K35', '', body_style4)
            sheet.merge_range('L23:M23', '', body_style4)
            sheet.merge_range('L24:M24', '', body_style4)
            sheet.merge_range('L25:M25', '', body_style4)
            sheet.merge_range('L26:M26', '', body_style4)
            sheet.merge_range('L27:M27', '', body_style4)
            sheet.merge_range('L28:M28', '', body_style4)
            sheet.merge_range('L29:M29', '', body_style4)
            sheet.merge_range('L30:M30', '', body_style4)
            sheet.merge_range('L31:M31', '', body_style4)
            sheet.merge_range('L32:M32', '', body_style4)
            sheet.merge_range('L33:M33', '', body_style4)
            sheet.merge_range('L34:M34', '', body_style4)
            sheet.merge_range('L35:M35', '', body_style4)
            sheet.merge_range('A37:A38', '可靠性', body_style4)
            sheet.merge_range('B37:E37', '1.冲水测试 □IPX6  □IPX5', body_style4)
            sheet.merge_range('B38:E38', '2.□震动测试  □跌落测试 □起雾测试', body_style4)
            sheet.merge_range('F36:G39', 'S-2', body_style4)
            sheet.merge_range('H36:I39', '', body_style4)
            sheet.merge_range('J36:K39', '', body_style4)
            sheet.merge_range('L36:M36', '', body_style4)
            sheet.merge_range('L37:M37', '', body_style4)
            sheet.merge_range('L38:M38', '', body_style4)
            sheet.merge_range('L39:M39', '', body_style4)
            sheet.write('A39:', '特殊需求', body_style4)
            sheet.merge_range('B39:E39', '如客户订单有要求时检查', body_style4)
            sheet.merge_range('A40:M40', '检验结果：    □ 合格       □ 不合格  %s      软件版本：必填' % (" " * 90), body_style5)
            sheet.merge_range('A41:M41', '备注：1.如没有不良结果就“√”有就“×”；2.检验不合格时报告才需要生产签字确认，反之只需要品质确认。', body_style5)
            # sheet.merge_range('A42:M42', '')
            sheet.merge_range('B42:D42', '检验员/检验时间:')
            sheet.merge_range('E42:F42', '')
            sheet.merge_range('G42:H42', '生产确认:')
            sheet.merge_range('I42:J42', '')
            sheet.write('K42', '审核:')
            sheet.merge_range('L42:M42', '')
            sheet.merge_range('A43:M43', '编号：SN-FM-QA-027%s版本:F' % (" " * 70))

        workbook.close()
        file_data.seek(0)
        self.query_result = base64.b64encode(file_data.read())

    def down_excel(self):
        self.ensure_one()
        self.prepare_excel_values()
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/content/%s/%s/%s/%s' % (
                self._name, self.id, 'query_result',
                '%s来料检验结果报告%s.xlsx' % (self.name, fields.Datetime.now().strftime('%Y-%m-%d %H-%M-%S'))),
            'target': 'new',
        }


class QualityCheckLine(models.Model):
    _name = 'quality.check.line'
    _description = 'Quality control details for quality control of products'

    check_id = fields.Many2one('quality.check', string='质检')
    item_id = fields.Many2one('check.items', string='检验项目')
    noncritical_id = fields.Many2one('noncritical.item', string='项目判定', default=1, compute='_compute_noncritical_id',
                                     store=True)
    noncritical_result = fields.Char(related='noncritical_id.result', string='项目结果')
    disadvantage = fields.Integer(string='缺点等级')
    state = fields.Selection(related='check_id.state', string='状态')
    product_id = fields.Many2one(related='check_id.product_id', string='产品')
    sample_standard_id = fields.Many2one('sample.standards', string='检验标准')
    AQL = fields.Many2one('receive.quality.limit', string='AQL')
    receive_qty = fields.Float(string='接受数', compute='_compute_receive_and_refuse_qty')
    refuse_qty = fields.Float(string='拒绝数', compute='_compute_receive_and_refuse_qty')
    sample_qty = fields.Float(string='样本量', compute='_compute_receive_and_refuse_qty')
    bad_qty = fields.Float('不良数量')
    bad_note = fields.Char('不良详细内容')

    @api.depends('check_id.IQC_code', 'AQL')
    def _compute_receive_and_refuse_qty(self):
        for rec in self:
            rec.AQL = rec.check_id.AQL.id
            qcs_id = self.env['quality.control.standards'].search(
                [('AQL_id', '=', rec.AQL.id), ('sample_id', '=', rec.check_id.IQC_code.id)], limit=1)
            rec.receive_qty = qcs_id.accept_qty
            rec.refuse_qty = qcs_id.refuse_qty
            rec.sample_qty = qcs_id.sample_id.sample_qty
            # 考虑单独做成计算字段,line_ids所有的AQL需要相同, 取set
            rec.check_id.inspection_level = qcs_id.id

    @api.constrains('receive_qty', 'refuse_qty', 'bad_qty')
    def check_noncritical_result(self):
        """限制数量"""
        for line in self:
            if line.bad_qty > line.sample_qty:
                raise UserError('不良数量不能超过样本数量!')

    @api.depends('bad_qty', 'sample_qty', 'refuse_qty', 'receive_qty')
    def _compute_noncritical_id(self):
        for rec in self:
            # 合格
            qualified_id = self.env['noncritical.item'].search([('result', '=', '合格')], limit=1)
            # 不合格
            unqualified_id = self.env['noncritical.item'].search([('result', '=', '验退')], limit=1)
            if rec.bad_qty < rec.refuse_qty:
                rec.noncritical_id = qualified_id.id
            else:
                rec.noncritical_id = unqualified_id.id
