from odoo import models, fields, api


class QualityPoint(models.Model):
    _inherit = "quality.point"

    item_ids = fields.Many2many('check.items', string='检验项目')
