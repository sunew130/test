from odoo import api, models, fields


class CheckItems(models.Model):
    _name = 'check.items'
    _description = '检验项目'

    name = fields.Char(string='检验项目')
    line_ids = fields.One2many('check.items.lines', 'item_id', string='检验内容')


class CheckItemsLines(models.Model):
    _name = 'check.items.lines'
    _description = '检验内容'

    name = fields.Char(string='检验内容')
    item_id = fields.Many2one('check.items', string='检验项目')


class NoncriticalItem(models.Model):
    _name = 'noncritical.item'
    _description = '质检项目判定'
    _rec_name = 'result'
    _sql_constraints = [
        ('name_unique', 'unique (name)', "项目判定序号必须唯一!"),
    ]

    name = fields.Integer('项目判定')
    result = fields.Char('判定结果')
