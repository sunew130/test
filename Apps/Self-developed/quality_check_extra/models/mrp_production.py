from odoo import api, models, fields


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    rework_ids = fields.One2many('mrp.rework', 'production_id', string='返工记录')


