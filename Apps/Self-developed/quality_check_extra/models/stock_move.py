from collections import defaultdict

from odoo import models, api


class StockMove(models.Model):
    _inherit = 'stock.move'


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def create(self, vals_list):
        """ 禁止退货后创建的收货单创建质检入库单 """
        picking_type_id = self.env['stock.picking.type'].search([('name', '=', '质检入库')])
        if self.env.context.get('ignore_check', False) and vals_list.get('picking_type_id', False) == picking_type_id.id:
            return self
        return super(StockPicking, self).create(vals_list)


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    @api.model
    def create(self, vals_list):
        res = super(StockMoveLine, self).create(vals_list)
        return res
