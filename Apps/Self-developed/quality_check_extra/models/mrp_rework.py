from odoo import api, models, fields


class MrpRework(models.Model):
    _name = 'mrp.rework'
    _description = '制造单质检不通过的记录'

    product_id = fields.Many2one('product.product', string='产品')
    production_id = fields.Many2one('mrp.production', string='制造单')
    rework_qty = fields.Float('返工个数')
    check_id = fields.Many2one('quality.check', string='质检记录')
