from odoo import models, fields, api


class SpecialPurchaseProductSetting(models.Model):
    _name = "special.purchase.product.setting"
    _description = '特采产品设置'

    product_id = fields.Many2one('product.product', string='产品')
    supplier_ids = fields.Many2many('product.supplierinfo', string='供应商')


