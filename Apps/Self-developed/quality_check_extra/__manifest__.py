# -*- coding: utf-8 -*-
{
    'name': "quality_check_extra",

    'summary': """
        """,

    'description': """
        
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'quality_control', 'quality', 'quality_mrp', 'mrp'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'data/items_data.xml',
        'views/check_result_print_template.xml',
        'views/check_result_print_template.xml',
        'views/quality_check_extra_inherit_view.xml',
        'views/check_items_settings_view.xml',
        'views/special_purchase_product_setting_view.xml',
        'views/quality_point_view_form_inherit_invisible.xml',
        'views/quality_control_standards_view.xml',
        'views/mrp_production_view_form_inherit_quality_invisible_button.xml',
        'views/mrp_production_form_view_inherit_add_page.xml',
        'wizard/create_return_order_confirm_wizard_view.xml',
        'report/check_result_print_report.xml',


        'views/menus_view.xml',
    ],
    # 'qweb': ['static/src/xml/*.xml',

             # ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
