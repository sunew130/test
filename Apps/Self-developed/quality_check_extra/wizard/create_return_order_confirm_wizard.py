from odoo import models, fields


class CreateReturnOrderConfirmWizard(models.TransientModel):
    _name = 'create.return.order.confirm.wizard'

    def confirm_create_return_order(self):
        active_id = self._context.get('active_id', False)
        check_id = self.env['quality.check'].browse(active_id)
        str_move_id = self._context.get('move_id', False)
        move_id = self.env['stock.move.line'].browse(str_move_id)
        if check_id and move_id:
            check_id._create_return_order(move_id=move_id)
        return {'type': 'ir.actions.act_window_close'}




