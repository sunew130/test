# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import datetime
from odoo import _

APPROVAL_STATE_LIST = [('wait_approval', '申请PC主管审批'), ('pc_manager_approval', 'PC主管审批'),
                       ('approval', '审批通过')]


class mrp_production_extra(models.Model):
    _inherit = 'mrp.production'
    x_outsourcing = fields.Boolean(string='Outsourcing')
    is_rework = fields.Boolean(string='是否返工', readonly=False)
    mo_id = fields.Many2one('mrp.production', '源制造订单',
                            domain="[('id', 'in', allowed_mo_ids)]",
                            states={'done': [('readonly', True)]}, check_company=True, readonly=False)
    allowed_mo_ids = fields.One2many('mrp.production', compute='_compute_allowed_mo_ids')

    date_planned_start = fields.Datetime(required=False)

    def write(self, vals):
        """ 草稿状态的单据(如果没有权限不允许编辑)"""
        if self.state != 'draft' and not self.user_has_groups('mrp_extra.group_edit_non-draft_mrp_production'):
            raise ValidationError('没有编辑非草稿状态制造单的权限!')
        return super(mrp_production_extra, self).write(vals)

    @api.depends('company_id', 'product_id')
    def _compute_allowed_mo_ids(self):
        # 筛选已完成的制造订单
        for record in self:
            if record.product_id:
                domain = [
                    ('state', '=', 'done'),
                    ('product_id', '=', record.product_id.id),
                    ('company_id', '=', record.company_id.id)
                ]
            else:
                domain = [
                    ('state', 'in', ['done', 'cancel']),
                    ('company_id', '=', record.company_id.id)
                ]
            allowed_mos = self.env['mrp.production'].search_read(domain, ['id'])
            if allowed_mos:
                record.allowed_mo_ids = [mo['id'] for mo in allowed_mos]
            else:
                record.allowed_mo_ids = False

    @api.onchange('bom_id', 'product_id', 'product_qty', 'product_uom_id', 'is_rework')
    def _onchange_move_raw(self):
        # 新增字段is_rework：是否返工单;条件过滤处理
        if self.product_id.id is False:
            self.move_raw_ids = [(5,)]
        if not self.bom_id and not self._origin.product_id:
            return
        if self.product_id != self._origin.product_id:
            self.move_raw_ids = [(5,)]
        if self.bom_id and self.product_qty > 0 and self.is_rework is False:
            # keep manual entries
            list_move_raw = [(4, move.id) for move in self.move_raw_ids.filtered(lambda m: not m.bom_line_id)]
            moves_raw_values = self._get_moves_raw_values()
            move_raw_dict = {move.bom_line_id.id: move for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)}
            for move_raw_values in moves_raw_values:
                if move_raw_values['bom_line_id'] in move_raw_dict:
                    # 更新现有list
                    list_move_raw += [(1, move_raw_dict[move_raw_values['bom_line_id']].id, move_raw_values)]
                else:
                    # 添加到list
                    list_move_raw += [(0, 0, move_raw_values)]
            self.move_raw_ids = list_move_raw
        elif self.is_rework is True:
            self.move_raw_ids = [(0, 0, {
                # 'sequence': bom_line.sequence if bom_line else 10,
                'name': self.name,
                'date': self.date_planned_start,
                'date_deadline': self.date_planned_start,
                # 'bom_line_id': bom_line.id if bom_line else False,
                'picking_type_id': self.picking_type_id.id,
                'product_id': self.product_id.id,
                'product_uom_qty': self.product_id.qty_available,
                'product_uom': self.product_id.uom_id.id,
                'location_id': self.product_id.location_id.id,
                'location_dest_id': self.product_id.with_company(self.company_id).property_stock_production.id,
                'raw_material_production_id': self.id,
                'company_id': self.company_id.id,
                # 'operation_id': self.product_id.operation_id,
                'price_unit': self.product_id.standard_price,
                'procure_method': 'make_to_stock',
                'origin': self.name,
                'state': 'draft',
                'warehouse_id': self.location_src_id.get_warehouse().id,
                'group_id': self.procurement_group_id.id,
                'propagate_cancel': self.propagate_cancel,
            })]
        else:
            # 从id集合中删除指定的id
            self.move_raw_ids = [(2, move.id) for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)]

    @api.onchange('product_id', 'x_outsourcing', 'is_rework')
    def onchange_product(self):
        if self._context.get('outsourcing_order') == 1:
            if self.x_outsourcing is False or self.state == 'draft':
                self.x_outsourcing = True
        elif self._context.get('rework_order') == 1:
            if self.is_rework is False or self.state == 'draft':
                self.is_rework = True

    def _get_move_finished_values(self, product_id, product_uom_qty, product_uom, operation_id=False,
                                  byproduct_id=False):

        have_date = self.date_planned_start
        # 如果没有填写安排的日期，就填写当前日期,防止没有填安排的日期而报错
        if have_date is False:
            self.date_planned_start = datetime.datetime.now()

        vals = super(mrp_production_extra, self)._get_move_finished_values(product_id, product_uom_qty, product_uom,
                                                                           operation_id, byproduct_id)

        # 如果没有填写安排的日期，还原之前的安排日期的状态
        if have_date is False:
            self.date_planned_start = False
            self.date_planned_finished = False

        return vals

    def action_confirm(self):
        # 确认时，没有设置安排的日期提示报错
        for production in self:
            if production.date_planned_start is False:
                raise ValidationError(
                    _("确定失败 :  没有设置安排的日期"))
        super(mrp_production_extra, self).action_confirm()

    approval_state = fields.Selection(selection=APPROVAL_STATE_LIST, string='审批状态', default='wait_approval',
                                      tracking=True)

    def action_cancel(self):
        self.approval_state = 'wait_approval'
        super(mrp_production_extra, self).action_cancel()

    def button_apply_pc_manager_approval(self):
        """
        申请PC主管审批
        """
        self.approval_state = 'pc_manager_approval'

    def button_pc_manager_approval(self):
        """
        PC主管审批
        """
        self.approval_state = 'approval'

    def button_action_open_orderpoints(self):
        action = self.orderpoint_id.with_context(
            search_default_trigger='manual',
            search_default_filter_to_reorder=True,
            search_default_filter_not_snoozed=True,
            default_trigger='manual',
            search_default_order_number=self.name,
            default_order_number=self.name
        ).action_open_orderpoints()
        return action