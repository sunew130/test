from collections import defaultdict

from odoo import models, api
import datetime

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.model
    def create(self, vals_list):

        # 没有设置日期时，填写当前日期
        if vals_list.get('date') is False:
            vals_list['date'] = datetime.datetime.now()

        return super(StockMove, self).create(vals_list)

    @api.model
    def write(self, vals):

        # 没有设置日期时，填写当前日期
        if vals.get('date') is False:
            vals['date'] = datetime.datetime.now()
        return super(StockMove, self).write(vals)