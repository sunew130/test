# -*- coding: utf-8 -*-

import json

from odoo import api, models, _
from odoo.tools import float_round

class ReportBomStructure(models.AbstractModel):
    _inherit = 'report.mrp.report_bom_structure'

    def _get_bom_lines(self, bom, bom_quantity, product, line_id, level):
        components, total = super(ReportBomStructure, self)._get_bom_lines(bom, bom_quantity, product, line_id, level)

        has_group = self.user_has_groups("sn_account.readonly_standard_price")
        for component in components:
            component['report_structure'] = 'all'
            if not has_group:
                component['report_structure'] = 'bom_structure'

        return components, total

    @api.model
    def get_html(self, bom_id=False, searchQty=1, searchVariant=False):
        res = self._get_report_data(bom_id=bom_id, searchQty=searchQty, searchVariant=searchVariant)
        res['lines']['report_type'] = 'html'
        res['lines']['report_structure'] = 'all'

        has_group = self.user_has_groups("sn_account.readonly_standard_price")
        res['groups_standard_price'] = has_group
        if not has_group:
            res['lines']['report_structure'] = 'bom_structure'

        res['lines']['has_attachments'] = res['lines']['attachments'] or any(component['attachments'] for component in res['lines']['components'])
        res['lines'] = self.env.ref('mrp.report_mrp_bom')._render({'data': res['lines']})
        return res


    def _get_pdf_line(self, bom_id, product_id=False, qty=1, child_bom_ids=[], unfolded=False):
        res = super(ReportBomStructure, self)._get_pdf_line(bom_id, product_id, qty, child_bom_ids, unfolded)
        # has_group = self.user_has_groups("sn_account.readonly_standard_price")
        # res['report_structure'] = 'all'
        # if not has_group:
        #     res['report_structure'] = 'bom_structure'

        return res

    def _get_report_values(self, docids, data=None):
        res = super(ReportBomStructure, self)._get_report_values(docids, data)

        has_group = self.user_has_groups("sn_account.readonly_standard_price")
        for item in res['docs']:
            item['report_structure'] = 'all'
            if not has_group:
                item['report_structure'] = 'bom_structure'


        return res