# -*- coding: utf-8 -*-
{
    'name': "mrp_extra",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mrp', 'stock', 'stock_extra'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/mrp_menu_security.xml',
        'security/security.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/mrp_production_views.xml',
        'views/stock_move_views.xml',
        'report/mrp_report_bom_structure.xml',
        'views/mrp_menus_views.xml',
        'views/mrp_production_approval_views.xml'
    ],
    'qweb': ['static/src/xml/*.xml',

             ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
