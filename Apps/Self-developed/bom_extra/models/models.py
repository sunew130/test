# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Bom_extra(models.Model):
    _inherit = 'mrp.bom'

    note = fields.Text(string="备注")


class BomLine_extra(models.Model):
    _inherit = 'mrp.bom.line'

    x_make_in_hand_count = fields.Integer(string="在手", compute="_compute_make_in_hand_count")
    x_make_wait_inspection_count = fields.Integer(string="待检", compute="_compute_make_wait_inspection_count")
    x_make_in_purchase_count = fields.Integer(string="在购", compute="_compute_make_in_purchase_count")
    x_make_count = fields.Integer(string="在制", compute="_compute_make_count")
    x_make_forecast_count = fields.Integer(string="预测", compute="_compute_make_forecast_count")

    material_tag = fields.Char(string="位号")

    def _compute_make_in_hand_count(self):
        for record in self:
            record.x_make_in_hand_count = record.product_id.qty_available

    def _compute_make_wait_inspection_count(self):
        for record in self:
            record.x_make_wait_inspection_count = record.product_id.to_check_product_qty

    def _compute_make_in_purchase_count(self):
        for record in self:
            record.x_make_in_purchase_count = record.product_id.to_purchase_product_qty

    def _compute_make_count(self):
        for record in self:
            record.x_make_count = record.product_id.to_product_product_qty

    def _compute_make_forecast_count(self):
        for record in self:
            record.x_make_forecast_count = record.product_id.virtual_available
