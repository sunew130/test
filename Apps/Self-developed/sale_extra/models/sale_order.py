# -*- coding: utf-8 -*-

from odoo import models, fields, api

APPROVAL_STATE_LIST = [('wait_approval', '申请销售主管审批'), ('sale_executive_approval', '销售主管审批'),
                       ('finance_approval', '财务审批'), ('approval', '审批通过')]


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    approval_state = fields.Selection(selection=APPROVAL_STATE_LIST, string='审批状态', default='wait_approval',
                                      tracking=True)

    def action_draft(self):
        """
        设为报价单的同时将审批状态修改为申请销售主管审批
        @return: action_draft
        """
        res = super(SaleOrder, self).action_draft()
        self.approval_state = 'wait_approval'
        return res

    def button_apply_sale_executive_approval(self):
        """
        申请销售主管审批
        """
        self.approval_state = 'sale_executive_approval'

    def button_sale_executive_approval(self):
        """
        销售主管审核
        """
        self.approval_state = 'finance_approval'

    def button_finance_approval(self):
        """
        财务审核
        """
        self.approval_state = 'approval'

    def button_refuse_approval(self):
        """
        拒绝申请, 状态回到申请销售主管审核状态
        """
        self.approval_state = 'wait_approval'
