odoo.define('hide_toolbar_dropdown', function (require) {
    "use strict";
    var ListController = require('web.ListController');
    var FormController = require('web.FormController');
    var session = require('web.session');

    ListController.include({
        init: function (parent, model, renderer, params) {
            this.isActionEnable = false;
            this.isPrintEnable = false;
            this._super.apply(this, arguments);
        },
        _getActionMenuItems: function (state) {
            var pros = this._super.apply(this, arguments);
            if ((this.modelName === 'product.product' || this.modelName === 'product.template') && pros) {
                if (!this.isActionEnable) {
                    pros.items.action = []
                    pros.items.other = []
                }
                if (!this.isPrintEnable) {
                    pros.items.print = []
                }
            }
            return pros;
        },

        willStart() {
            var self = this;
            session.user_has_group('product_extra.group_product_action_dropdown').then(function (has_group) {
                if (has_group) {
                    self.isActionEnable = true;
                }
            });
            session.user_has_group('product_extra.group_product_print_dropdown').then(function (has_group) {
                if (has_group) {
                    self.isPrintEnable = true;
                }
            });
            return this._super.apply(this, arguments);
        }
    });

    FormController.include({
        _getActionMenuItems: function (state) {
            var pros = this._super.apply(this, arguments);
            // session.session_reload()
            if ((this.modelName === 'product.product' || this.modelName === 'product.template') && pros) {
                session.user_has_group('product_extra.group_product_action_dropdown').then(function (has_group) {
                    if (!has_group) {
                        pros.items.action = []
                        pros.items.other = []
                    }
                })
                session.user_has_group('product_extra.group_product_print_dropdown').then(function (has_group) {
                    if (!has_group) {
                        pros.items.print = []
                    }
                })
            }
            return pros;
        }
    })


})



