# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import operator as py_operator
from ast import literal_eval
from collections import defaultdict

from dateutil.relativedelta import relativedelta
from odoo import _, api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools import pycompat, float_is_zero
from odoo.tools.float_utils import float_round
from odoo.exceptions import ValidationError
from datetime import datetime

class Product(models.Model):
    _inherit = "product.product"

    to_purchase_product_qty = fields.Float(compute='_compute_purchased_product_extra_qty', string='To Purchase')

    to_product_product_qty = fields.Float(compute='_compute_product_product_extra_qty', string='To Product')

    to_check_product_qty = fields.Float(compute='_compute_check_product_extra_qty', string='To Check')

    def _compute_check_product_extra_qty(self):
        result = {data['product_id'][0]: data['product_qty'] for data in
                  self.env['stock.move.line'].read_group(
                      ['&', ('product_id', 'in', self.ids), ('picking_id.picking_type_id.sequence_code', '=', 'QC')],
                      ['product_id', 'product_qty'],
                      ['product_id'])}

        for product in self:
            product.to_check_product_qty = result.get(product.id, 0.0)

    def _compute_product_product_extra_qty(self):
        date_from = fields.Datetime.to_string(fields.Date.context_today(self) - relativedelta(years=1))
        domain = [
            ('state', 'in', ['waiting', 'confirmed']),
            ('product_id', 'in', self.ids)
        ]

        mrp_production = self.env['mrp.production'].read_group(domain, ['product_id', 'product_qty'],
                                                               ['product_id'])
        production_data = dict([(data['product_id'][0], data['product_qty']) for data in mrp_production])

        for product in self:
            if not product.id:
                product.to_product_product_qty = 0.0
                continue
            product.to_product_product_qty = float_round(production_data.get(product.id, 0),
                                                         precision_rounding=product.uom_id.rounding)

    def _compute_purchased_product_extra_qty(self):
        date_from = fields.Datetime.to_string(fields.Date.context_today(self) - relativedelta(years=1))

        # 物料在购数量的统计应该是发送询价单状态或者采购单状态
        domain1 = [
            ('order_id.state', 'not in', ['draft', 'cancel']),
            ('product_id', 'in', self.ids),
            ('order_id.x_can_recover_case', '=', False)
            # ,('order_id.date_approve', '>=', date_from)
        ]

        to_purchase_order_lines = self.env['purchase.order.line'].read_group(domain1, ['product_id', 'product_uom_qty',
                                                                                       'qty_received'],
                                                                             ['product_id'])
        to_purchase_data = dict([(data['product_id'][0], data['product_uom_qty'] - data['qty_received']) for data in
                                 to_purchase_order_lines])

        for product in self:
            if not product.id:
                product.purchased_product_qty = 0.0
                continue
            product.to_purchase_product_qty = float_round(to_purchase_data.get(product.id, 0),
                                                          precision_rounding=product.uom_id.rounding)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # TDE FIXME: strange
        if self._context.get('search_categ_id'):
            search_data = self._context.get('search_categ_id')
            if self._context.get('search_purchase_outsourcing') is True:
                if 1 in search_data:
                    search_data.remove(1)
            elif self._context.get('search_purchase_outsourcing') is False:
                if 6 in search_data:
                    search_data.remove(6)

            if self._context.get('search_mrp_rework') is False:
                if 6 in search_data:
                    search_data.remove(6)

            args.append(('categ_id', 'child_of', search_data))
            # args.append((('categ_id', 'child_of', self._context['search_categ_id'])))
        return super(Product, self)._search(args, offset=offset, limit=limit, order=order, count=count,
                                                   access_rights_uid=access_rights_uid)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    to_purchase_product_qty = fields.Float(compute='_compute_purchased_product_extra_qty', string='To Purchase')

    to_product_product_qty = fields.Float(compute='_compute_product_product_extra_qty', string='To Product')

    to_check_product_qty = fields.Float(compute='_compute_check_product_extra_qty', string='To Check')

    x_min_packing_quantity = fields.Integer(string='Minimum packing quantity')

    x_brand = fields.Char(string='Brand')

    x_manufacturer_pdt_number = fields.Char(string='Manufacturer product number')

    effect_date = fields.Datetime('生效时间')

    expire_date = fields.Datetime('失效时间')

    def _compute_check_product_extra_qty(self):
        for template in self:
            template.to_check_product_qty = sum([p.to_check_product_qty for p in template.product_variant_ids])

    def _compute_product_product_extra_qty(self):
        for template in self:
            template.to_product_product_qty = float_round(
                sum([p.to_product_product_qty for p in template.product_variant_ids]),
                precision_rounding=template.uom_id.rounding)

    def _compute_purchased_product_extra_qty(self):
        for template in self:
            template.to_purchase_product_qty = float_round(
                sum([p.to_purchase_product_qty for p in template.product_variant_ids]),
                precision_rounding=template.uom_id.rounding)

    def write(self, vals):
        """ 没有权限不能编辑采购员 """
        if 'x_purchase_user_id' in vals.keys() and not self.user_has_groups('product_extra.group_edit_x_purchase_user'):
            raise ValidationError('没有编辑产品采购员的权限!')
        return super(ProductTemplate, self).write(vals)

    @property
    def _get_finished_product_category(self):
        finished = self.env['product.category'].search(
            [('name', '=', '成品'), ('complete_name', '=', '成品')])
        return finished.id

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if self.user_has_groups('product_extra.group_only_finished_products'):
            args += [('categ_id', 'child_of', self._get_finished_product_category)]
        return super(ProductTemplate, self)._search(
            args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if self.user_has_groups('product_extra.group_only_finished_products'):
            domain += [('categ_id', 'child_of', self._get_finished_product_category)]
        return super(ProductTemplate, self).read_group(
            domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
