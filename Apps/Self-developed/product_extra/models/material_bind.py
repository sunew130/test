# -*- coding: utf-8 -*-
from odoo import api, fields, models


class material_bind(models.Model):
    _name = 'purchase_extra.material_bind'
    _description = 'material bind'
    _rec_name = 'x_product_id'
    x_product_id = fields.Many2one('product.product', string='product to be bound')
    line_ids = fields.One2many('purchase_extra.material_bind_line', 'x_bind_product_id')
    sync_qty = fields.Boolean('绑定物料的数量，随主物料数量的变化而变化')


class material_bind_line(models.Model):
    _name = 'purchase_extra.material_bind_line'
    _rec_name = 'x_product_id'
    x_bind_product_id = fields.Many2one('purchase_extra.material_bind', string="bound product id")
    x_product_id = fields.Many2one('product.product', string="绑定物料")
    x_bind_count = fields.Integer(string='数量')
