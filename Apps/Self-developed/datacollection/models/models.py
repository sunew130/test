# -*- coding: utf-8 -*-

from odoo import models, fields, api
import pymysql
import json
import requests
import time
import logging


class sunview_dev_type_info(models.Model):
    _name = 'datacollection.sunview_dev_type_info'
    _description = 'datacollection.sunview_dev_type_info'
    x_dev_type_id = fields.Integer(string="设备类型ID")
    x_dev_type_name = fields.Char(string="设备类型")


class sunview_access_protocol_info(models.Model):
    _name = 'datacollection.sunview_protocol_info'
    _description = 'datacollection.sunview_protocol_info'
    x_dev_protocal_id = fields.Integer(string="设备协议ID", store="true")
    x_dev_protocal_name = fields.Char(string="设备协议", store="true")


class sunview_dev_info(models.Model):
    _name = 'datacollection.sunview_dev_info'
    _description = 'datacollection.sunview_dev_info'
    x_cmu_guid = fields.Char()
    x_summary_info_id = fields.Many2one('datacollection.sunview_info', 'sunview_info')
    x_dev_type = fields.Char(string="设备类型")
    x_dev_protocal = fields.Char(string="设备协议")
    x_dev_sum = fields.Integer(string="设备总数")
    # x_dev_type_name = fields.Char(string='设备类型', compute='_compute_dev_type_name')
    # x_dev_protocal_name = fields.Char(string='设备协议', compute='_compute_dev_protocal_name')
    x_dev_type_name = fields.Char(string='设备类型')
    x_dev_protocal_name = fields.Char(string='设备协议')
    x_cmu_ipaddr = fields.Char(string="IP")
    x_world_area = fields.Char(string="世界区域")
    x_country = fields.Char(string="国家")
    x_province = fields.Char(string="省/州")
    x_city = fields.Char(string="城市")

    def _compute_dev_type_name(self):
        sql_query = "select x_dev_type_id, x_dev_type_name from datacollection_sunview_dev_type_info"
        self.env.cr.execute(sql_query)
        rs = self.env.cr.fetchall()

        for record in self:
            record.x_dev_type_name = ''
            for item in rs:
                if item[0] is None:
                    continue
                if record.x_dev_type == str(item[0]):
                    record.x_dev_type_name = item[1]
                    break

    def _compute_dev_protocal_name(self):
        sql_query = "select x_dev_protocal_id, x_dev_protocal_name from datacollection_sunview_protocol_info"
        self.env.cr.execute(sql_query)
        rs = self.env.cr.fetchall()

        for record in self:
            record.x_dev_protocal_name = ''
            for item in rs:
                if item[0] is None:
                    continue
                if record.x_dev_protocal == str(item[0]):
                    record.x_dev_protocal_name = item[1]
                    break

    def _get_dev_type_name(self, dev_type_list, dev_type_id):
        x_dev_type_name = " "
        for item in dev_type_list:
            if item[0] is None:
                continue
            if str(dev_type_id) == str(item[0]):
                x_dev_type_name = item[1]
                break
        return x_dev_type_name

    def _get_dev_protocol_name(self, dev_protocol_list, dev_protocol_id):
        x_dev_protocol_name = " "
        for item in dev_protocol_list:
            if item[0] is None:
                continue
            if str(dev_protocol_id) == str(item[0]):
                x_dev_protocol_name = item[1]
                break
        return x_dev_protocol_name

    def _get_dev_sum_categ(self, dev_sum):
        x_dev_sum_categ = ""
        if dev_sum < 10:
            x_dev_sum_categ = "C1:1-10个"
        elif dev_sum < 50:
            x_dev_sum_categ = "C2:10-50个"
        elif dev_sum < 100:
            x_dev_sum_categ = "C3:50-100个"
        elif dev_sum < 200:
            x_dev_sum_categ = "C4:100-200个"
        elif dev_sum < 500:
            x_dev_sum_categ = "C5:200-500个"
        else:
            x_dev_sum_categ = "C6:500个以上"
        return x_dev_sum_categ

    def _get_ch_sum_categ(self, ch_sum):
        x_ch_sum_categ = ""
        if ch_sum < 1:
            x_ch_sum_categ = "C0:0 ch"
        elif ch_sum < 5:
            x_ch_sum_categ = "C1:1-4 ch"
        elif ch_sum < 17:
            x_ch_sum_categ = "C2:5-16 ch"
        elif ch_sum < 65:
            x_ch_sum_categ = "C3:17-64 ch"
        elif ch_sum < 257:
            x_ch_sum_categ = "C4:65-256 ch"
        else:
            x_ch_sum_categ = "C5:256 more ch"
        return x_ch_sum_categ

    def _get_sunview_categ_ver(self, sunview_ver):
        if len(sunview_ver) < 8:
            return ''
        list_ver = sunview_ver.split('.')
        if len(list_ver) < 6:
            return ''
        x_sunview_ver = list_ver[0] + '.' + list_ver[1] + '.' + list_ver[4]
        return x_sunview_ver

    def _get_x_brand_categ(self, sunview_ver):
        if len(sunview_ver) < 8:
            return ''
        list_ver = sunview_ver.split('.')
        if len(list_ver) < 6:
            return ''
        x_brand_categ = ""
        if list_ver[2] == "1":
            x_brand_categ = "景阳"
        elif list_ver[2] == "3":
            x_brand_categ = "中性"
        else:
            x_brand_categ = "OEM"
        return x_brand_categ

    def update_dev_info_to_db(self):
        db = pymysql.connect(host='sunell.com.cn', port=3360, user='admin', password='inview_admin123',
                             database='inview', charset='utf8')
        cursor = db.cursor()
        # sql = """SELECT * FROM
        #            (SELECT user_info_id, MAX(report_time),access_device_sum,access_channel_sum,snap_sum,software_version, ip, id
        #            FROM `inview_sunview_summary_info`  WHERE TO_DAYS(report_time) = TO_DAYS(NOW()) AND ip!='' GROUP BY user_info_id
        #            ) AS b LEFT JOIN inview_sunview_software_user_info AS a ON a.id = b.user_info_id"""
        sql = """SELECT * FROM
               (SELECT user_info_id, MAX(report_time),access_device_sum,access_channel_sum,snap_sum,software_version, 
               ip, id FROM `inview_sunview_summary_info` WHERE report_time >= DATE_SUB(NOW() ,INTERVAL 24 HOUR) 
               AND report_time <= NOW() AND ip!='' GROUP BY user_info_id ) 
               AS b LEFT JOIN inview_sunview_software_user_info AS a ON a.id = b.user_info_id"""
        cursor.execute(
            sql)  # report_time >= DATE_SUB(NOW() ,INTERVAL 24 HOUR) AND report_time <= NOW() DATE_SUB(NOW() ,INTERVAL 24 HOUR) AND
        #       report_time <= NOW()
        rs = cursor.fetchall()
        # cursor.close()
        # db.close()
        if len(rs) == 0:
            cursor.close()
            db.close()
            return

        sql_query1 = "select x_dev_protocal_id, x_dev_protocal_name from datacollection_sunview_protocol_info"
        self.env.cr.execute(sql_query1)
        rs_dev_protocol = self.env.cr.fetchall()

        sql_query2 = "select x_dev_type_id, x_dev_type_name from datacollection_sunview_dev_type_info"
        self.env.cr.execute(sql_query2)
        rs_dev_type = self.env.cr.fetchall()

        for rss in rs:
            x_ver_categ = self._get_sunview_categ_ver(rss[5])
            x_dev_sum_categ = self._get_dev_sum_categ(rss[2])
            x_ch_sum_categ = self._get_ch_sum_categ(rss[3])
            x_brand_categ = self._get_x_brand_categ(rss[5])
            sql_query = """INSERT INTO datacollection_sunview_info (x_cmu_user_id, x_record_time,x_dev_sum,x_ch_sum
            ,x_snap_sum,x_sunview_ver, x_cmu_ipaddr,id,x_cmu_guid,x_ver_categ, x_dev_sum_categ,x_ch_sum_categ, 
            x_brand_categ, x_first_record_time) VALUES({0},'{1}', {2}, {3},{4},'{5}','{6}',{7}, '{8}','{9}','{10}',
            '{11}', '{12}', '{1}')  
            ON conflict (x_cmu_user_id) DO UPDATE SET x_record_time='{1}', x_dev_sum={2}, x_ch_sum={3}, x_snap_sum={4},
             x_sunview_ver='{5}',x_cmu_ipaddr='{6}',x_cmu_guid='{8}', x_ver_categ='{9}', x_dev_sum_categ= '{10}',
              x_ch_sum_categ= '{11}', x_brand_categ='{12}';""" \
                .format(rss[0], rss[1], rss[2], rss[3], rss[4], rss[5], rss[6], rss[7], rss[9], x_ver_categ,
                        x_dev_sum_categ, x_ch_sum_categ, x_brand_categ)
            self.env.cr.execute(sql_query)
            self.env.cr.commit()

            sql_device_info = "SELECT * FROM `inview_sunview_device_info` WHERE summary_info_id = {0}".format(rss[7])
            cursor.execute(sql_device_info)
            rs_dev_info = cursor.fetchall()

            sql_info_id = "SELECT id FROM datacollection_sunview_info WHERE x_cmu_user_id = {0}".format(rss[0])
            self.env.cr.execute(sql_info_id)
            rs_info_id = self.env.cr.fetchall()

            if len(rs_info_id) < 1:
                continue

            if len(rs_dev_info) > 0:
                sql_dc_dev_info_del = "DELETE from datacollection_sunview_dev_info WHERE x_summary_info_id = {0}".format(
                    rs_info_id[0][0])
                self.env.cr.execute(sql_dc_dev_info_del)

            for dev_info in rs_dev_info:
                x_dev_type_name = self._get_dev_type_name(rs_dev_type, dev_info[3])
                x_dev_protocal_name = self._get_dev_protocol_name(rs_dev_protocol, dev_info[2])
                sql_dc_dev_info_in = "INSERT INTO datacollection_sunview_dev_info (x_summary_info_id, x_dev_type," \
                                     "x_dev_protocal,x_dev_sum,x_cmu_ipaddr, x_dev_type_name, x_dev_protocal_name)" \
                                     " VALUES({0}, {1}, {2}, {3},'{4}','{5}', '{6}')".format(rs_info_id[0][0],
                                                                                             dev_info[3], dev_info[2],
                                                                                             dev_info[4], dev_info[6],
                                                                                             x_dev_type_name,
                                                                                             x_dev_protocal_name)
                self.env.cr.execute(sql_dc_dev_info_in)
                self.env.cr.commit()
            self.update_geographical_position_by_ip(rss[6])
        cursor.close()
        db.close()

    @api.model
    def get_collect_data_from_sunview_db(self):
        db = pymysql.connect(host='sunell.com.cn', port=3360, user='admin', password='inview_admin123',
                             database='inview', charset='utf8')
        cursor = db.cursor()
        # sql = """SELECT * FROM
        #         (SELECT user_info_id, MAX(report_time),access_device_sum,access_channel_sum,snap_sum,software_version, ip, id
        #         FROM `inview_sunview_summary_info`  WHERE ip!='' GROUP BY user_info_id,DATE_FORMAT(report_time, '%Y-%m-%d')
        #         ) AS b LEFT JOIN inview_sunview_software_user_info AS a ON a.id = b.user_info_id"""
        sql = """SELECT * FROM
                        (SELECT user_info_id, MAX(report_time),access_device_sum,access_channel_sum,snap_sum,
                        software_version, ip, id FROM `inview_sunview_summary_info`  WHERE ip!='' GROUP BY user_info_id 
                        ) AS b LEFT JOIN inview_sunview_software_user_info AS a ON a.id = b.user_info_id"""
        cursor.execute(sql)
        rs = cursor.fetchall()
        # cursor.close()
        # db.close()
        if len(rs) == 0:
            cursor.close()
            db.close()
            return

        sql = """SELECT user_info_id, MIN(report_time) FROM 
                inview_sunview_summary_info  WHERE ip!='' GROUP BY user_info_id"""
        cursor.execute(sql)
        rs_first_time = cursor.fetchall()

        sql_clear = "delete from datacollection_sunview_dev_info where id > 0"
        self.env.cr.execute(sql_clear)
        sql_clear1 = "delete from datacollection_sunview_info where id > 0"
        self.env.cr.execute(sql_clear1)

        sql_query1 = "select x_dev_protocal_id, x_dev_protocal_name from datacollection_sunview_protocol_info"
        self.env.cr.execute(sql_query1)
        rs_dev_protocol = self.env.cr.fetchall()

        sql_query2 = "select x_dev_type_id, x_dev_type_name from datacollection_sunview_dev_type_info"
        self.env.cr.execute(sql_query2)
        rs_dev_type= self.env.cr.fetchall()

        for rss in rs:
            x_ver_categ = self._get_sunview_categ_ver(rss[5])
            x_dev_sum_categ = self._get_dev_sum_categ(rss[2])
            x_ch_sum_categ = self._get_ch_sum_categ(rss[3])
            x_brand_categ = self._get_x_brand_categ(rss[5])
            x_first_record_time = ''
            for item in rs_first_time:
                if item[0] == rss[0]:
                    x_first_record_time = item[1]
                    break

            sql_cmu = "insert into datacollection_sunview_info (x_cmu_user_id, x_record_time,x_dev_sum,x_ch_sum, " \
                      "x_snap_sum,x_sunview_ver,x_cmu_ipaddr,id,x_cmu_guid, x_ver_categ, x_dev_sum_categ," \
                      "x_ch_sum_categ, x_brand_categ, x_first_record_time) VALUES({0}, '{1}', " \
                      "{2}, {3},{4},'{5}','{6}',{7}, '{8}', '{9}', '{10}', '{11}', '{12}','{13}')" \
                      "".format(rss[0], rss[1], rss[2], rss[3], rss[4], rss[5], rss[6], rss[7], rss[9],
                                x_ver_categ, x_dev_sum_categ, x_ch_sum_categ, x_brand_categ, x_first_record_time)
            self.env.cr.execute(sql_cmu)

        id_list = ''
        for rss in rs:
            id_list += str(rss[7]) + ','
        id_list += '-1'

        # db = pymysql.connect(host='sunell.com.cn', port=3360, user='admin', password='inview_admin123',
        #                      database='inview', charset='utf8')
        # cursor1 = db1.cursor()
        sql = """SELECT * FROM inview_sunview_device_info where summary_info_id in ({0})""".format(id_list)
        cursor.execute(sql)
        rsd = cursor.fetchall()
        cursor.close()
        db.close()
        if len(rsd) == 0:
            return

        for rss in rsd:
            x_cmu_ipaddr = ""
            for rss1 in rs:
                if rss1[7] == rss[1]:
                    x_cmu_ipaddr = rss1[6]
                    break
            x_dev_type_name = self._get_dev_type_name(rs_dev_type, rss[3])
            x_dev_protocal_name = self._get_dev_protocol_name(rs_dev_protocol, rss[2])
            sql_dev = "insert into datacollection_sunview_dev_info (x_summary_info_id,x_dev_protocal,x_dev_type" \
                      ", x_dev_sum, x_cmu_ipaddr,x_dev_type_name, x_dev_protocal_name) VALUES({0},{1},{2},{3},'{4}','{5}'" \
                      ",'{6}')".format(rss[1], rss[2], rss[3], rss[4], x_cmu_ipaddr, x_dev_type_name, x_dev_protocal_name)
            self.env.cr.execute(sql_dev)

    def get_china_geographical_position_by_ip(self, str_china_ip):
        url = "http://ip.ws.126.net/ipquery?ip=" + str_china_ip
        para = {}
        header = {}

        str_result = requests.get(url, params=para, headers=header, )
        if 200 != str_result.status_code:
            return None

        str_res_text = str_result.text
        pos_start = str_res_text.find("{")
        pos_end = str_res_text.find("}")
        if pos_start <= 0 or pos_end <= 0:
            return None
        str_res_text = str_res_text[pos_start:pos_end + 1]
        str_res_text = str_res_text.replace("city", "\"city\"")
        str_res_text = str_res_text.replace("province", "\"province\"")
        res_json = json.loads(str_res_text)
        return res_json

    def update_geographical_position_by_ip(self, str_ip):
        sql_country_select = "SELECT x_world_area,x_country, x_province, x_city  " \
                             "from datacollection_sunview_info where x_cmu_ipaddr='{0}'".format(str_ip)
        self.env.cr.execute(sql_country_select)
        rs = self.env.cr.fetchall()
        net_search_and_update = False
        local_search_and_update = False
        str_world_area = ''
        str_country = ''
        str_province = ''
        str_city = ''
        if 0 != len(rs):
            has_empty = False
            for item in rs:
                if item[1] == '' or item[1] is None:
                    has_empty = True
                    if str_country != '':
                        break
                else:
                    str_world_area = item[0]
                    str_country = item[1]
                    str_province = item[2]
                    str_city = item[3]
                    if has_empty:
                        break
            if str_country == '' and has_empty == True:
                net_search_and_update = True
            elif str_country != '' and has_empty == True:
                local_search_and_update = True
            else:
                return False
        else:
            net_search_and_update = True

        if net_search_and_update == False and local_search_and_update == False:
            return False

        if net_search_and_update:
            url = "http://ip-api.cn/json/" + str_ip
            para = {"lang": "zh-CN", "fields": "continent,country,regionName,city,countryCode"}
            header = {}

            r = requests.get(url, params=para, headers=header, )
            if 200 != r.status_code:
                return False
            json_r = r.json()
            str_world_area = json_r["continent"]
            if str_ip == "220.231.188.178" or str_ip == "220.231.188.184":
                str_world_area = "景阳"
            str_country = json_r["country"]
            str_province = json_r["regionName"]
            str_city = json_r["city"]
            if json_r["countryCode"] == "CN":
                req_json = self.get_china_geographical_position_by_ip(str_ip)
                if req_json is not None:
                    if len(req_json["province"]) > 0:
                        str_province = req_json["province"]
                    if len(req_json["city"]) > 0:
                        str_city = req_json["city"]

        str_world_area = str_world_area.replace("'", "''")
        str_country = str_country.replace("'", "''")
        if str_country == "台湾":
            str_country = "中国台湾"
        elif str_country == "澳门":
            str_country = "中国澳门"
        elif str_country == "香港":
            str_country = "中国香港"
        # _logger = logging.getLogger(__name__)
        # _logger.info("update_geographical_position_by_ip str_province=%s str_ip = %s", str_province, str_ip)
        str_province = str_province.replace("'", "''")
        str_city = str_city.replace("'", "''")

        sql_country_update = """UPDATE datacollection_sunview_info SET x_world_area='{0}', x_country='{1}', 
                                x_province='{2}', x_city='{3}' where x_cmu_ipaddr = '{4}'""".format(
            str_world_area, str_country, str_province, str_city, str_ip)

        self.env.cr.execute(sql_country_update)
        self.env.cr.commit()

        sql_country_update1 = """UPDATE datacollection_sunview_dev_info SET x_world_area='{0}', x_country='{1}', 
                                        x_province='{2}', x_city='{3}' where x_cmu_ipaddr = '{4}'""".format(
            str_world_area, str_country, str_province, str_city, str_ip)

        self.env.cr.execute(sql_country_update1)
        self.env.cr.commit()

        return net_search_and_update

    def organization_geographical_position(self):
        # db = pymysql.connect(host='sunell.com.cn', port=3360, user='admin', password='inview_admin123',
        #                      database='inview', charset='utf8')
        # cursor = db.cursor()
        # sql = """SELECT ip FROM `inview_sunview_summary_info` AS a, `inview_sunview_software_user_info`
        #          AS b WHERE a.`user_info_id` = b.`id` AND ip!=0 GROUP BY a.`ip`"""
        # cursor.execute(sql)
        # rs = cursor.fetchall()
        # cursor.close()
        # db.close()
        # if len(rs) == 0:
        #     return

        sql_pos = """select x_cmu_ipaddr FROM datacollection_sunview_info"""
        self.env.cr.execute(sql_pos)
        rs = self.env.cr.fetchall()
        timespan = 60 / 40.0
        index = 0
        count = len(rs)
        for item in rs:
            time_s = time.time()
            ret = self.update_geographical_position_by_ip(item[0])
            if ret:
                time_cost = time.time() - time_s
                timesleep = timespan - time_cost
                if timesleep > 0:
                    time.sleep(timesleep)
                index += 1
                _logger = logging.getLogger(__name__)
                _logger.info("organization_geographical_position index=%d sleeptime = %f count=%d", index, timesleep,
                             count)


class sunview_info(models.Model):
    _name = 'datacollection.sunview_info'
    _description = 'datacollection.sunview_info'
    x_cmu_guid = fields.Char(string="CMU唯一ID")
    x_cmu_user_id = fields.Integer()
    x_summary_info_id = fields.One2many('datacollection.sunview_dev_info', 'x_summary_info_id')
    x_cmu_ipaddr = fields.Char(string="IP")
    x_world_area = fields.Char(string="世界区域")
    x_country = fields.Char(string="国家")
    x_province = fields.Char(string="省/州")
    x_city = fields.Char(string="城市")
    x_dev_sum = fields.Integer(string="设备总数")
    x_ch_sum = fields.Integer(string="通道总数")
    x_snap_sum = fields.Integer(string="抓拍总数")
    x_first_record_time = fields.Datetime(string="首次上报时间")
    x_record_time = fields.Datetime(string="最新上报时间")
    x_sunview_ver = fields.Char(string="Sunview 版本信息")
    x_ver_categ = fields.Char(string="版本分类")
    x_dev_sum_categ = fields.Char(string="设备数量分类")
    x_ch_sum_categ = fields.Char(string="通道数量分类")
    x_brand_categ = fields.Char(string="品牌分类")
    _sql_constraints = [('unique_code', 'UNIQUE(x_cmu_user_id)', 'code must be unique')]
