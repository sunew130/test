# -*- coding: utf-8 -*-

from odoo import _, api, fields, models, SUPERUSER_ID
from odoo.tools import float_compare


class Product(models.Model):
    _inherit = "product.product"

    approval_product_id = fields.One2many('approval.product.line', 'product_id', string="Products", check_company=True,
                                          domain=lambda self: ['&', ('approval_type', '=', 'pricing'),
                                                               ('request_status', '=', 'approved')],
                                          order='approval_request_id desc, begin_quantity')

    # 选择最优供应商
    # 1.排序价格低优先
    # 2.价格相同，交货期断优先
    def _select_seller(self, partner_id=False, quantity=0.0, date=None, uom_id=False, params=False):
        self.ensure_one()
        if date is None:
            date = fields.Date.context_today(self)
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        res = self.env['product.supplierinfo']
        res_other = self.env['product.supplierinfo']
        sellers = self._prepare_sellers(params)
        sellers = sellers.filtered(lambda s: not s.company_id or s.company_id.id == self.env.company.id)
        for seller in sellers:
            # Set quantity in UoM of seller
            quantity_uom_seller = quantity
            if quantity_uom_seller and uom_id and uom_id != seller.product_uom:
                quantity_uom_seller = uom_id._compute_quantity(quantity_uom_seller, seller.product_uom)

            if seller.date_start and seller.date_start > date:
                continue
            if seller.date_end and seller.date_end < date:
                continue
            # 目前为‘-VS ’后缀的供应商为虚拟供应商
            if partner_id and seller.name not in [partner_id, partner_id.parent_id] and partner_id.name.find('-VS') < 0:
                # 采购单的供应商选定后，继续执行尝试获取最佳供应商
                if float_compare(quantity_uom_seller, seller.min_qty, precision_digits=precision) == -1:
                    continue
                if seller.product_id and seller.product_id != self:
                    continue

                res_other |= seller
                continue
            if float_compare(quantity_uom_seller, seller.min_qty, precision_digits=precision) == -1:
                continue
            if seller.product_id and seller.product_id != self:
                continue

            res |= seller

        seller_other = res_other.sorted(key=lambda x: (x['price'], x['delay']))[:1]
        seller = res.sorted(key=lambda x: (x['price'], x['delay']))[:1]

        # 获取最优供应商
        better_seller = seller
        if seller_other and seller:
            if seller_other.price < seller.price:
                better_seller = seller_other

        if params is not False and params['order_line_id']:
            params['order_line_id']['current_seller'] = seller.name
            params['order_line_id']['better_seller'] = better_seller.name

        return seller


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    approval_product_id = fields.One2many(related='product_variant_id.approval_product_id')
