# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64

from odoo import api, fields, models, tools, _

CATEGORY_SELECTION = [
    ('required', 'Required'),
    ('optional', 'Optional'),
    ('no', 'None')]


class ApprovalCategory(models.Model):
    _inherit = 'approval.category'

    approval_type = fields.Selection(selection_add=[('pricing', 'Create Pricing')])

    has_supplier = fields.Selection(CATEGORY_SELECTION, string="Has Supplier", default="no", required=True)
    has_price = fields.Selection(CATEGORY_SELECTION, string="Has Price", default="no", required=True)
    has_begin_quantity = fields.Selection(CATEGORY_SELECTION, string="Has Begin Quantity", default="no", required=True)
    has_end_quantity = fields.Selection(CATEGORY_SELECTION, string="Has End Quantity", default="no", required=True)
    has_delay_time = fields.Selection(CATEGORY_SELECTION, string="Has Delay Time", default="no", required=True)

    @api.onchange('approval_type')
    def _onchange_approval_type(self):
        if self.approval_type == 'pricing':
            self.has_product = 'required'
            self.has_quantity = 'required'
            self.has_supplier = 'required'
            self.has_price = 'required'
            self.has_begin_quantity = 'required'
            self.has_end_quantity = 'required'
            self.has_delay_time = 'required'

    def open_pricing_request(self):

        # 找出核价单的审批类型
        res = self.env['approval.category'].search([('approval_type', '=', 'pricing')], limit=1)
        if res is None:
            return

        self |= res
        if self.automated_sequence:
            name = self.sequence_id.next_by_id()
        else:
            name = self.name
        return {
            "type": "ir.actions.act_window",
            "res_model": "approval.request",
            "views": [[self.env.ref('approvals_pricing.approval_request_view_tree_extra').id, "tree"], [False, "form"]],
            "name": "核价单",
            "context": {
                'form_view_initial_mode': 'edit',
                'default_name': name,
                'default_category_id': self.id,
                'default_request_owner_id': self.env.user.id,
                'default_request_status': 'new'
            },

        }


