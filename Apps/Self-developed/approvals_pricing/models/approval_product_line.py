# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class ApprovalProductLine(models.Model):
    _inherit = 'approval.product.line'

    product_price = fields.Float("Uom price")
    begin_quantity = fields.Float("Begin quantity")
    end_quantity = fields.Float("End quantity")
    supplier_id = fields.Many2one('res.partner', string='Supplier', domain="['&', ('supplier_rank','>', 0), "
                                                                           "('is_company', '=', True)]")

    # 原税前单价
    ori_unit_price_before_tax = fields.Float("Original unit price before tax", digits=(16, 5))
    # 原含税单价
    ori_tax_inclu_unit_price = fields.Float("Original tax-included unit price", digits=(16, 5))
    # 新税前单价
    new_unit_price_before_tax = fields.Float("New unit price before tax", digits=(16, 5))
    # 新含税单价
    new_tax_inclu_unit_price = fields.Float("New tax-included unit price", digits=(16, 5))

    request_owner_id = fields.Many2one('res.users', string="Request Owner",
                                       related="approval_request_id.request_owner_id")

    date_confirmed = fields.Datetime(string="Date Confirmed", related="approval_request_id.date_confirmed")

    date_approved = fields.Datetime(string="Date Approved", related="approval_request_id.date_approved")

    request_status = fields.Selection(string="Request Status", related="approval_request_id.request_status",
                                      domain="[('request_status','=', 'approved')]")

    approval_type = fields.Selection(related='approval_request_id.approval_type')

    delay_time = fields.Integer(string="Delay Time")

    x_min_packing_quantity = fields.Integer(string="Minimum amount of packaging")

    @api.onchange('product_id', 'begin_quantity', 'end_quantity')
    def _onchange_get_last_pricing(self):
        domain = []
        if self.product_id and self.supplier_id and self.begin_quantity >= 0 and self.end_quantity >= 0:
            domain += [('product_id', '=', self.product_id.id)]
            domain += [('begin_quantity', '=', self.begin_quantity)]
            domain += [('end_quantity', '=', self.end_quantity)]
            domain += [('request_status', '=', 'approved')]
            domain += [('supplier_id', '=', self.supplier_id.id)]
            domain += [('approval_type', '=', 'pricing')]
            record = self.env['approval.product.line'].search(domain, order='approval_request_id desc', limit=1)
            if record:
                self.ori_unit_price_before_tax = record.new_unit_price_before_tax
                self.ori_tax_inclu_unit_price = record.new_tax_inclu_unit_price

    @api.onchange('new_tax_inclu_unit_price')
    def _onchange_compute_new_unit_price_before_tax(self):
        if self.supplier_id:
            amount = self.supplier_id.taxed_id.amount
            self.new_unit_price_before_tax = self.new_tax_inclu_unit_price * 100 / (amount + 100)
