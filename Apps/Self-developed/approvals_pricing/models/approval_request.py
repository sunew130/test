# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ApprovalRequest(models.Model):
    _inherit = 'approval.request'

    has_supplier = fields.Selection(related="category_id.has_supplier")
    has_price = fields.Selection(related="category_id.has_price")
    has_begin_quantity = fields.Selection(related="category_id.has_begin_quantity")
    has_end_quantity = fields.Selection(related="category_id.has_end_quantity")
    has_delay_time = fields.Selection(related="category_id.has_delay_time")
    x_can_approve = fields.Boolean(default=False, string='是否显示批准按钮', compute='_compute_can_approve')

    def action_confirm(self):
        valid = self.check_data_valid()
        if valid is False:
            return
        super(ApprovalRequest, self).action_confirm()

    def check_data_valid(self):
        if 0 == len(self.product_line_ids):
            raise UserError(_("You haven't added the product yet."))
            return False

        if 1 == len(self.product_line_ids):
            return True

        product_tmpl_id = -1
        supplier_id = -1
        for product_line in self.product_line_ids:
            if -1 == product_tmpl_id or -1 == supplier_id:
                product_tmpl_id = product_line.product_id.product_tmpl_id.id
                supplier_id = product_line.supplier_id.id
                continue
            if product_line.supplier_id.id is False:
                raise UserError(_("The product supplier cannot be empty, please choose again."))
                return False
            if product_tmpl_id != product_line.product_id.product_tmpl_id.id:
                raise UserError(_("The products you added are inconsistent. Please select the same product."))
                return False
            if supplier_id != product_line.supplier_id.id:
                raise UserError(
                    _("The product you added does not have the same supplier. Please select the same supplier."))
                return False

        return True

    def action_approve(self, approver=None):

        super(ApprovalRequest, self).action_approve(approver)
        self.save_approve_result()

    def save_approve_result(self):
        if 'approved' != self.request_status:
            return

        #尝试把首次导入的供应商价格表保存在核价单中
        self.try_save_supplier_info_2_pricing()

        if self.product_line_ids:
            supplier_id = self.product_line_ids[0].supplier_id
            seller_ids = self.product_line_ids[0].product_id.product_tmpl_id.seller_ids
            for seller_id in seller_ids:
                if supplier_id == seller_id.name:
                    seller_id.unlink()

        for product in self.product_line_ids:
            # seller_ids = product.product_id.seller_ids
            # for seller_id in seller_ids:
            #     if product.end_quantity
            # product.supplier_id
            # product.supplier_id.property_product_pricelist
            # product.supplierinfo
            # res = self.env['product.supplierinfo'].search(
            #     ['&', ('product_id', '=', product.product_id), ('id', '=', product.supplier_id)])
            self.sudo().env['product.supplierinfo'].create([
                {
                    'name': product.supplier_id.id,
                    # 'product_id': product.product_id.id,
                    'product_tmpl_id': product.product_id.product_tmpl_id.id,
                    'currency_id': product.product_id.currency_id.id,
                    'delay': product.delay_time,
                    'product_uom': product.product_uom_id,
                    'min_qty': product.begin_quantity,
                    'price': product.new_unit_price_before_tax,
                    'x_min_packing_quantity': product.x_min_packing_quantity,
                }])

        self.select_better_supplier()

    def action_withdraw(self, approver=None):
        if 'approved' == self.request_status:
            self.undo_save_approve_data()
        if not isinstance(approver, models.BaseModel):
            approver = self.mapped('approver_ids').filtered(
                lambda approver: approver.user_id == self.env.user
            )
        approver.write({'status': 'pending'})

    def undo_save_approve_data(self):
        if self.product_line_ids:
            supplier_id = self.product_line_ids[0].supplier_id
            seller_ids = self.product_line_ids[0].product_id.product_tmpl_id.seller_ids
            product_id = self.product_line_ids[0].product_id.id
            begin_quantity = self.product_line_ids[0].begin_quantity
            for seller_id in seller_ids:
                if supplier_id == seller_id.name:
                    seller_id.unlink()

            domain = [('product_id', '=', product_id)]
            domain += [('request_status', '=', 'approved')]
            domain += [('supplier_id', '=', supplier_id.id)]
            domain += [('approval_request_id', '!=', self.id)]
            domain += [('approval_type', '=', 'pricing')]

            records = self.env['approval.product.line'].search(domain, order='approval_request_id desc,begin_quantity', limit=10)
            request_id = None
            for record in records:
                #获取统一核价单的产品核价
                if request_id is None:
                    request_id = record.approval_request_id.id
                elif request_id != record.approval_request_id.id:
                    break
                self.sudo().env['product.supplierinfo'].create([
                    {
                        'name': record.supplier_id.id,
                        # 'product_id': product.product_id.id,
                        'product_tmpl_id': record.product_id.product_tmpl_id.id,
                        'delay': record.delay_time,
                        'product_uom': record.product_uom_id,
                        'min_qty': record.begin_quantity,
                        'price': record.new_unit_price_before_tax,
                    }])

    def _compute_can_approve(self):
        for record in self:
            record.x_can_approve = False
            for approve_item in record.approver_ids:
                if 'pending' == approve_item.status:
                    if self.env.user.id == approve_item.user_id.id \
                            or self.env.user.id in self.env.user.associate_user_id.x_associated_user_id.ids:
                        record.x_can_approve = True
                    break

    def select_better_supplier(self):
        #1.按最小数量排序
        #2.按价格低排序
        #2.按交期排序
        if self.product_line_ids:
            seller_ids = self.product_line_ids[0].product_id.product_tmpl_id.seller_ids
            list_seller_id = []

            for seller_id in seller_ids:
                list_seller_id.append([seller_id.min_qty, seller_id.price, seller_id.delay, seller_id])

            list_seller_id.sort()

            for i in range(len(list_seller_id)):
                list_seller_id[i][3].sequence = i + 1

    def try_save_supplier_info_2_pricing(self):
        domain = []
        #1.搜索从核价单中，是否找到产品的核价记录
        if self.product_line_ids:
            domain += [('product_id', '=',  self.product_line_ids[0].product_id.id)]
            domain += [('request_status', '=', 'approved')]
            domain += [('approval_type', '=', 'pricing')]
            domain += [('approval_request_id', '!=',  self.id)]
            record = self.env['approval.product.line'].search(domain, order='approval_request_id desc', limit=1)

            # 2.没有找到产品的核价记录，通过从当前的供应商价格表，写在最早的核价单中
            if len(record) == 0:
                #2.1获取最早的核价单
                approval_request = self.env['approval.request'].search([('name', '=', 'Pricing00001')], limit=1)

                if approval_request:
                    #2.2获取当前的供应商价格表，写在最早的核价单
                    records = self.env['product.supplierinfo'].search(
                        [('product_tmpl_id', '=', self.product_line_ids[0].product_id.product_tmpl_id.id)], order='name, min_qty')

                    for item in records:
                        self.env['approval.product.line'].create([{
                            'approval_request_id': approval_request.id,
                            'supplier_id': item.name.id,
                            'product_id': item.product_tmpl_id.product_variant_id.id,
                            'description' : item.product_tmpl_id.display_name,
                            'delay_time': item.delay,
                            'product_uom_id': item.product_uom.id,
                            'begin_quantity': item.min_qty,
                            'new_unit_price_before_tax': item.price,
                            'new_tax_inclu_unit_price': item.price* (item.name.taxed_id.amount + 100) / 100}]
                        )