# -*- coding: utf-8 -*-

from odoo import _, api, fields, models, SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    current_seller = fields.Many2one('res.partner', string='Current seller', readonly=True, compute="_compute_seller")

    better_seller = fields.Many2one('res.partner', string='Better seller', readonly=True, compute="_compute_seller")

    def _compute_seller(self):
        if not self.product_id:
            return
        params = {'order_id': self.order_id, 'order_line_id': self}
        for record in self:
            record.product_id._select_seller(
                partner_id=record.partner_id,
                quantity=record.product_qty,
                date=record.order_id.date_order and record.order_id.date_order.date(),
                uom_id=record.product_uom,
                params=params)

    @api.onchange('product_qty', 'product_uom')
    def _onchange_quantity(self):
        if not self.product_id:
            return
        params = {'order_id': self.order_id, 'order_line_id': self}
        seller = self.product_id._select_seller(
            partner_id=self.partner_id,
            quantity=self.product_qty,
            date=self.order_id.date_order and self.order_id.date_order.date(),
            uom_id=self.product_uom,
            params=params)

        if seller or not self.date_planned:
            self.date_planned = self._get_date_planned(seller).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        # If not seller, use the standard price. It needs a proper currency conversion.
        if not seller:
            price_unit = self.env['account.tax']._fix_tax_included_price_company(
                self.product_id.standard_price,
                self.product_id.supplier_taxes_id,
                self.taxes_id,
                self.company_id,
            )
            if price_unit and self.order_id.currency_id and self.order_id.company_id.currency_id != self.order_id.currency_id:
                price_unit = self.order_id.company_id.currency_id._convert(
                    price_unit,
                    self.order_id.currency_id,
                    self.order_id.company_id,
                    self.date_order or fields.Date.today(),
                )
            self.price_unit = price_unit
            return

        price_unit = self.env['account.tax']._fix_tax_included_price_company(seller.price,
                                                                             self.product_id.supplier_taxes_id,
                                                                             self.taxes_id,
                                                                             self.company_id) if seller else 0.0
        if price_unit and seller and self.order_id.currency_id and seller.currency_id != self.order_id.currency_id:
            price_unit = seller.currency_id._convert(
                price_unit, self.order_id.currency_id, self.order_id.company_id, self.date_order or fields.Date.today())

        if seller and self.product_uom and seller.product_uom != self.product_uom:
            price_unit = seller.product_uom._compute_price(price_unit, self.product_uom)

        self.price_unit = price_unit
        # 最小包装量计算
        min_p_qty = seller.x_min_packing_quantity
        if min_p_qty > 0:
            pdt_qty = int(self.product_qty)
            mod_qty = min_p_qty if pdt_qty % min_p_qty > 0 else 0
            self.product_qty = (pdt_qty // min_p_qty) * min_p_qty + mod_qty



