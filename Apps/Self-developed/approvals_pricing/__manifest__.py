# -*- coding: utf-8 -*-
{
    'name': "approvals_pricing",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'approvals', 'purchase'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/security.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/approval_category_views.xml',
        'views/approval_product_line_views.xml',
        'views/approval_request_views.xml',
        'data/approval_category_data.xml',
        'views/purchase_pricing_views.xml',
        'views/product_pricing_views.xml',
        'views/purchase_views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
        'data/approval_demo.xml',
    ],
}
