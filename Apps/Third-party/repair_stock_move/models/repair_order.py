from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.tools import float_compare


class RepairOrder(models.Model):
    _inherit = "repair.order"

    stock_move_ids = fields.One2many("stock.move", "repair_id")
    show_check_availability = fields.Boolean(compute="_compute_show_check_availability",
                                             help="Technical field used to compute whether the button 'Check Availability' should be displayed.")
    ignore_availability = fields.Boolean()

    @api.depends("state")
    def _compute_show_check_availability(self):
        for rec in self:
            rec.show_check_availability = (
                    any(
                        move.state in ("waiting", "confirmed", "partially_available")
                        and float_compare(
                            move.product_uom_qty,
                            0,
                            precision_rounding=move.product_uom.rounding,
                        )
                        for move in rec.stock_move_ids
                    )
                    and not rec.ignore_availability
            )

    def _create_repair_stock_move(self):
        self.ensure_one()
        return self.env["stock.move"].create({
            "name": self.name,
            "product_id": self.product_id.id,
            "product_uom": self.product_uom.id or self.product_id.uom_id.id,
            "product_uom_qty": self.product_qty,
            "partner_id": self.address_id.id,
            "lot_ids": [(4, self.lot_id.id)] if self.lot_id else self.lot_id,
            "picking_type_id": self._get_stock_warehouse.int_type_id.id,
            "location_id": self._get_repair_location,
            "location_dest_id": self.location_id.id,
            # "move_line_ids": [(0, 0, {"product_id": self.product_id.id,
            #                           "lot_id": self.lot_id.id,
            #                           "product_uom_qty": 0,  # bypass reservation here
            #                           "product_uom_id": self.product_uom.id or self.product_id.uom_id.id,
            #                           "qty_done": self.product_qty,
            #                           "package_id": False,
            #                           "result_package_id": False,
            #                           # "owner_id": False,
            #                           "location_id": self._get_repair_location,
            #                           "company_id": self.company_id.id,
            #                           "location_dest_id": self.location_id.id})],
            "repair_id": self.id,
            "origin": self.name,
            "company_id": self.company_id.id,
        })

    def action_repair_confirm(self):
        res = {}
        for repair in self:
            moves = self.env["stock.move"]
            for operation in repair.operations:
                move = operation.create_stock_move()
                moves |= move
                operation.write({"move_id": move.id})
            move = repair._create_repair_stock_move()
            repair.move_id = move.id
            res[repair.id] = (moves, move)
        # self.mapped("stock_move_ids")._action_confirm()
        return super().action_repair_confirm(), res

    def action_assign(self):
        draft = self.filtered(lambda r: r.state == "draft")
        if draft:
            draft.action_repair_start()
        moves = self.mapped("stock_move_ids")
        moves = moves.filtered(
            lambda move: move.state not in ("draft", "cancel", "done")
        )
        if not moves:
            raise UserError(_("Nothing to check the availability for."))
        moves._action_assign()
        return True

    def action_repair_start(self):
        res = super().action_repair_start()
        self.mapped("stock_move_ids")._action_assign()
        return res

    def action_force_availability(self):
        self.write({"ignore_availability": True})

    def _force_qty_done_in_repair_lines(self):
        for operation in self.mapped("operations"):
            for move in operation.stock_move_ids:
                if move.state not in ["confirmed", "waiting", "partially_available"]:
                    continue
                product_qty = move.product_uom._compute_quantity(
                    operation.product_uom_qty,
                    move.product_id.uom_id,
                    rounding_method="HALF-UP",
                )
                available_quantity = self.env["stock.quant"]._get_available_quantity(
                    move.product_id,
                    move.location_id,
                    lot_id=operation.lot_id,
                    strict=False,
                )
                move._update_reserved_quantity(
                    product_qty - move.reserved_availability,
                    available_quantity,
                    move.location_id,
                    lot_id=operation.lot_id,
                    strict=False,
                )
                move._set_quantity_done(operation.product_uom_qty)
                if operation.lot_id:
                    move.move_line_ids.lot_id = operation.lot_id

    def action_repair_cancel(self):
        self.mapped("stock_move_ids")._action_cancel()
        return super().action_repair_cancel()

    def action_repair_end(self):
        if any(r.show_check_availability for r in self):
            raise UserError(_("Some related stock moves are not available."))
        # I can not everything has been reserved.
        self._force_qty_done_in_repair_lines()
        for repair in self:
            operation_moves = repair.mapped("operations.move_id")
            if operation_moves:
                consumed_lines = operation_moves.mapped("move_line_ids")
                produced_lines = repair.move_id.move_line_ids
                operation_moves |= repair.move_id
                produced_lines.write({"consume_line_ids": [(6, 0, consumed_lines.ids)]})

        self.move_id._set_quantity_done(self.move_id.product_uom_qty)
        self.move_id._action_done()
        for move in self.mapped("operations.move_id"):
            move._set_quantity_done(move.product_uom_qty)
            move._action_done()
        return super().action_repair_end()

    def action_repair_done(self):
        self.ensure_one()
        if self.stock_move_ids:
            return {self.id: self.move_id.id}
        return super().action_repair_done()
