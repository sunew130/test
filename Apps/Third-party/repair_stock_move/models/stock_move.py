from odoo import fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    repair_line_id = fields.Many2one("repair.line", string="Repair Line", ondelete="cascade")
