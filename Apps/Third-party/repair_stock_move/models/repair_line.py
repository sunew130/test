
from odoo import api, fields, models


class RepairLine(models.Model):
    _inherit = "repair.line"

    stock_move_ids = fields.One2many(
        comodel_name="stock.move",
        inverse_name="repair_line_id",
    )

    def create_stock_move(self):
        self.ensure_one()
        move = self.env["stock.move"].create(
            {
                "name": self.repair_id.name,
                "product_id": self.product_id.id,
                "product_uom_qty": self.product_uom_qty,
                "product_uom": self.product_uom.id,
                "partner_id": self.repair_id.address_id.id,
                "lot_ids": [(4, self.lot_id.id)] if self.lot_id else self.lot_id,
                "location_id": self.location_id.id,
                "location_dest_id": self.repair_id._get_repair_location,
                "repair_id": self.repair_id.id,
                "repair_line_id": self.id,
                "origin": self.repair_id.name,
                "company_id": self.company_id.id,
                "picking_type_id": self.repair_id._get_stock_warehouse.int_type_id.id
            }
        )
        return move

    # @api.model
    # def create(self, vals):
    #     res = super().create(vals)
    #     if res and res.repair_id.state == "confirmed":
    #         move = res.create_stock_move()
    #         move._action_confirm()
    #         res.move_id = move
    #     if res and res.repair_id.state == "under_repair":
    #         move = res.create_stock_move()
    #         move._action_confirm()
    #         move._action_assign()
    #         res.move_id = move
    #     return res

    # @api.onchange("product_id")
    # def _onchange_location(self):
    #     if self.state == "draft":
    #         self.location_id = self.repair_id.location_id

    # TODO: write qty - update stock move.
    # TODO: default repair location in repair lines.
