# -*- coding: utf-8 -*-
{
    "name": "Repair Stock Move",
    "version": "14.0.1.0.0",
    "development_status": "Alpha",
    "license": "LGPL-3",
    "category": "Repair",
    "summary": "Ongoing Repair Stock Moves Definition in odoo",
    "author": "ForgeFlow",
    "website": "",
    "depends": ["repair"],
    "data": [
        # "views/repair_order_views.xml",
    ],
    "installable": True,
    "application": False,
}
